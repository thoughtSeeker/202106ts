import 'dart:ui' show lerpDouble;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class CustomBottomNavigationBar extends StatefulWidget {
  final Curve curve;
  final Color activeColor;
  final Color inactiveColor;
  final Color inactiveStripColor;
  final Color indicatorColor;
  final bool enableShadow;
  final bool enableTopBorder;
  int currentIndex;
  ValueChanged<int> onTap;
  final List<CustomBottomNavigationBarItem> items;

  CustomBottomNavigationBar({
    Key key,
    this.curve = Curves.linear,
    @required this.onTap,
    @required this.items,
    this.activeColor,
    this.inactiveColor,
    this.inactiveStripColor,
    this.indicatorColor,
    this.enableShadow = false,
    this.enableTopBorder = true,
    this.currentIndex = 0,
  })  : assert(items != null),
        assert(items.length >= 2 && items.length <= 5),
        assert(onTap != null),
        assert(currentIndex != null),
        assert(enableShadow != null),
        super(key: key);

  @override
  State createState() => _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  static const double barHeight = 80;
  static const double indicatorHeight = 2;

  Curve get curve => widget.curve;

  List<CustomBottomNavigationBarItem> get items => widget.items;

  double width = 0;
  Color activeColor;
  Duration duration = Duration(milliseconds: 270);

  double _getIndicatorPosition(int index) {
    var isLtr = Directionality.of(context) == TextDirection.ltr;
    if (isLtr)
      return lerpDouble(-1.0, 1.0, index / (items.length - 1));
    else
      return lerpDouble(1.0, -1.0, index / (items.length - 1));
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    activeColor = widget.activeColor ?? Theme.of(context).indicatorColor;

    return Container(
      height: barHeight + MediaQuery.of(context).viewPadding.bottom,
      width: width,
      decoration: BoxDecoration(
        color: widget.inactiveStripColor ?? Theme.of(context).cardColor,
        border: widget.enableTopBorder
            ? Border(
                top: BorderSide(
                  width: 1.0,
                  color: Color(0xFFF1F2F7),
                ),
              )
            : null,
        boxShadow: widget.enableShadow
            ? [
                BoxShadow(
                  color: Color(0xFFF1F2F7),
                  blurRadius: 10,
                ),
              ]
            : null,
      ),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: indicatorHeight,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: items.map((item) {
                var index = items.indexOf(item);
                return GestureDetector(
                  onTap: () => _select(index),
                  child: _buildItemWidget(
                    item,
                    index == widget.currentIndex,
                  ),
                );
              }).toList(),
            ),
          ),
          Positioned(
            top: 0,
            width: width,
            child: AnimatedAlign(
              alignment: Alignment(
                _getIndicatorPosition(widget.currentIndex),
                0,
              ),
              curve: curve,
              duration: duration,
              child: Container(
                color: widget.indicatorColor ?? activeColor,
                width: width / items.length,
                height: indicatorHeight,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _select(int index) {
    widget.currentIndex = index;
    widget.onTap(widget.currentIndex);

    setState(() {});
  }

  Widget _buildIcon(
    CustomBottomNavigationBarItem item,
    bool isSelected,
  ) {
    return SvgPicture.asset(
      item.iconPath,
      color: isSelected ? activeColor : widget.inactiveColor,
    );
  }

  Widget _buildText(
    CustomBottomNavigationBarItem item,
    bool isSelected,
  ) {
    return DefaultTextStyle.merge(
      child: item.title,
      style: TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w600,
        height: 16.0 / 12.0,
        color: isSelected ? activeColor : widget.inactiveColor,
      ),
    );
  }

  Widget _buildItemWidget(
    CustomBottomNavigationBarItem item,
    bool isSelected,
  ) {
    return Container(
      color: item.backgroundColor,
      height: barHeight,
      width: width / items.length,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildIcon(item, isSelected),
          SizedBox(
            height: 5.0,
          ),
          _buildText(item, isSelected),
        ],
      ),
    );
  }
}

class CustomBottomNavigationBarItem {
  final String iconPath;
  final Widget title;
  final Color backgroundColor;

  CustomBottomNavigationBarItem({
    @required this.iconPath,
    @required this.title,
    this.backgroundColor = Colors.white,
  });
}
