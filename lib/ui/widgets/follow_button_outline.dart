import 'package:flutter/material.dart';


class FollowButtonOutline extends StatelessWidget {
  final VoidCallback onPressed;
  FollowButtonOutline({this.onPressed, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90,
      child: OutlineButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        borderSide: BorderSide(color: Colors.blue),
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        color: Colors.blue,
        onPressed: onPressed,
        child: Text(
          'button__follow',
          style: TextStyle(
            color: Colors.blue,
            fontSize: 12.0,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
