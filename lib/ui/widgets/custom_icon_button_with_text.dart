import 'package:flutter/material.dart';

class CustomIconWithText extends StatelessWidget {
  final String text;
  final Widget icon;
  final VoidCallback onPressed;

  CustomIconWithText({
    Key key,
    this.text,
    this.icon,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: const Color(0xf000000),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
            icon: icon,
            // color: Colors.yellow,
            onPressed: onPressed,
          ),
          Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 12,
              color: Theme.of(context).textTheme.bodyText1.color,
            ),
          )
        ],
      ),
    );
  }
}
