import 'package:flutter/material.dart';

/// Basic component that return / show user profile picture
/// Generic picture will be shown if user not set profile picture
class UserAvatar extends StatelessWidget {
  /// Url to user profile picture
  final String profilePicture;

  /// Size of avatar, default size is set 36.0
  final double size;

  /// Callback when user tap on user profile picture
  final VoidCallback onTap;

  const UserAvatar({
    Key key,
    this.profilePicture,
    this.size = 36.0,
    this.onTap = null,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        child: CircleAvatar(
          backgroundColor: Colors.white,
          backgroundImage: profilePicture == null
              ? AssetImage('assets/images/avatar.png') as ImageProvider
              : NetworkImage(profilePicture),
          radius: size,
        ),
        onTap: onTap,
      ),
    );
  }
}
