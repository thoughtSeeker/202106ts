import 'package:flutter/material.dart';

/// Icon button with white backgroud and shadow
class CustomIconButton extends StatelessWidget {
  final VoidCallback onPressed;
  final Widget icon;
  final Color color;

  CustomIconButton({
    Key key,
    this.icon,
    this.onPressed,
    this.color = Colors.transparent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: icon,
        height: 50,
        width: 50,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 6.0,
              )
            ]),
      ),
      onTap: onPressed,
    );
  }
}
