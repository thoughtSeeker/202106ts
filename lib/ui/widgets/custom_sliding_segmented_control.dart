import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Sliding segmented control in appbar
class CustomSlidingSegmentedControl extends StatelessWidget {
  /// Text on the left side
  final String leftText;
  /// Text on the right side
  final String rightText;
  /// Group value of segmented control
  final int value;
  /// Function when sliding is moved
  final Function(int) onValueChanged;

  CustomSlidingSegmentedControl({
    Key key,
    this.leftText,
    this.rightText,
    this.value,
    this.onValueChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(24, 0, 24, 10),
      child: CupertinoSlidingSegmentedControl(
        backgroundColor: Color(0xFFdee1e8),
        onValueChanged: onValueChanged,
        children: <int, Widget>{
          0: Container(
            child: Center(
              child: Text(
                leftText,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: (value == 0)
                      ? Theme.of(context).textTheme.bodyText1.color
                      : Theme.of(context)
                          .textTheme
                          .bodyText1
                          .color
                          .withOpacity(0.5),
                ),
              ),
            ),
            width: 500,
          ),
          1: Container(
            child: Center(
              child: Text(
                rightText,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: (value == 1)
                      ? Theme.of(context).textTheme.bodyText1.color
                      : Theme.of(context)
                          .textTheme
                          .bodyText1
                          .color
                          .withOpacity(0.5),
                ),
              ),
            ),
            width: 500,
          ),
        },
        groupValue: value,
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
      ),
    );
  }
}
