import 'package:flutter/material.dart';


class FollowingButton extends StatelessWidget {
  final VoidCallback onPressed;
  final double width;
  final double fontSize;

  FollowingButton(
      {this.onPressed, this.width, Key key, this.fontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 90,
      height: 36,
      child: FlatButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        color: Colors.blue,
        onPressed: onPressed,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Icon(Icons.check, size: 14, color: Colors.white),
              Text(
                'button__following',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize ?? 12.0,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
