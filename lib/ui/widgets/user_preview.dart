import 'package:flutter/material.dart';

import 'follow_button_outline.dart';
import 'following_button.dart';

/// User Preview with follow button
class UserPreview extends StatelessWidget {
  /// Users first and last name
  final String fullName;

  /// Users bio
  final String bio;

  /// Users rating
  final double rating;

  /// Number of users followers
  final double followers;

  /// User profile picture link
  final String userImg;

  /// True if user is followed
  final bool following;

  UserPreview(this.fullName, this.bio, this.rating, this.followers,
      this.following, this.userImg);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 5, 20, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CircleAvatar(
                radius: 32,
                backgroundColor: Colors.black,
                backgroundImage: NetworkImage(userImg),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    fullName,
                    style: Theme.of(context).textTheme.subtitle2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    bio,
                    style: Theme.of(context).textTheme.subtitle2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.subtitle2,
                      children: [
                        TextSpan(
                          text: rating.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: ' rating - ',
                          style: TextStyle(fontSize: 11),
                        ),
                        TextSpan(
                          text: followers.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text: ' followers',
                          style: TextStyle(fontSize: 11),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              (following)
                  ? FollowingButton(
                      onPressed: () {},
                    )
                  : FollowButtonOutline(
                      onPressed: () {},
                    ),
            ],
          ),
        ),
        Divider()
      ],
    );
  }
}
