import 'dart:async';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'widgets.dart';

/// Bottom sheet for private and group courses
class CourseBottomSheets {
  /// Controller for topic name textfield
  static TextEditingController topicNameController = TextEditingController();

  /// Controller for description textfield
  static TextEditingController descriptionController = TextEditingController();

  /// Controller for price textfield
  static TextEditingController priceController = TextEditingController();

  /// Value of language dropdown
  static String languageValue = '';

  /// Value of currency dropdown
  static String currencyValue = '';

  /// Value of duration dropdown
  static String durationValue = '';

  /// Value of video calls switch
  static bool videoCalls = false;

  /// Value of annonymous callers switch
  static bool anonnymousCallers = false;

  /// Bottom sheet with edit, delete options
  static Future<void> editOrDeleteSheet(BuildContext context) async {
    return await showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        isScrollControlled: false,
        builder: (context) {
          return Wrap(
            children: <Widget>[
              Stack(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(24, 36, 24, 36),
                    child: Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Text(
                            'screen_teacher__modal_delete_course_title',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Text(
                            'screen_teacher__modal_delete_course_subtitle',
                            style: TextStyle(
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .color
                                  .withOpacity(0.7),
                              fontWeight: Theme.of(context)
                                  .textTheme
                                  .subtitle1
                                  .fontWeight,
                              fontSize: 14,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        CustomOutlineButton(
                          text: 'button__edit_course',
                          onPressed: () {
                            Navigator.of(context).pop<void>();
                            editTopicSheet(context);
                          },
                          textStyle: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: Colors.blue,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: CustomRaisedButton(
                            onPressed: () {},
                            text: 'button__delete_course',
                            color: Colors.blue,
                            textStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Positioned(
                    right: 10,
                    top: 24,
                    child: IconButton(
                      icon: Icon(
                        Icons.clear,
                      ),
                      onPressed: () => Navigator.of(context).pop<void>(),
                    ),
                  )
                ],
              ),
            ],
          );
        });
  }

  /// Bottom sheet for adding new topics
  static void addNewTopicSheet(BuildContext context) async {
    topicSheet(context, false);
  }

  /// Loading data for topic from backend
  static void _loadData() {
    topicNameController.text = 'Engilsh Course';
    descriptionController.text =
        'teacher_screen_bottom_sheet_currency_dollar';
    priceController.text = '250';

    languageValue = 'teacher_screen_bottom_sheet_language_english';
    currencyValue = 'teacher_screen_bottom_sheet_currency_dollar';
    durationValue = 'teacher_screen_bottom_sheet_duration_5_minutes';

    videoCalls = true;
    anonnymousCallers = false;
  }

  /// Bottom sheet for editing topic
  static void editTopicSheet(BuildContext context) {
    _loadData();
    topicSheet(context, true);
  }

  /// Bottom sheet
  static void topicSheet(BuildContext context, bool edit) async {
    await showModalBottomSheet<void>(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      isScrollControlled: true,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, StateSetter setState) {
            return Wrap(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(24),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            edit == true
                                // ignore: lines_longer_than_80_chars
                                ? 'screen_teacher__modal_edit_course_topic_title'

                                : 'button__add_new_topic',
                            style: TextStyle(
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                              fontWeight: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .fontWeight,
                              fontSize: 20,
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.clear,
                            ),
                            onPressed: () => Navigator.of(context).pop<void>(),
                          ),
                        ],
                      ),
                      TextField(
                        controller: topicNameController,
                        decoration: InputDecoration(
                          labelText: 'input_hint__topic_name',
                          suffixIcon: BottomSheetTooltip(),
                        ),
                      ),
                      TextField(
                        controller: descriptionController,
                        decoration: InputDecoration(
                          labelText: 'input_hint__description_optional',
                          suffixIcon: BottomSheetTooltip(),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: DropdownButtonFormField(
                          hint: Text(
                            'input_hint__duration',
                          ),
                          onChanged: (String value) {
                            setState(() {
                              durationValue = value;
                            });
                          },
                          items: [
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_duration_5_minutes'
                                ,
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_duration_15_minutes'
                                ,
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_duration_30_minutes'
                                ,
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_duration_45_minutes'
                                ,
                          ]
                              .map(
                                (e) => DropdownMenuItem(
                                  child: Text(e),
                                  value: e,
                                ),
                              )
                              .toList(),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: priceController,
                              decoration: InputDecoration(
                                labelText: 'input_hint__price',
                                suffixIcon: BottomSheetTooltip(),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: DropdownButtonFormField(
                              hint: Text(
                                'input_hint__currency',
                              ),
                              items: [
                                // ignore: lines_longer_than_80_chars
                                'screen_teacher__modal_edit_course_topic_currency_dollar'
                                    ,
                                // ignore: lines_longer_than_80_chars
                                'screen_teacher__modal_edit_course_topic_currency_euro'

                              ]
                                  .map(
                                    (e) => DropdownMenuItem(
                                      child: Text(e),
                                      value: e,
                                    ),
                                  )
                                  .toList(),
                              onChanged: (String value) {
                                setState(() {
                                  currencyValue = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: DropdownButtonFormField(
                          hint: Text(
                            'input_hint__language',
                          ),
                          onChanged: (String value) {
                            setState(() {
                              languageValue = value;
                            });
                          },
                          items: [
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_language_english'
                                ,
                            // ignore: lines_longer_than_80_chars
                            'screen_teacher__modal_edit_course_topic_language_german'
                                ,
                          ]
                              .map(
                                (e) => DropdownMenuItem(
                                  child: Text(e),
                                  value: e,
                                ),
                              )
                              .toList(),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: SvgPicture.asset(
                              'assets/icons/video.svg',
                              width: 20.0,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'button__allow_video_call',
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                          CupertinoSwitch(
                            activeColor: Colors.blue,
                            value: videoCalls,
                            onChanged: (value) {
                              setState(() {
                                videoCalls = value;
                              });
                            },
                          ),
                        ],
                      ),
                      Divider(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: SvgPicture.asset(
                              'assets/icons/anons_allowed.svg',
                              width: 20.0,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              // ignore: lines_longer_than_80_chars
                              'button__allow_anonymous_callers',
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                          CupertinoSwitch(
                            activeColor: Colors.blue,
                            value: anonnymousCallers,
                            onChanged: (value) {
                              setState(() {
                                anonnymousCallers = value;
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      edit == false
                          ? CustomRaisedButton(
                              onPressed: () {},
                              text: 'button__add_topic',
                              color: Colors.blue,
                              textStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                              ),
                            )
                          : Container(
                              child: Column(
                                children: [
                                  CustomRaisedButton(
                                    onPressed: () {},
                                    text: 'button__save_changes',
                                    color: Colors.blue,
                                    textStyle: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                    ),
                                  ),
                                  CustomFlatButton(
                                    onPressed: () =>
                                        Navigator.of(context).pop<void>(),
                                    text: 'button__cancel',
                                    textStyle: TextStyle(
                                      color: Colors.blue,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}

/// Tooltip for question icon of bottom sheet
class BottomSheetTooltip extends StatefulWidget {
  @override
  BottomSheetTooltipState createState() => BottomSheetTooltipState();
}

class BottomSheetTooltipState extends State<BottomSheetTooltip> {
  OverlayEntry _overlayEntry;

  /// Flag for disabling double tooltip
  bool pressed = false;

  /// Globalkey of widget
  final GlobalKey _key = GlobalKey();

  void onPressed() {
    final RenderBox renderBox =
        _key.currentContext.findRenderObject() as RenderBox;
    final position = renderBox.localToGlobal(Offset.zero);

    setState(() {
      pressed = true;
    });
    _overlayEntry = OverlayEntry(builder: (context) => _Tooltip(position));
    Overlay.of(context).insert(_overlayEntry);
    Timer(Duration(seconds: 2), () => _overlayEntry.remove());
    setState(() {
      pressed = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      key: _key,
      onPressed: pressed == false ? onPressed : null,
      icon: SvgPicture.asset(
        'assets/icons/question_circle.svg',
        width: 20.0,
      ),
    );
  }
}

/// Design of tooltip
class _Tooltip extends StatelessWidget {
  /// Position on screen
  final Offset position;
  _Tooltip(this.position);
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: position.dy + 60,
      left: 24,
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width - 48,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 6.0,
              ),
            ],
          ),
          child: Center(
            child: Text(
              'screen_teacher__modal_delete_course_subtitle',
              style: TextStyle(
                fontSize: 14,
                fontWeight: Theme.of(context).textTheme.subtitle2.fontWeight,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
