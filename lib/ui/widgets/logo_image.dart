import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class LogoImage extends StatelessWidget {
  final double width;

  const LogoImage({Key key, this.width}) : super(key: key);

  Widget showImage(BuildContext context, String imageName) {
    double width2 = MediaQuery.of(context).size.width / 2.1;
    return Container(
      margin: const EdgeInsets.all(0.0),
      height: MediaQuery.of(context).size.height - 100,
      width: width2,
      child: Center(),
      decoration: BoxDecoration(
        color: const Color(0xfffffff),
        image: DecorationImage(
          image: AssetImage(imageName),
          fit: BoxFit.scaleDown,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('->29  buildContext logo_image');

    String imagename = 'assets/images/logo.png';
    return showImage(context, imagename);
  }
}
