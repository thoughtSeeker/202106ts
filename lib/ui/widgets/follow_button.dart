
import 'package:flutter/material.dart';

import 'custom_raised_button.dart';

class FollowButton extends StatelessWidget {
  final VoidCallback onPressed;

  FollowButton({this.onPressed, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomRaisedButton(
      text: 'button__follow',
      onPressed: onPressed,
      width: 100,
      height: 36,
      textStyle: TextStyle(
        fontSize: 14,
        color: Colors.white,
      ),
      color: Colors.blue,
    );
  }
}
