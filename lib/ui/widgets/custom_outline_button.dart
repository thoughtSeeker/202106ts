import 'package:flutter/material.dart';

class CustomOutlineButton extends StatelessWidget {
  final Widget icon;
  final String text;
  final TextStyle textStyle;
  final double width, height;
  final VoidCallback onPressed;

  const CustomOutlineButton({
    Key key,
    this.icon,
    this.text,
    this.textStyle = const TextStyle(fontSize: 18),
    this.width = 200,
    this.height = 50,
    this.onPressed,
  }) : super(key: key);

  Widget _icon() {
    if (icon != null) {
      return Positioned(
        left: 0.0,
        child: icon,
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.infinity,
      height: height ?? 44.0,
      child: OutlineButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6.0),
        ),
        borderSide: BorderSide(
          width: 1.0,
          color: textStyle?.color?.withOpacity(0.5) ??
              Colors.white.withOpacity(0.5),
          style: BorderStyle.solid,
        ),
        onPressed: onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              color: Colors.transparent,
            ),
            _icon(),
            Text(
              text,
              style: TextStyle(
                color: textStyle?.color ?? Colors.white,
                fontSize: textStyle?.fontSize ?? 16.0,
                height: textStyle?.height ?? 20.0 / 16.0,
                fontStyle: textStyle?.fontStyle ?? FontStyle.normal,
                fontWeight: textStyle?.fontWeight ?? FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
