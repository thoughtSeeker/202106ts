import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EuroSign{

  static String getEuroSign() {
    Runes input = Runes(' \u{20AC}'); // euro symbol
    String s = String.fromCharCodes(input);
    return s;
  }

}
