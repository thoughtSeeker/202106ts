// import 'package:flutter/material.dart';
// import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
//
// class EmailProviders extends StatefulWidget {
//   static const List<String> _emailProvidersDefault = [
//     '@gmail.com',
//     '@yahoo.com',
//     '@outlook.com',
//     '@icloud.com',
//   ];
//
//   final List<String> emailProviders;
//
//   const EmailProviders({
//     Key key,
//     this.emailProviders = _emailProvidersDefault,
//   }) : super(key: key);
//
//   @override
//   _EmailProvidersState createState() => _EmailProvidersState();
// }
//
// class _EmailProvidersState extends State<EmailProviders> {
//   bool _keyboardState;
//
//   @override
//   void initState() {
//     super.initState();
//     _keyboardState = KeyboardVisibility.isVisible;
//     KeyboardVisibility.onChange.listen((bool visible) {
//       setState(() {
//         _keyboardState = visible;
//       });
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return !_keyboardState
//         ? Container()
//         : Container(
//             height: 36.0,
//             width: double.infinity,
//             child: ListView.builder(
//               scrollDirection: Axis.horizontal,
//               itemCount: widget.emailProviders.length,
//               itemBuilder: (context, index) {
//                 final String item = widget.emailProviders[index];
//                 return Row(
//                   children: [
//                     Container(
//                       height: 36.0,
//                       padding: EdgeInsets.symmetric(
//                         vertical: 9.0,
//                         horizontal: 12.0,
//                       ),
//                       decoration: BoxDecoration(
//                         color: Colors.grey[300],
//                         borderRadius: BorderRadius.circular(6.0),
//                       ),
//                       child: Align(
//                         alignment: Alignment.center,
//                         child: Text(
//                           item,
//                           style: const TextStyle(
//                             color: Color(0xFF172B4D),
//                             fontSize: 14.0,
//                             fontWeight: FontWeight.w500,
//                             height: 1.4,
//                           ),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 6.0,
//                     ),
//                   ],
//                 );
//               },
//             ),
//           );
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//   }
// }
