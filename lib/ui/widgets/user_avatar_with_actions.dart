import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:logging/logging.dart';
import 'package:thoughtseekers/ui/widgets/user_avatar.dart';

import '../../blocs/blocs.dart';
import '../../models/models.dart';
import '../themes/themes.dart';
import 'widgets.dart';

class UserAvatarWithActions extends StatefulWidget {
  final bool showPlusButton;
  final bool showTextButton;

  const UserAvatarWithActions({
    Key key,
    this.showPlusButton = false,
    this.showTextButton = false,
  }) : super(key: key);

  @override
  _UserAvatarWithActionsState createState() => _UserAvatarWithActionsState();
}

class _UserAvatarWithActionsState extends State<UserAvatarWithActions> {
  final _logger = Logger('UserAvatarWithActions');

  EditProfileCubit _editProfileCubit;

  PickedFile _imageFile;
  dynamic _pickImageError;
  String _retrieveDataError;

  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      return Image.file(File(_imageFile.path));
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _imageFile = response.file;
      });
    } else {
      _retrieveDataError = response.exception.code;
    }
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final User user =
        (context.read<AuthenticationBloc>().state as AuthenticationSuccess)
            .user;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Stack(
          children: <Widget>[
            // User Profile picture widget
            UserAvatar(
              profilePicture: user.profilePicture,
            ),
            // Add image button widget
            widget.showPlusButton
                ? Positioned(
                    bottom: 0.0,
                    right: 0.0,
                    child: InkWell(
                      child: Stack(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Colors.white,
                            radius: 10.0,
                          ),
                          Icon(
                            Icons.add_circle,
                            size: 20.0,
                            color: ThemeColors.lightBlue,
                          ),
                        ],
                      ),
                      onTap: _changeImage,
                    ),
                  )
                : Container(),
          ],
        ),
        widget.showTextButton ? const SizedBox(height: 5.0) : Container(),
        widget.showTextButton
            ? CustomFlatButton(
                text: 'button__change_photo',
                textStyle: TextStyle(
                  color: ThemeColors.darkBlue,
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
                onPressed: _changeImage,
              )
            : Container(),
      ],
    );
  }

  /// Pop up with camera options
  void _changeImage() async {
    await showCupertinoModalPopup<void>(
      context: context,
      builder: (context) {
        return CupertinoActionSheet(
          title: Text('label__choose_photo_from'),
          cancelButton: CupertinoActionSheetAction(
            child: Text('button__cancel'),
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            CupertinoActionSheetAction(
              child: Text('button__camera'),
              onPressed: () => _getImage(ImageSource.camera),
            ),
            CupertinoActionSheetAction(
              child: Text('button__gallery'),
              onPressed: () => _getImage(ImageSource.gallery),
            ),
          ],
        );
      },
    );
  }

  /// Function for picking and cropping image
  Future<Null> _getImage(ImageSource source) async {
    try {
      final PickedFile pickedFile = await _picker.getImage(source: source);

      File croppedFile = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(
          ratioX: 1.0,
          ratioY: 1.0,
        ),
        compressQuality: 100,
        maxWidth: 1000,
        maxHeight: 1000,
        cropStyle: CropStyle.rectangle,
        androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'label__crop_your_photo',
          lockAspectRatio: true,
          hideBottomControls: true,
        ),
        iosUiSettings: IOSUiSettings(
          title: 'label__crop_your_photo',
        ),
      );

      await _editProfileCubit.editProfilePhotoSubmitted(croppedFile);
    } catch (e) {
      _logger.fine(e);
    }
  }
}
