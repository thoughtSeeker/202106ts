import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';


/// Linear rating indicator
class LinearRating extends StatelessWidget {
  /// Max rating possible
  final int maxRating;

  /// Current rating
  final double rating;

  /// Title of rating
  final String title;

  /// Flag am I rated
  final bool yourRating;

  /// Width of LinearPercentIndicator
  final double width;

  /// Text if I rated
  String yourRatingText;

  LinearRating({
    Key key,
    @required this.maxRating,
    @required this.rating,
    @required this.title,
    this.yourRating,
    this.width,
  }) : super(key: key) {
    if (yourRating == true)
      yourRatingText = '(${'screen_course_review__your_rating'})';
    else
      yourRatingText = '';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '$title $yourRatingText',
            style: TextStyle(
              fontSize: 12,
              fontWeight: (yourRating == false)
                  ? Theme.of(context).textTheme.subtitle1.fontWeight
                  : Theme.of(context).textTheme.bodyText1.fontWeight,
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                LinearPercentIndicator(
                  backgroundColor: Color(0xFFF2F2F2),
                  progressColor: Color(0xFFFFCC00),
                  lineHeight: 6,
                  percent: rating / maxRating,
                  width: width ?? MediaQuery.of(context).size.width * 0.65,
                ),
                Container(
                  width: 30,
                  child: Text(
                    rating.toStringAsFixed(0),
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: (yourRating == false)
                          ? Theme.of(context).textTheme.subtitle1.fontWeight
                          : Theme.of(context).textTheme.bodyText1.fontWeight,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
