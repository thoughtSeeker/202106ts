
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Button with dashed border and add icon
class CustomOutlineDashedButton extends StatelessWidget {
  /// Function when button is clicked
  final VoidCallback onPress;
  /// Text of button
  final String text;

  CustomOutlineDashedButton({
    Key key,
    this.text,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
    margin: EdgeInsets.symmetric(vertical: 10))
    );
  }
}
