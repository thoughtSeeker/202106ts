import 'package:flutter/material.dart';

/// Navigation pill for top of bottom sheet
class NavigationPill extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 30,
        margin: EdgeInsets.only(bottom: 10),
        height: 5,
        decoration: BoxDecoration(
          color: Theme.of(context).textTheme.bodyText1.color,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
      ),
    );
  }
}
