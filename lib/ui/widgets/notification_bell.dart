import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:thoughtseekers/ui/screens/notification/view/notification_screen.dart';

import '../screens/screens.dart';

/// Creating notification widget
class NotificationBell extends StatefulWidget {
  @override
  _NotificationBellState createState() => _NotificationBellState();
}

class _NotificationBellState extends State<NotificationBell> {
  /// Flag that shows if there are notifications
  bool notification = true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // show / remove badge on tap
      onTap: () {
        setState(() {
          notification = !notification;
        });
        Navigator.of(context).push<void>(
          NotificationScreen.route(),
        );
      },
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 8.0,
              )
            ]),
        child: Center(
          child: Stack(
            children: <Widget>[
              SvgPicture.asset(
                'assets/icons/bell.svg',
              ),
              // Badge widget
              Positioned(
                top: 0,
                right: 0,
                child: (notification != true)
                    ? Container()
                    : CircleAvatar(
                        radius: 4.8,
                        backgroundColor: Color(0xFFFF4267),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
