import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';


import '../../../../../blocs/blocs.dart';
import 'widgets.dart';

class ResetPasswordWizard extends StatefulWidget {
  const ResetPasswordWizard({
    Key key,
  }) : super(key: key);

  @override
  _ResetPasswordWizardState createState() => _ResetPasswordWizardState();
}

class _ResetPasswordWizardState extends State<ResetPasswordWizard> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ResetPasswordCubit, ResetPasswordState>(
      listenWhen: (previous, current) => previous.status != current.status,
      listener: (BuildContext context, state) {
        if (state.status.isSubmissionSuccess) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Color(0xFF2684FF),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child:
                          const Text('screen_reset_password__message_success')
                              ,
                    ),
                    const Icon(
                      Icons.check,
                      size: 44.0,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),
            );
          _navigator.push<void>(StepCompleted.route());
        } else if (state.status.isSubmissionInProgress) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: const Text(
                              'screen_reset_password__message_in_progress')
                          ,
                    ),
                    const CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        } else if (state.status.isSubmissionFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: const Text('screen_reset_password__message_failed')
                          ,
                    ),
                    const Icon(Icons.warning),
                  ],
                ),
              ),
            );
        }
      },
      child: Navigator(
        key: _navigatorKey,
        onGenerateRoute: (_) => StepResetPassword.route(),
      ),
    );
  }
}
