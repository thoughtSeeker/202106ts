import 'package:flutter/material.dart';


class ResetPasswordText extends StatelessWidget {
  final String bodyText1;
  final String bodyText2;
  final TextAlign textAlign;

  const ResetPasswordText({
    Key key,
    @required this.bodyText1,
    @required this.bodyText2,
    this.textAlign = TextAlign.start,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          bodyText1,
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: textAlign,
        ),
        const SizedBox(height: 12.0),
        Text(
          bodyText2,
          style: TextStyle(
            color: Color(0xFF172B4D),
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
            height: 1.4,
          ),
          textAlign: textAlign,
        ),
      ],
    );
  }
}
