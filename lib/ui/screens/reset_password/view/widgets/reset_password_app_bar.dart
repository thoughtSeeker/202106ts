import 'package:flutter/material.dart';


class ResetPasswordAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  final VoidCallback onPressed;
  final AppBar _appBar = AppBar();

  ResetPasswordAppBar({
    Key key,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: Theme.of(context).appBarTheme.iconTheme.size,
          color: Theme.of(context).appBarTheme.iconTheme.color,
        ),
        onPressed: onPressed ?? () => Navigator.of(context).pop(),
      ),
      title: Text(
        'screen_reset_password__step_reset_password_header_title',
        style: Theme.of(context)
            .appBarTheme
            .textTheme
            .headline6
            .copyWith(fontSize: 18.0),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
