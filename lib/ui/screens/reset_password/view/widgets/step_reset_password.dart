
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/ui/screens/welcome/view/welcome_screen.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/reset_password/models/models.dart';
import '../../../../widgets/widgets.dart';
import '../../../screens.dart';
import 'widgets.dart';

class StepResetPassword extends StatelessWidget {
  const StepResetPassword({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepResetPassword(),
    );
  }

  @override
  Widget build(BuildContext context) {
    DeepLink deepLink = (context.read<AuthenticationBloc>().state
            as AuthenticationResetPassword)
        .deepLink;

    print('----------------------------${deepLink.email}');

    return Scaffold(
      appBar: ResetPasswordAppBar(
        onPressed: () => Navigator.of(context).pushAndRemoveUntil<void>(
          WelcomeScreen.route(),
          (route) => false,
        ),
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 40.0,
            ),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 73.0),
                ResetPasswordText(
                  bodyText1:
                      'screen_reset_password__step_reset_password_title',
                  bodyText2:
                      'screen_reset_password__step_reset_password_subtitle'
                          ,
                ),
                const SizedBox(height: 32.0),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    deepLink.email,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
                const SizedBox(height: 10.0),
                _PasswordInput(),
                const SizedBox(height: 10.0),
                _PasswordConfirmedInput(),
                const SizedBox(height: 24.0),
                _ResetPasswordButton(),
                const SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _PasswordInput extends StatefulWidget {
  @override
  __PasswordInputState createState() => __PasswordInputState();
}

class __PasswordInputState extends State<_PasswordInput> {
  FocusNode _focusNode;
  TextEditingController _passwordController;

  String _hintText = 'input_hint__enter_new_password';
  String _labelText = 'input_label__enter_new_password';

  ResetPasswordCubit _resetPasswordCubit;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _resetPasswordCubit = BlocProvider.of<ResetPasswordCubit>(context);
    _passwordController = TextEditingController(
      text: _resetPasswordCubit.state.password.value,
    )..addListener(_onPasswordValueChanged);
    _focusNode = FocusNode()..addListener(_onPasswordFocusChanged);
  }

  void _onPasswordFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__enter_new_password';
        _labelText = 'input_label__enter_new_password';
      });
    } else {
      if (_passwordController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__enter_new_password';
          _labelText = 'input_hint__enter_new_password';
        });
      }
    }
  }

  void _onPasswordValueChanged() {
    _resetPasswordCubit.resetPasswordPasswordChanged(_passwordController.text);
  }

  String _populatePasswordErrorText(Password password) {
    if (password.invalid) {
      switch (password.error) {
        case PasswordValidationError.invalidFormat:
          return 'input_error__invalid_format_enter_password';

        default:
          print('Exception (_RESET_PASSWORD_): ${password.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          obscureText: !_showPassword,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
            errorText: _populatePasswordErrorText(state.password),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _passwordController,
        );
      },
    );
  }
}

class _PasswordConfirmedInput extends StatefulWidget {
  @override
  __PasswordConfirmedInputState createState() =>
      __PasswordConfirmedInputState();
}

class __PasswordConfirmedInputState extends State<_PasswordConfirmedInput> {
  FocusNode _focusNode;
  TextEditingController _passwordConfirmedController;

  String _hintText = 'input_hint__enter_confirm_new_password';
  String _labelText = 'input_label__enter_confirm_new_password';

  ResetPasswordCubit _resetPasswordCubit;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _resetPasswordCubit = BlocProvider.of<ResetPasswordCubit>(context);
    _passwordConfirmedController = TextEditingController(
      text: _resetPasswordCubit.state.password.value,
    )..addListener(_onPasswordConfirmedValueChanged);
    _focusNode = FocusNode()..addListener(_onPasswordConfirmedFocusChanged);
  }

  void _onPasswordConfirmedFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__enter_confirm_new_password';
        _labelText = 'input_label__enter_confirm_new_password';
      });
    } else {
      if (_passwordConfirmedController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__enter_confirm_new_password';
          _labelText = 'input_hint__enter_confirm_new_password';
        });
      }
    }
  }

  void _onPasswordConfirmedValueChanged() {
    _resetPasswordCubit.resetPasswordPasswordConfirmedChanged(
      _passwordConfirmedController.text,
    );
  }

  String _populatePasswordConfirmedErrorText(
      PasswordConfirmed passwordConfirmed) {
    if (passwordConfirmed.invalid) {
      switch (passwordConfirmed.error) {
        case PasswordConfirmedValidationError.mismatch:
          return 'input_error__mismatch_enter_confirm_new_password';

        default:
          print(
            'Exception (_RESET_PASSWORD_): '
            '${passwordConfirmed.error.toString()}',
          );
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _passwordConfirmedController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordState>(
//      buildWhen: (previous, current) =>
//          previous.passwordConfirmed != current.passwordConfirmed,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          obscureText: !_showPassword,
          enabled: !state.password.invalid,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
            errorText:
                _populatePasswordConfirmedErrorText(state.passwordConfirmed),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _passwordConfirmedController,
        );
      },
    );
  }
}

class _ResetPasswordButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'button__reset_password',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.status.isValidated
              ? () => {
                    FocusScope.of(context).unfocus(),
                    context.read<ResetPasswordCubit>().resetPasswordSubmitted(),
                  }
              : null,
        );
      },
    );
  }
}
