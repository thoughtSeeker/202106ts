import 'package:flutter/material.dart';


class NotificationWidget extends StatelessWidget {
  /// Name of user from notification
  final String name;

  /// Profile picture
  final String imgUrl;

  /// Payment amout
  final String payment;

  /// Time of notification
  final String time;

  /// Flag if notification is already seen
  final bool seen;

  /// List of words
  List<String> words = [];

  NotificationWidget(
      {Key key,
      @required this.name,
      @required this.payment,
      @required this.imgUrl,
      @required this.time,
      @required this.seen})
      : super(key: key) {
    String text = 'Notification received payment';
    List<String> temp = text.split('\$$payment');
    words.add(temp[0]);
    words.add('\$$payment');
    temp = temp[1].split(name);
    words.add(temp[0]);
    words.add(name);
    words.add(temp[1]);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: <Widget>[
              CircleAvatar(
                radius: 32,
                backgroundColor: Theme.of(context).textTheme.bodyText1.color,
                backgroundImage: NetworkImage(imgUrl),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 10, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                            style: TextStyle(
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .fontWeight,
                                fontSize: 12,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .color),
                            children: <TextSpan>[
                              TextSpan(text: words[0]),
                              TextSpan(
                                  text: words[1],
                                  style: TextStyle(color: Colors.blue)),
                              TextSpan(text: words[2]),
                              TextSpan(
                                  text: words[3],
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              TextSpan(text: words[4]),
                            ]),
                      ),
                      SizedBox(height: 8),
                      Text(
                        time,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: Theme.of(context)
                                .textTheme
                                .subtitle2
                                .fontWeight,
                            color: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .color
                                .withOpacity(0.8)),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (seen == false)
                        ? Colors.transparent
                        : Color(0xFFFF4267)),
              )
            ],
          ),
        ),
        Divider()
      ],
    );
  }
}
