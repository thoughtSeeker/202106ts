import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../widgets/widgets.dart';
import 'widgets/widgets.dart';

/// Screen for user's notifications
class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const NotificationScreen(),
    );
  }

  @override
  NotificationScreenState createState() => NotificationScreenState();
}

class NotificationScreenState extends State<NotificationScreen> {
  /// Flag used for bottom bar
  bool showBottomBar = true;

  /// List of widgets for notification
  List<Widget> notifications = [];

  /// Text for grouping notifications
  Widget _intervalText(String text) {
    return Text(
      text,
      style: TextStyle(
        color: Color(0xFF172B4d).withOpacity(0.3),
      ),
    );
  }

  /// Grouping notifications by date
  // ignore: lines_longer_than_80_chars
  void _groupNotifications(
      int condition, List data, String group, String last) {
    DateTime now = DateTime.now();
    DateTime time = DateTime.parse(data[0]['time'].toString());

    if (data.isNotEmpty && now.difference(time).inDays < condition) {
      notifications.add(_intervalText(group));

      // ignore: lines_longer_than_80_chars
      while (data.isNotEmpty &&
          now
                  .difference(DateTime.parse(data[0]['time'].toString()))
                  .inDays
                  .floor() <
              condition) {
        setState(() {
          notifications.add(NotificationWidget(
              imgUrl: data[0]['imgUrl'].toString(),
              name: data[0]['name'].toString(),
              payment: data[0]['payment'].toString(),
              seen: last != '' &&
                      last != null &&
                      DateTime.parse(data[0]['time'].toString())
                          .isAfter(DateTime.parse(last))
                  ? true
                  : false,
              time: (condition < 3)
                  ? DateFormat('hh:mm a').format(time).toString()
                  : DateFormat('dd/MM/yyyy hh:mm a').format(time).toString()));
        });

        data.removeAt(0);
      }
    }
  }

  /// Loading notifications from backend
  void _loadNotifications() async {
    var temp = {
      {
        'name': 'Jelena Davidson',
        'payment': 860,
        'time': '2020-08-30 23:56:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Jelena Davidson',
        'payment': 760,
        'time': '2020-08-30 13:52:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Helen Tran',
        'payment': 850,
        'time': '2020-08-26 13:27:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Jelena Davidson',
        'payment': 660,
        'time': '2020-08-22 23:27:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Helen Tran',
        'payment': 880,
        'time': '2020-08-22 13:27:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Jelena Davidson',
        'payment': 960,
        'time': '2020-08-12 03:27:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      },
      {
        'name': 'Helen Tran',
        'payment': 160,
        'time': '2020-07-27 03:27:00',
        'imgUrl': 'https://tinyurl.com/y62f5ft8'
      }
    };

    List<Map<String, dynamic>> data = temp.toList();

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String lastNotification = '';

    try {
      lastNotification = preferences.getString('last_notification_time');
    } catch (e) {
      print('Exception $e');
    }

    _groupNotifications(
        1, data, 'screen_notification__today', lastNotification);
    _groupNotifications(
        2, data, 'screen_notification__yesterday', lastNotification);
    _groupNotifications(
        7, data, 'screen_notification__this_week', lastNotification);
    _groupNotifications(
        30, data, 'screen_notification__this_month', lastNotification);

    if (data.isNotEmpty) {
      notifications.add(_intervalText('screen_notification__earlier'));
      while (data.isNotEmpty) {
        var notification = data[0];
        setState(() {
          notifications.add(NotificationWidget(
            imgUrl: notification['imgUrl'].toString(),
            name: notification['name'].toString(),
            payment: notification['payment'].toString(),
            seen: lastNotification != '' &&
                lastNotification != null &&
                DateTime.parse(data[0]['time'].toString())
                    .isAfter(DateTime.parse(lastNotification)),
            time: DateFormat('dd/MM/yyyy hh:mm a')
                .format(DateTime.parse(data[0]['time'].toString()))
                .toString(),
          ));
        });
        data.removeAt(0);
      }
    }

    // await preferences.setString('last_notification_time', newLastTime);
  }

  @override
  void initState() {
    super.initState();
    _loadNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'screen_notification__header_title',
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: Theme.of(context).appBarTheme.iconTheme.size,
            color: Theme.of(context).appBarTheme.iconTheme.color,
          ),
          onPressed: () => Navigator.of(context).pop<void>(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.more_horiz,
              color: Theme.of(context).appBarTheme.iconTheme.color,
            ),
            onPressed: () {},
          ),
        ],
      ),
      bottomNavigationBar: (showBottomBar == true)
          ? Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(16, 20, 16, 0),
                  height: 126,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: Color(0xFFF1F2F7))),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.white.withOpacity(0.9),
                          spreadRadius: 80,
                          blurRadius: 80)
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'screen_notification__bottom_bar_text',
                        style: TextStyle(
                          color: Theme.of(context).textTheme.bodyText1.color,
                          fontSize: 12,
                          height: 1.4,
                          fontWeight:
                              Theme.of(context).textTheme.subtitle2.fontWeight,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 8),
                      CustomRaisedButton(
                        text: 'button__complete_profile',
                        width: 143,
                        color: Color(0xFF2684FF),
                        textStyle: TextStyle(fontSize: 14, color: Colors.white),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 5,
                  child: IconButton(
                    icon: Icon(
                      Icons.clear,
                      size: Theme.of(context).appBarTheme.iconTheme.size,
                      color: Theme.of(context).appBarTheme.iconTheme.color,
                    ),
                    onPressed: () {
                      setState(() {
                        showBottomBar = false;
                      });
                    },
                  ),
                )
              ],
            )
          : SizedBox(
              height: 0,
            ),
      body: Container(
        padding: EdgeInsets.fromLTRB(24, 20, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: ListView.builder(
          itemCount: notifications.length,
          itemBuilder: (context, index) {
            return notifications[index];
          },
        ),
      ),
    );
  }
}
