import 'package:flutter/material.dart';

/// Textfield of step four screen
class DescribeItTextField extends StatelessWidget {
  /// Hint text for textfield
  final String hintText;

  /// Controller for textfield
  final TextEditingController controller;

  /// Height
  final double height;

  DescribeItTextField({
    Key key,
    this.hintText,
    this.controller,
    this.height
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
          height: height,
          padding: EdgeInsets.only(right: 20),
          margin: EdgeInsets.only(right: 50),
          decoration: BoxDecoration(border: Border.all(color: Colors.black)),
          child: TextField(
            keyboardType: TextInputType.multiline,
            maxLines: 9,
            controller: controller,
            decoration: InputDecoration(
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              contentPadding:
                  EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
              hintText: hintText,
              hintStyle: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 15.0,
              ),
            ),
          )),
    );
  }
}
