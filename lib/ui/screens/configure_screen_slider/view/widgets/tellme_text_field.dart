import 'package:flutter/material.dart';

/// Textfield of step four screen
class TellMeTextField extends StatelessWidget {
  /// Hint text for textfield
  final String hintText;

  /// Controller for textfield
  final TextEditingController controller;

  TellMeTextField({
    Key key,
    this.hintText,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        width: 200,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(border: Border.all(color: Colors.black)),
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );
  }
}
