import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/widgets/configure_screen_slider_app_bar.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/widgets/describeit_text_field.dart';
import 'package:thoughtseekers/ui/screens/data_input/view/data_input_screen.dart';
import 'package:thoughtseekers/ui/screens/next_one/view/next_one_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_flat_button.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Configure',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ConfigureScreenSliderScreen(),
    );
  }
}

class ConfigureScreenSliderScreen extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => ConfigureScreenSliderScreen(),
    );
  }

  @override
  _ConfigureScreenSliderScreenState createState() =>
      _ConfigureScreenSliderScreenState();
}

class _ConfigureScreenSliderScreenState
    extends State<ConfigureScreenSliderScreen> {
  double horizontalMargin = 10.0;

  double spaceBetweenButtons = 2.0;

  /// Controller for tell me textfield
  TextEditingController tellmeController = TextEditingController();

  /// Controller for describe it textfield
  TextEditingController describeItController = TextEditingController();
  int _radioValue = 0;

  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String firstThingsFirst = "Define your life!";
    String tellMe = "Tell me something important for you in life";

    String describeIt = "Describe it a bit if you want";
    String defineScale = "Define scale";
    String hintText = "The important thing...";

    _handleRadioValueChange(int value) {
      setState(() {
        _radioValue = value;
        switch (_radioValue) {
          case 0:
            break;
          case 1:
            break;
        }
      });
    }

    @override
    void initState() {
      setState(() {
        // always preselected Slider
        _radioValue = 0;
      });
      super.initState();
    }

    double height = 10;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: ConfigureScreenSliderAppBar(),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: horizontalMargin),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Align(
                  alignment: Alignment.center,
                  child: Text(firstThingsFirst,
                      style: TextStyle(color: Colors.black, fontSize: 22))),
              Container(
                height: height,
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(tellMe,
                      style: TextStyle(color: Colors.black, fontSize: 17))),
              Container(
                height: height,
              ),

              /*DescribeItTextField(
                height: 40,
                controller: tellmeController,
                hintText: "The important thing...",
              ),*/
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: 200,
                  // color: Colors.cyan,
                  // color: Color(0xf82bfe9),
                  padding: EdgeInsets.only(right: 20),
                  margin: EdgeInsets.only(right: 50),

                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                  ),
                 /* decoration:
                      BoxDecoration(border: Border.all(color: Colors.black)),*/
                  child: TextField(
                    controller: tellmeController,
                    decoration: InputDecoration(
                      hintText: hintText,
                      hintStyle: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: height,
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(describeIt,
                      style: TextStyle(color: Colors.black, fontSize: 17))),
              Container(
                height: height,
              ),

          Align(
            alignment: Alignment.centerLeft,
            child: Container(
                height: 100,
                padding: EdgeInsets.only(right: 20),
                margin: EdgeInsets.only(right: 50),
                decoration: BoxDecoration(border: Border.all(color: Colors.black)),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 9,
                  controller: describeItController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding:
                    EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                    hintText: "A day in your life where you fully accomplish ${tellmeController.text.toLowerCase()} would be defined by...",
                    hintStyle: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 15.0,
                    ),
                  ),
                )),
          ),


             /* DescribeItTextField(
                height: 140,
                controller: describeItController,
                hintText:
                    "A day in your life where you fully accomplish ${tellmeController.text.toLowerCase()} would be defined by...",
              ),*/
              Container(
                height: height,
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(defineScale,
                      style: TextStyle(color: Colors.black, fontSize: 17))),
              Container(
                height: 2,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new Radio(
                          value: 0,
                          groupValue: _radioValue,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          'Slider',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                        Container(
                          width: 26,
                        ),
                        Image.asset(
                          "assets/images/slider.png",
                          height: 28.0,
                          fit: BoxFit.cover,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new Radio(
                          value: 1,
                          groupValue: _radioValue,
                          onChanged: _handleRadioValueChange,
                        ),
                        new Text(
                          'Number',
                          style: new TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                        Container(
                          width: 20,
                        ),
                        new Text(
                          '1, 2, 3, ...',
                          style: new TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Container(
                height: height,
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomFlatButton(
                    width: buttonWidth(),
                    padding: EdgeInsets.all(
                      2.0,
                    ),
                    text: 'I am finished adding',
                    textStyle: TextStyle(color: Colors.black, fontSize: 14),
                    onPressed: () => Navigator.of(context).push<void>(
                      DataInputScreen.route(),
                    ),
                  ),
                  Container(
                    width: spaceBetweenButtons,
                  ),
                  CustomFlatButton(
                    width: buttonWidth(),
                    padding: EdgeInsets.all(
                      2.0,
                    ),
                    text: 'Next one',
                    textStyle: TextStyle(color: Colors.black, fontSize: 14),
                    onPressed: () => Navigator.of(context).push<void>(
                      ConfigureScreenSliderScreen.route(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }

  double buttonWidth() {
    double ret = 140;
    double width = MediaQuery.of(context).size.width;
    double twice = 8 * horizontalMargin;
    double trueWidth = width - twice - spaceBetweenButtons;
    double half = trueWidth / 2;
    ret = half;
    print('->109 ret = ${ret.toString()} ');

    return ret;
  }
}
