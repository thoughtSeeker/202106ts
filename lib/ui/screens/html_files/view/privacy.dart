import 'dart:async';

import 'package:thoughtseekers/utils/remote_address.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PrivacyHtmlPage extends StatefulWidget{

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => PrivacyHtmlPage(),
    );
  }

  @override
  State<StatefulWidget> createState() {
    return PrivacyHtmlPageState();
  }

}
class PrivacyHtmlPageState extends State<PrivacyHtmlPage>{
  WebViewController webViewController;
  @override
  initState(){
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    String baseUrl = RemoteAddress.base;
    // String baseUrl = "https://dev.aftournaments.com/";
    String url = baseUrl + "StaticPages/privacy-policy.html";
    return Center(
        child: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          javascriptChannels: Set.from([
            JavascriptChannel(
                name: 'PrivacyHtmlPage',
                onMessageReceived: (JavascriptMessage message) {
                })
          ]),
          onWebViewCreated: (WebViewController w) {
            webViewController = w;
          },
        )
    );
  }

}