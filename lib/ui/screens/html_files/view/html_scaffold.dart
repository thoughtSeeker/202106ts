
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/html_files/view/edit_profile_app_bar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HtmlScaffold extends StatefulWidget {
  String url;
  String title;

  HtmlScaffold({Key key, this.url, this.title}) : super(key: key);

  static Route route(String url, String title) {
    return MaterialPageRoute<void>(
      builder: (_) => HtmlScaffold(url: url, title: title),
    );
  }

  @override
  _HtmlScaffoldState createState() => _HtmlScaffoldState();
}

class _HtmlScaffoldState extends State<HtmlScaffold> {
  WebViewController webViewController;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String url = widget.url;
    return Scaffold(
      appBar: EditProfileAppBar(
        title: widget.title,
        // title: 'screen_edit_profile__header_title',
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Center(
            child: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          javascriptChannels: Set.from([
            JavascriptChannel(
                name: 'HtmlScaffold',
                onMessageReceived: (JavascriptMessage message) {})
          ]),
          onWebViewCreated: (WebViewController w) {
            webViewController = w;
          },
        )),
      ),
    );
  }
}
