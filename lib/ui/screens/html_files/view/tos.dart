
import 'package:thoughtseekers/ui/screens/html_files/view/edit_profile_app_bar.dart';
import 'package:thoughtseekers/utils/remote_address.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TosHtmlPage extends StatefulWidget {
  const TosHtmlPage({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const TosHtmlPage(),
    );
  }

  @override
  _TosHtmlPageState createState() => _TosHtmlPageState();
}

class _TosHtmlPageState extends State<TosHtmlPage> {
  WebViewController webViewController;

  @override
  void initState() {
    super.initState();
  }

  /// Light blue color
  Color lightBlue = Color(0xFF2684FF);

  @override
  Widget build(BuildContext context) {
    String baseUrl = RemoteAddress.base;
    String url = baseUrl + "StaticPages/terms-of-service.html";
    return Scaffold(
      appBar: EditProfileAppBar(
        title: 'TOS',
        // title: 'screen_edit_profile__header_title',
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Center(
            child: WebView(
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          javascriptChannels: Set.from([
            JavascriptChannel(
                name: 'TosHtmlPage',
                onMessageReceived: (JavascriptMessage message) {})
          ]),
          onWebViewCreated: (WebViewController w) {
            webViewController = w;
          },
        )),
      ),
    );
  }
}
