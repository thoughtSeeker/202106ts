import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../screens.dart';

class NotificationsSettingsScreen extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => NotificationsSettingsScreen(),
    );
  }

  @override
  NotificationsSettingsScreenState createState() =>
      NotificationsSettingsScreenState();
}

class NotificationsSettingsScreenState
    extends State<NotificationsSettingsScreen> {
  /// Flags for cupertino switches
  List<bool> values = [false, false, false, false, false];

  /// Toogle cupertino switch and save value to storage
  void _toogleAndSave(int index, String key) async {
    setState(() {
      values[index] = !values[index];
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setBool(key, values[index]);
  }

  /// Loading flags from storage
  void _loadData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      values[0] = preferences.getBool('notifications_bookins') ?? false;
      values[1] = preferences.getBool('notifications_payments') ?? false;
      values[2] = preferences.getBool('notifications_followings') ?? false;
      values[3] = preferences.getBool('notifications_reviews') ?? false;
      values[4] = preferences.getBool('notifications_weekly_updates') ?? false;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'screen_notification__header_title',
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: Theme.of(context).appBarTheme.iconTheme.size,
            color: Theme.of(context).appBarTheme.iconTheme.color,
          ),
          onPressed: () => Navigator.of(context).pop<void>(),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(24),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            NotificationsListItem(
              text: 'screen_settings_notifications__list_item_bookings',
              onChanged: (value) {
                _toogleAndSave(0, 'notifications_bookins');
              },
              value: values[0],
            ),
            NotificationsListItem(
              text: 'screen_settings_notifications__list_item_payments',
              onChanged: (value) {
                _toogleAndSave(1, 'notifications_payments');
              },
              value: values[1],
            ),
            NotificationsListItem(
              text: 'screen_settings_notifications__list_item_followings',
              onChanged: (value) {
                _toogleAndSave(2, 'notifications_followings');
              },
              value: values[2],
            ),
            NotificationsListItem(
              text: 'screen_settings_notifications__list_item_reviews',
              onChanged: (value) {
                _toogleAndSave(3, 'notifications_reviews');
              },
              value: values[3],
            ),
            NotificationsListItem(
              text: 'screen_settings_notifications__list_item_weekly_updates'
                  ,
              onChanged: (value) {
                _toogleAndSave(4, 'notifications_weekly_updates');
              },
              value: values[4],
              divider: false,
            ),
            SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () => Navigator.of(context).push<void>(
                Notificationsv2Screen.route(),
              ),
              child: Text(
                'button__ios_guide',
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
