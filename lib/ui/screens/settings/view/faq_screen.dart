import 'package:flutter/material.dart';

import '../../screens.dart';

class FAQScreen extends StatelessWidget {
  const FAQScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const FAQScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'screen_settings_faq__header_title',
        ),
        leading: IconButton(
          icon: Icon(
            Icons.ac_unit_rounded,
            size: Theme.of(context).appBarTheme.iconTheme.size,
            color: Colors.red,
            // color: Theme.of(context).appBarTheme.iconTheme.color,
          ),
          onPressed: () => Navigator.of(context).pop<void>(),
        ),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(24, 20, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: ListView(
          children: [
            TextField(
              decoration: InputDecoration(
                hintText: 'input_hint__ask_a_question',
                prefixIcon: Icon(
                  Icons.search,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                'screen_settings_faq__title',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SettingsListHeader(
              text: 'screen_settings_faq__header_payment_and_billing',
              color: Colors.blue,
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_how_to_use',
              subtitle: 'screen_settings_faq__header_payment_and_billing',
              onTap: (){},
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_supported_payments',
              subtitle: 'screen_settings_faq__header_payment_and_billing',
              onTap: (){},
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_how_to_use',
              subtitle: 'screen_settings_faq__question_book_course',
              onTap: (){},
            ),
            SettingsListHeader(
              text: 'screen_settings_faq__header_general',
              color: Colors.blue,
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_how_to_use',
              subtitle: 'screen_settings_faq__header_general',
              onTap: (){},
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_supported_payments',
              subtitle: 'screen_settings_faq__header_general',
              onTap: (){},
            ),
            QuestionPreview(
              title: 'screen_settings_faq__question_how_to_use',
              subtitle: 'screen_settings_faq__header_general',
              onTap: (){},
            ),
          ],
        ),
      ),
    );
  }
}
