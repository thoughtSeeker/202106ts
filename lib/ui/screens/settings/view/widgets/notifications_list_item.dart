import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Notifications settings list item with cupertino switch
class NotificationsListItem extends StatelessWidget {
  /// Text of item
  final String text;
  /// When switch is changed
  final Function(bool) onChanged;
  /// Value of cupertino switch
  final bool value;
  /// Flag if there is no need for divider
  final bool divider;

  NotificationsListItem({
    Key key,
    this.text,
    this.onChanged,
    this.value,
    this.divider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            text,
            style: TextStyle(
              fontSize: 14,
              fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
              height: 28/14,
            ),
          ),
          trailing: CupertinoSwitch(
            value: value,
            onChanged: onChanged,
            activeColor:Colors.blue,
          ),
        ),
        divider == null || divider == true ? Divider() : Container(),
      ],
    );
  }
}
