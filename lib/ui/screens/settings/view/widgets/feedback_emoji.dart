import 'package:flutter/material.dart';

/// Emoji in feedback bottom sheet
class FeedbackEmoji extends StatelessWidget {
  /// Icon of emoji
  final Widget icon;

  /// Subtitle of emoji
  final String text;

  /// Value of emoji
  final bool value;

  /// Function when emoji is tapped
  final VoidCallback onTap;

  FeedbackEmoji({
    Key key,
    this.icon,
    this.text,
    this.value,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10,bottom: 20),
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              width: 64,
              height: 64,
              decoration: BoxDecoration(
                color: value
                    ? Color(0xFFFFCC00)
                    : Color(0xFF172B4D).withOpacity(0.3),
                shape: BoxShape.circle,
              ),
              child: Center(
                child: icon,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,
                color: value
                    ? Theme.of(context).textTheme.bodyText1.color
                    : Theme.of(context)
                        .textTheme
                        .bodyText1
                        .color
                        .withOpacity(0.3),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
