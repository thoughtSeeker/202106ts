import 'package:flutter/material.dart';


class SettingsAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar _appBar = AppBar();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: Theme.of(context).appBarTheme.iconTheme.size,
          color: Theme.of(context).appBarTheme.iconTheme.color,
        ),
        onPressed: () => Navigator.of(context).pop<void>(),
      ),
      title: Text(
        'Title',
        style: TextStyle(fontSize: 18),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
