
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../widgets/custom_raised_button.dart';
import 'widgets.dart';

/// Bottom sheet for send feedback option
class FeedbackBottomSheet {
  /// Controlled for optional message
  static final TextEditingController _controller = TextEditingController();

  /// Emoji values
  static List<bool> emojiValues = [false, false, false];

  static void _tapEmoji(int index) {
    for (int i = 0; i < 3; i++) {
      if (i == index)
        emojiValues[i] = true;
      else
        emojiValues[i] = false;
    }
  }

  static void showSheet(BuildContext context) async {
    await showModalBottomSheet<void>(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      isScrollControlled: true,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, StateSetter setState) {
            return Wrap(
              children: [
                Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.all(40.0),
                      child: Column(
                        children: [
                          Text(
                            'screen_settings_feedback__title',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 20,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 15.0),
                            child: Text(
                              'screen_settings_feedback__subtitle',
                              style: TextStyle(
                                color: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .color
                                    .withOpacity(0.5),
                                fontWeight: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .fontWeight,
                                fontSize: 14,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              FeedbackEmoji(
                                text:
                                    'screen_settings_feedback__emoji_meh',
                                icon: SvgPicture.asset(
                                  'assets/icons/feedback_meh.svg',
                                  width: 24.0,
                                ),
                                value: emojiValues[0],
                                onTap: () {
                                  setState(() {
                                    _tapEmoji(0);
                                  });
                                },
                              ),
                              FeedbackEmoji(
                                text: 'screen_settings_feedback__emoji_ok',
                                icon: SvgPicture.asset(
                                  'assets/icons/feedback_ok.svg',
                                  width: 24.0,
                                ),
                                value: emojiValues[1],
                                onTap: () {
                                  setState(() {
                                    _tapEmoji(1);
                                  });
                                },
                              ),
                              FeedbackEmoji(
                                text:
                                    'screen_settings_feedback__emoji_love',
                                icon: SvgPicture.asset(
                                  'assets/icons/feedback_love.svg',
                                  width: 24.0,
                                ),
                                value: emojiValues[2],
                                onTap: () {
                                  setState(() {
                                    _tapEmoji(2);
                                  });
                                },
                              )
                            ],
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'screen_settings_feedback__drop_line',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .fontSize,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom,
                            ),
                            child: TextField(
                              controller: _controller,
                              decoration: InputDecoration(
                                hintText: 'input_hint__feedback_hint',
                              ),
                              autofocus: true,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CustomRaisedButton(
                            onPressed: () {},
                            text: 'button__send_your_feedback',
                            color: Colors.blue,
                            textStyle: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      right: 10,
                      top: 25,
                      child: IconButton(
                        icon: Icon(
                          Icons.clear,
                        ),
                        onPressed: () => Navigator.of(context).pop<void>(),
                      ),
                    )
                  ],
                ),
              ],
            );
          },
        );
      },
    );
  }
}
