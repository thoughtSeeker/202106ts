import 'package:flutter/material.dart';

/// Items list on settings screen
class SettingsListItem extends StatelessWidget {
  /// Title of item
  final String text;

  /// Icon leading title
  final Widget icon;

  /// When widget is tapped
  final VoidCallback onTap;

  /// Flag if there is no need for right arrow
  final bool trailing;

  /// Flag if there is no need for divider
  final bool divider;

  SettingsListItem({
    Key key,
     this.text,
     this.icon,
     this.onTap,
    this.trailing,
    this.divider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          contentPadding: EdgeInsets.zero,
          dense: true,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 14,
            ),
          ),
          leading: icon,
          trailing: trailing == null || trailing == true
              ? Icon(Icons.chevron_right)
              : null,
          onTap: onTap,
        ),
        divider == null || divider == true
            ? Divider()
            : SizedBox(
                height: 10.0,
              ),
      ],
    );
  }
}
