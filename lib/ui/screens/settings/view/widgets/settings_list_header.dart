import 'package:flutter/material.dart';

/// Header of settings group
class SettingsListHeader extends StatelessWidget {
  /// Text of header
  final String text;

  /// Color of header
  final Color color;

  SettingsListHeader({
    Key key,
    this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 20.0,
        bottom: 10.0,
      ),
      child: Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 16,
          color: color ?? Theme.of(context).textTheme.bodyText1.color,
        ),
      ),
    );
  }
}
