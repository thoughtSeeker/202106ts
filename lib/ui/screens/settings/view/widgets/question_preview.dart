import 'package:flutter/material.dart';

/// Question preview in faq screen
class QuestionPreview extends StatelessWidget {
  /// Function when question is tapped
  final VoidCallback onTap;
  /// Title of question
  final String title;
  /// Subtitle of question
  final String subtitle;

  QuestionPreview({
    Key key,
    this.onTap,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment:CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      overflow:TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText1.fontWeight,
                        height: 28/14,
                      ),
                    ),
                    Text(
                      subtitle,
                      overflow:TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 12,
                        fontWeight:
                            Theme.of(context).textTheme.bodyText1.fontWeight,
                        height: 20 / 12,
                      ),
                    ),
                  ],
                ),
              ),
              Icon(
                Icons.chevron_right,
              )
            ],
          ),
          Divider(),
        ],
      ),
    );
  }
}
