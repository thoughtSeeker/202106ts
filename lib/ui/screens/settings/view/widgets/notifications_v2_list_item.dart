import 'package:flutter/material.dart';

/// Single list item in notifications v2 screen
class Notificationsv2ListItem extends StatelessWidget {
  /// Icon of widget
  final Widget icon;

  /// Title of widget
  final String title;

  /// Subtitle of widget
  final String subtitle;

  Notificationsv2ListItem({
    Key key,
    this.icon,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20.0),
            height: 48,
            width: 48,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.05),
                  offset: Offset(-1, 2),
                  blurRadius: 3,
                  spreadRadius: 2,
                ),
              ],
            ),
            child: Center(
              child: icon,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight:
                      Theme.of(context).textTheme.bodyText1.fontWeight,
                  height: 28 / 14,
                  color: Colors.blue,
                ),
              ),
              Text(
                subtitle,
                style: TextStyle(
                  fontWeight:
                      Theme.of(context).textTheme.subtitle1.fontWeight,
                  fontSize: 14,
                  color: Theme.of(context).textTheme.bodyText1.color,
                  height: 20 / 14,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
