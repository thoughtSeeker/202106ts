import 'package:flutter/material.dart';

import 'package:flutter_svg/svg.dart';

/// Header of app review screen
///
/// contains overal rating with emoji and text
// ignore: must_be_immutable
class AppReviewHeader extends StatelessWidget {
  /// Overal app rating
  final double overalRating;

  /// Number of rating
  final double numberOfRatings;

  /// Number of reviews
  final double numberOfReviews;

  /// Text of rating
  String textRating;

  /// String with path to smiley
  String ratingSmiley;

  AppReviewHeader({
    Key key,
    this.overalRating,
    this.numberOfRatings,
    this.numberOfReviews,
  }) : super(key: key) {
    if (overalRating == 5.0) {
      textRating = 'screen_app_review__rating_5';
      ratingSmiley = 'assets/icons/app_rating_5.svg';
    } else if (overalRating >= 4.0 && overalRating < 5.0) {
      textRating = 'screen_app_review__rating_4';
      ratingSmiley = 'assets/icons/app_rating_4.svg';
    } else if (overalRating >= 3.0 && overalRating < 4.0) {
      textRating = 'screen_app_review__rating_3';
      ratingSmiley = 'assets/icons/app_rating_3.svg';
    } else if (overalRating >= 2.0 && overalRating < 3.0) {
      textRating = 'screen_app_review__rating_2';
      ratingSmiley = 'assets/icons/app_rating_2.svg';
    } else {
      textRating = 'screen_app_review__rating_1';
      ratingSmiley = 'assets/icons/app_rating_1.svg';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                overalRating.toStringAsFixed(1),
                style: TextStyle(
                  fontSize: 48,
                  //height: 20 / 48,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Stack(
                alignment: Alignment.center,
                children: [
                  Icon(
                    Icons.star,
                    size: 48,
                    color: Color(0xFFFFCC00),
                  ),
                  SvgPicture.asset(ratingSmiley),
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Text(
              textRating,
              style: TextStyle(
                fontSize: 16,
                height: 20 / 16,
                fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
              ),
            ),
          ),
          Text(
            'screen_app_review__rating_subtitle',
            style: TextStyle(
              fontSize: 12,
              height: 20 / 12,
              fontWeight: Theme.of(context).textTheme.subtitle1.fontWeight,
              color:
                  Theme.of(context).textTheme.bodyText1.color.withOpacity(0.7),
            ),
          ),
        ],
      ),
    );
  }
}
