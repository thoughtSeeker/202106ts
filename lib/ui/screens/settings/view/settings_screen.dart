import 'package:thoughtseekers/ui/screens/edit_profile/view/edit_profile_screen.dart';
import 'package:thoughtseekers/ui/screens/html_files/view/html_scaffold.dart';
import 'package:thoughtseekers/ui/screens/html_files/view/privacy.dart';
import 'package:thoughtseekers/ui/screens/html_files/view/tos.dart';
import 'package:thoughtseekers/utils/remote_address.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
// import 'package:social_share/social_share.dart';

import '../../../../blocs/blocs.dart';
import '../../screens.dart';
import 'widgets/widgets.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const SettingsScreen(),
    );
  }

  /// Color for the icons
  static const Color iconColor = Color(0xFF8f95a3);

  /// dialog for show logout functionality
  void _showLogOutDialog(BuildContext context) async {
    AuthenticationBloc _authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);

    await showDialog<void>(
      context: context, builder: (context) => CupertinoAlertDialog(
        title: Text('Log Out'),
        content: Text('Logging out enables you to enter the app with another login credentials'),
        actions: <Widget>[
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('Cancel'),
          ),
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () {
              _authenticationBloc.add(AuthenticationLoggedOut());
            },
            child: Text('Logout'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: SettingsAppBar(),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: ListView(
          padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
          children: <Widget>[/*
            SettingsListHeader(
              text: 'Name',
            ),*/
            SettingsListItem(
              text: 'Name',
              icon: SvgPicture.asset(
                'assets/icons/profile.svg',
                color: Color(0xFF172B4D).withOpacity(0.5),
              ),
              onTap: () => Navigator.of(context).push<void>(
                EditProfileScreen.route(),
              ),
            ),

            SettingsListItem(
              text: 'Password',
              icon: SvgPicture.asset(
                'assets/icons/profile.svg',
                color: Color(0xFF172B4D).withOpacity(0.5),
              ),
              onTap: () => Navigator.of(context).push<void>(
                EditProfileScreen.route(),
              ),
            ),

            SettingsListItem(
              text: 'Terms and conditions',
              icon: SvgPicture.asset('assets/icons/terms.svg'),
              onTap: () => Navigator.of(context).push<void>(
                HtmlScaffold.route(
                    RemoteAddress.base + "/eula.html",
                   "Terms of Service"),
              ),
            ),

            SettingsListItem(
              text: 'Logout',
              icon: SvgPicture.asset('assets/icons/exit.svg'),
              onTap: () => _showLogOutDialog(context),
              trailing: false,
              divider: false,
            ),
            SettingsListItem(

              text: 'Contact the team',
              icon: SvgPicture.asset('assets/icons/lock.svg'),
              onTap: () => Navigator.of(context).push<void>(
                HtmlScaffold.route(RemoteAddress.base + "fish-users/getbypagename/eula.html",
                    "Privacy Policy"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
