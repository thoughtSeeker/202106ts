import 'package:flutter/material.dart';

import 'widgets/notifications_v2_list_item.dart';

class Notificationsv2Screen extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => Notificationsv2Screen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'screen_settings_notifications_v2__header_title',
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            size: Theme.of(context).appBarTheme.iconTheme.size,
            color: Theme.of(context).appBarTheme.iconTheme.color,
          ),
          onPressed: () => Navigator.of(context).pop<void>(),
        ),
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(24,20,24,0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          children: [
            Image.asset(
              'assets/images/notificationsv2.png',
            ),
            Text(
              'screen_settings_notifications_v2__title',
              style: TextStyle(
                fontSize: 14,
                fontWeight: Theme.of(context).textTheme.bodyText1.fontWeight,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical:10),
              child: Text(
                'screen_settings_notifications_v2__subtitle',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: Theme.of(context).textTheme.subtitle1.fontWeight,
                  height: 20/14,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Notificationsv2ListItem(
              icon: Image.asset(
                'assets/images/notificationsv2_step_one.png',
              ),
              subtitle:
                  'screen_settings_notifications_v2__list_item_one',
              title:
                  'screen_settings_notifications_v2__step',
            ),
            Notificationsv2ListItem(
              icon: Image.asset(
                'assets/images/notificationsv2_step_two.png',
              ),
              subtitle:
                  'screen_settings_notifications_v2__list_item_two',
              title:
                  'screen_settings_notifications_v2__step',
            ),
            Notificationsv2ListItem(
              icon: Image.asset(
                'assets/images/notificationsv2_step_three.png',
              ),
              subtitle:
                  'screen_settings_notifications_v2__list_item_three',
              title: 'screen_settings_notifications_v2__step'
              ,
            ),
            Notificationsv2ListItem(
              icon: Image.asset(
                'assets/images/notificationsv2_step_four.png',
              ),
              subtitle:
                  'screen_settings_notifications_v2__list_item_four',
              title:
                  'screen_settings_notifications_v2__step',
            ),
          ],
        ),
      ),
    );
  }
}
