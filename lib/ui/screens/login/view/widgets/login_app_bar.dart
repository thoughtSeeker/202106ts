import 'package:flutter/material.dart';


import '../../../../widgets/widgets.dart';
import '../../../screens.dart';

class LoginAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar _appBar = AppBar();

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
    /*  leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: Theme.of(context).appBarTheme.iconTheme.size,
          color: Theme.of(context).appBarTheme.iconTheme.color,
        ),
        // onPressed: () => Navigator.of(context).pop<void>(),
      ),*/
      title: Text(
        'Thought Seekers',
        style: Theme.of(context)
            .appBarTheme
            .textTheme
            .headline6
            .copyWith(fontSize: 18.0),
      ),
     /* actions: <Widget>[
        CustomFlatButton(
          padding: EdgeInsets.only(
            right: 2.0,
          ),
          text: 'button__register',
          textStyle: Theme.of(context).appBarTheme.textTheme.headline6,
          onPressed: () => Navigator.of(context).push<void>(
            RegisterScreen.route(),
          ),
        ),
      ],*/
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
