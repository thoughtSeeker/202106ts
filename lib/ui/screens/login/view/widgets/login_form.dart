
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/ui/screens/forgot_password/view/forgot_password_screen.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../widgets/widgets.dart';
import '../../../screens.dart';

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (BuildContext context, state) {
        if (state.status.isSubmissionSuccess) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Color(0xFF2684FF),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('You have successfully logged in'),
                    const Icon(
                      Icons.check,
                      size: 44.0,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),
            );
        } else if (state.status.isSubmissionInProgress) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Logging in progress'),
                    const CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        } else if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Login failed'),
                    const Icon(Icons.warning),
                  ],
                ),
              ),
            );
        }
      },
      child: Container(
        color:Colors.red,
        child: Column(
          children: <Widget>[
            _EmailInput(),
            const SizedBox(height: 10.0),
            _PasswordInput(),
            const SizedBox(height: 24.0),
            _LoginButton(),
            const SizedBox(height: 24.0),
            Padding(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom + 20.0,
              ),
              child: Container(
                color:Colors.blue,
                child: _ForgotPasswordButton(),
                /*CustomFlatButton(
                  text: 'Forgot password',
                  textStyle: const TextStyle(
                    color: Colors.white,
                    // color: Color(0xFF2684FF),
                  ),
                  onPressed: () => Navigator.of(context)
                      .push<void>(ForgotPasswordScreen.route()),
                ),*/
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _EmailInput extends StatefulWidget {
  @override
  __EmailInputState createState() => __EmailInputState();
}

class __EmailInputState extends State<_EmailInput> {
  FocusNode _focusNode;
  TextEditingController _emailController;
  String _hintText = 'Enter email address';
  String _labelText = 'Enter email address';

  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController = TextEditingController(
      text: _loginBloc.state.email.value,
    )..addListener(_onEmailValueChanged);
    _focusNode = FocusNode()..addListener(_onEmailFocusChanged);
  }

  void _onEmailFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter email address';
        _labelText = 'Enter email address';
      });
    } else {
      if (_emailController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter email address';
          _labelText = 'Enter email address';
        });
      }
    }
  }

  void _onEmailValueChanged() {
    _loginBloc.add(
      LoginEmailChanged(email: _emailController.text),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
          ),
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _emailController,
          onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        );
      },
    );
  }
}

class _PasswordInput extends StatefulWidget {
  @override
  __PasswordInputState createState() => __PasswordInputState();
}

class __PasswordInputState extends State<_PasswordInput> {
  FocusNode _focusNode;
  TextEditingController _passwordController;

  String _hintText = 'Enter password';
  String _labelText = 'Enter password';

  LoginBloc _loginBloc;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _passwordController = TextEditingController(
      text: _loginBloc.state.password.value,
    )..addListener(_onPasswordValueChanged);
    _focusNode = FocusNode()..addListener(_onPasswordFocusChanged);
  }

  void _onPasswordFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter password';
        _labelText = 'iEnter password';
      });
    } else {
      if (_passwordController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter password';
          _labelText = 'Enter password';
        });
      }
    }
  }

  void _onPasswordValueChanged() {
    _loginBloc.add(
      LoginPasswordChanged(
        password: _passwordController.text,
      ),
    );
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (BuildContext context, state) {
        return TextFormField(
          obscureText: !_showPassword,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
          ),
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _passwordController,
        );
      },
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'Login',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.status.isValidated
              ? () => {
                    FocusScope.of(context).unfocus(),
                    context.read<LoginBloc>().add(LoginSubmitted()),
                  }
              : null,
        );
      },
    );
  }
}


class _ForgotPasswordButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'Forgot password',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.status.isValidated
              ? () => {
            FocusScope.of(context).unfocus(),
            // todo
            context.read<LoginBloc>().add(LoginSubmitted()),
          }
              : null,
        );
      },
    );
  }
}
