import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/ui/widgets/city_background.dart';

import '../../../../blocs/blocs.dart';
import '../../../widgets/widgets.dart';
import 'widgets/widgets.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const LoginScreen(),
    );
  }

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  bool keyboardVisible = true;
  @override
  Widget build(BuildContext context) {
    Container eraseKeyboard() {
      FocusScope.of(context).requestFocus(new FocusNode());
      SystemChannels.textInput.invokeMethod('TextInput.hide');
      return Container();
    }
    if(keyboardVisible){
      keyboardVisible = false;
      eraseKeyboard();
    }
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: LoginAppBar(),
        body: Container(
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 40.0),
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    const SizedBox(height: 73.0),
                    LoginText(),
                    const SizedBox(height: 32.0),
                    BlocProvider(
                      create: (BuildContext context) => LoginBloc(
                        authenticationBloc:
                            context.read<AuthenticationBloc>(),
                      ),
                      child: LoginForm(),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
