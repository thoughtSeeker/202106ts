import 'package:flutter/material.dart';

import 'package:thoughtseekers/ui/screens/new_user/view/new_user_screen.dart';
import 'package:thoughtseekers/ui/screens/search/view/search_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_bottom_navigation_bar.dart';

import '../../../widgets/widgets.dart';
import '../../screens.dart';

class HomeScreen extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => HomeScreen(),
    );
  }

  @override
  State createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ///
  final int _homeIndex = 0;

  ///
  final int _searchIndex = 2;

  ///
  int _selectedIndex;

  @override
  void initState() {
    _selectedIndex = _homeIndex;
    super.initState();
  }

  void _onItemTapped(int index) {
    if (index == _searchIndex) {
      showSearch<dynamic>(
        context: context,
        delegate: SearchPageDelegate(),
      );
    }
    setState(() {
      _selectedIndex = index != _searchIndex ? index : _selectedIndex;
    });
  }

  ///
  final List<Widget> _widgetOptions = <Widget>[
    NewUserScreen(),
    // BookingScreen(),
    Container(), // SearchScreen(),
    // FavoriteScreen(),
    SettingsScreen(),
  ];

  ///
  final List<CustomBottomNavigationBarItem> items = [
    CustomBottomNavigationBarItem(
      iconPath: 'assets/icons/home.svg',
      title: Text('button__bottom_navigation_bar_item_home'),
    ),
    CustomBottomNavigationBarItem(
      iconPath: 'assets/icons/calendar.svg',
      title: Text('button__bottom_navigation_bar_item_bookings'),
    ),
    CustomBottomNavigationBarItem(
      iconPath: 'assets/icons/search.svg',
      title: Text('button__bottom_navigation_bar_item_search'),
    ),
    CustomBottomNavigationBarItem(
      iconPath: 'assets/icons/heart.svg',
      title: Text('button__bottom_navigation_bar_item_favorites'),
    ),
    CustomBottomNavigationBarItem(
      iconPath: 'assets/icons/profile.svg',
      title: Text('button__bottom_navigation_bar_item_settings'),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        // check if current tab
        // back to home tab if current tab not home
        // close app if current tab is home tab
        bool willPop = _selectedIndex == _homeIndex;
        setState(() {
          _selectedIndex = _homeIndex;
        });
        return Future<bool>.value(willPop);
      },
      child: Scaffold(
        // body: _widgetOptions.elementAt(_selectedIndex),
        body: IndexedStack(
          index: _selectedIndex,
          children: _widgetOptions,
        ),
        bottomNavigationBar: CustomBottomNavigationBar(
          onTap: _onItemTapped,
          curve: Curves.easeInBack,
          items: items,
          activeColor: Color(0xFF2684FF),
          inactiveColor: Color(0xFF172B4D),
          currentIndex: _selectedIndex,
        ),
      ),
    );
  }
}
