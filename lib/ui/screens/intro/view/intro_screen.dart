import 'package:flutter/material.dart';

import 'package:thoughtseekers/ui/screens/welcome/view/welcome_screen.dart';

import '../../../widgets/widgets.dart';
import '../../screens.dart';
import 'widgets/widgets.dart';

class IntroScreen extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => IntroScreen(),
    );
  }

  @override
  State createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  int _selectedPageIndex;

  PageController _controller;

  static const _kDuration = Duration(milliseconds: 300);

  static const _kCurve = Curves.ease;

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = 0;
    _controller = PageController(
      initialPage: _selectedPageIndex,
    );
  }

  final List<Widget> _pages = <Widget>[
    StepOne(),
    StepTwo(),
    StepThree(),
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: [
          CustomFlatButton(
            padding: EdgeInsets.symmetric(
              horizontal: 40.0,
            ),
            text: 'Skip',
            textStyle: TextStyle(
              color: Color(0xFF2684FF),
              fontWeight: FontWeight.w600,
            ),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil<void>(
                WelcomeScreen.route(),
                (route) => false,
              );
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          width: size.width,
          height: size.height,
          color: Colors.white,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              PageView(
                controller: _controller,
                children: _pages,
                onPageChanged: (pageIndex) {
                  setState(() {
                    _selectedPageIndex = pageIndex;
                  });
                },
              ),
              Positioned(
                bottom: 35.0,
                left: 40.0,
                right: 40.0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    DotsIndicator(
                      controller: _controller,
                      itemCount: _pages.length,
                      onPageSelected: (int pageIndex) {
                        _controller.animateToPage(
                          pageIndex,
                          duration: _kDuration,
                          curve: _kCurve,
                        );
                      },
                    ),
                    Container(
                      width: 56.0,
                      height: 56.0,
                      child: FlatButton(
                        onPressed: () {
                          if (_selectedPageIndex >= _pages.length - 1) {
                            Navigator.of(context).pushAndRemoveUntil<void>(
                              WelcomeScreen.route(),
                              (route) => false,
                            );
                          } else {
                            _controller.animateToPage(
                              _selectedPageIndex + 1,
                              duration: _kDuration,
                              curve: _kCurve,
                            );
                          }
                        },
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.white,
                          size: 24.0,
                        ),
                        shape: CircleBorder(),
                        color: Color(0xFF2684FF),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
