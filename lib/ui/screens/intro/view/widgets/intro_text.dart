import 'package:flutter/material.dart';

class IntroText extends StatelessWidget {
  final String title;
  final String subtitle;

  const IntroText({
    Key key,
    @required this.title,
    @required this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          title,
          style: Theme.of(context).textTheme.bodyText2.copyWith(
                fontSize: 24.0,
                height: 32.0 / 24.0,
              ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 12.0),
        Text(
          subtitle,
          style: TextStyle(
            color: Color(0xFF172B4D),
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            height: 24.0 / 16.0,
          ),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
