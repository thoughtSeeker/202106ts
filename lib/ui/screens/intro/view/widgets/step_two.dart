import 'package:flutter/material.dart';


import 'widgets.dart';

class StepTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 40.0,
        bottom: 140.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Image.asset(
              'assets/images/intro_step_2.png',
              fit: BoxFit.fitWidth,
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 45.0,
            ),
            child: IntroText(
              title: 'screen_intro__step_two_title',
              subtitle: 'screen_intro__step_two_subtitle',
            ),
          ),
        ],
      ),
    );
  }
}
