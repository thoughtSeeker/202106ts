import 'package:flutter/material.dart';


import 'widgets.dart';

class StepThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 40.0,
        bottom: 140.0,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: Image.asset(
              'assets/images/intro_step_3.png',
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 45.0,
            ),
            child: IntroText(
              title: 'screen_intro__step_three_title',
              subtitle: 'screen_intro__step_three_subtitle',
            ),
          )
        ],
      ),
    );
  }
}
