import 'package:flutter/material.dart';


class MapText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'screen_map_title',
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.start,
        ),
        const SizedBox(height: 12.0),
        Text(
          'screen_map_subtitle',
          style: TextStyle(
            color: Color(0xFF172B4D),
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
            height: 1.4,
          ),
          textAlign: TextAlign.start,
        ),
      ],
    );
  }
}
