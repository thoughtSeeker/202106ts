import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/login/view/widgets/login_form.dart';

class MainMenubuttonLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Container(
        height: 60,
        width: 300,
        color: Colors.red,
        child: Center(
          child:
              Text('Button for Login', style: TextStyle(color: Colors.white)),
        ),
      ),
      onPressed: () {
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => LoginForm()),
        );
      },
    );
  }
}
