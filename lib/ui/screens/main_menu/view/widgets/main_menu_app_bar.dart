import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


import '../../../../widgets/widgets.dart';
import '../../../screens.dart';

class MainMenuAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar _appBar = AppBar();

  @override
  Widget build(BuildContext context) {
    TextStyle ts = TextStyle(fontSize: 18, color: Colors.black);
    // print('---> ts = $ts');
    return AppBar(
      backgroundColor: Colors.white,
      /*leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: 32,
          color: Colors.black,
        ),
        onPressed: () => Navigator.of(context).pop<void>(),
      ),*/
      title: AutoSizeText(
        'screen_fish__header_title',
        style: ts,
      ),
      actions: <Widget>[
       /* CustomFlatButton(
          padding: EdgeInsets.only(
            right: 24.0,
          ),
          text: 'button__register',
          textStyle: ts,
          onPressed: () => Navigator.of(context).push<void>(
            RegisterScreen.route(),
          ),
        ),*/
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
