import 'package:flutter/material.dart';

import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

class MainMenuScreen extends StatefulWidget {
  static Route animatedRoute() {
    return MaterialPageRoute<void>(
      builder: (_) => MainMenuScreen(),
    );
  }

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => MainMenuScreen(),
    );
  }

  @override
  _MainMenuScreenState createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends State<MainMenuScreen> {
  double vertical1 = 23.0;
  double vertical2 = 12.0;

  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: MainMenuAppBar(),
        body: Container(
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 40.0),
            children: <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: <Widget>[],
                ),
              )
            ],
          ),
        ));
  }
}
