import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/configure_screen_slider_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_flat_button.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: WelcomeScreen(),
    );
  }
}

class WelcomeScreen extends StatefulWidget {
  static Route animatedRoute() {
    return MaterialPageRoute<void>(
      builder: (_) => WelcomeScreen(),
    );
  }

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => WelcomeScreen(),
    );
  }

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String text = '''
    
Welcome Toni!
    
Ready to embark on a unique experience?

We have changed many lives into positive.

Step by step, day by day.

And it's easy.

You get one reminder a day to input your number. 
    
You can also write what went well or not so.

That's all.

And after a time... you will see some data.
We will look at that data, and will start to improve your life. Together.

Step by step, day by day.

We are ready.

Are you?

Yes, let's start!''';
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: WelcomeAppBar(),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(/*
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,*/
            children: <Widget>[
              AutoSizeText(text,
                  maxFontSize: 45,
                  minFontSize: 7,
                  maxLines: 50,
                  style: TextStyle(color: Colors.black, fontSize: 17)),
              Expanded(
                  child: Container(
                height: 20,
              )),
              CustomFlatButton(
                padding: EdgeInsets.all(
                  12.0,
                ),
                text: 'Yes, let\'s start',
                textStyle: TextStyle(color: Colors.black, fontSize: 17),
                onPressed: () => Navigator.of(context).push<void>(
                  ConfigureScreenSliderScreen.route(),
                ),
              ),
            ],
          ),
        ));
  }
}
