import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/configure_screen_slider_screen.dart';
import 'package:thoughtseekers/ui/screens/congrats/view/congrats_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_flat_button.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DataInputScreen(),
    );
  }
}

class DataInputScreen extends StatefulWidget {
  static Route animatedRoute() {
    return MaterialPageRoute<void>(
      builder: (_) => DataInputScreen(),
    );
  }

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => DataInputScreen(),
    );
  }

  @override
  _DataInputScreenState createState() => _DataInputScreenState();
}

class _DataInputScreenState extends State<DataInputScreen> {
  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String text = ''' ''';
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: DataInputAppBar(),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(text, style: TextStyle(color: Colors.black, fontSize: 17)),
              Expanded(
                  child: Container(
                height: 20,
              )),
              CustomFlatButton(
                padding: EdgeInsets.all(
                  12.0,
                ),
                text: 'Save',
                textStyle: TextStyle(color: Colors.black, fontSize: 17),
                onPressed: () => Navigator.of(context).push<void>(
                  CongratsScreen.route("Toni", "toni@gmail.com"),
                ),
              ),
            ],
          ),
        ));
  }
}
