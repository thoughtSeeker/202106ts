import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/configure_screen_slider_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_flat_button.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: NextOneScreen(),
    );
  }
}

class NextOneScreen extends StatefulWidget {
  static Route animatedRoute() {
    return MaterialPageRoute<void>(
      builder: (_) => NextOneScreen(),
    );
  }

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => NextOneScreen(),
    );
  }

  @override
  _NextOneScreenState createState() => _NextOneScreenState();
}

class _NextOneScreenState extends State<NextOneScreen> {
  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String text = ''' ''';
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: NextOneAppBar(),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(text, style: TextStyle(color: Colors.black, fontSize: 17)),
              Expanded(
                  child: Container(
                height: 20,
              )),
              CustomFlatButton(
                padding: EdgeInsets.all(
                  12.0,
                ),
                text: 'Yes, let\'s start',
                textStyle: TextStyle(color: Colors.black, fontSize: 17),
                onPressed: () => Navigator.of(context).push<void>(
                  ConfigureScreenSliderScreen.route(),
                ),
              ),
            ],
          ),
        ));
  }
}
