import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


import '../../../../widgets/widgets.dart';
import '../../../screens.dart';

class NextOneAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar _appBar = AppBar();

  @override
  Widget build(BuildContext context) {
    TextStyle ts = TextStyle(fontSize: 18, color: Colors.black);
    // print('---> ts = $ts');
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: 22,
          color: Colors.blueGrey,
        ),
        onPressed: () => Navigator.of(context).pop<void>(),
      ),
      title: AutoSizeText(
        'Next one',
        style: ts,
      ),
     /* actions: <Widget>[
        CustomFlatButton(
          padding: EdgeInsets.only(
            right: 24.0,
          ),
          text: 'register',
          textStyle: ts,
          onPressed: () => null *//*Navigator.of(context).push<void>(
            RegisterScreen.route(),
          ),*//*
        ),
      ],*/
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
