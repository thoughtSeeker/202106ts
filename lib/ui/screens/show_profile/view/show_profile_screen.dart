import 'package:thoughtseekers/models/user/user.dart';
import 'package:thoughtseekers/ui/screens/show_profile/view/widgets/show_profile_app_bar.dart';
import 'package:thoughtseekers/ui/screens/show_profile/view/widgets/show_team_profile_for_user.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../blocs/blocs.dart';

class ShowProfileScreen extends StatefulWidget {
  final User user1;
  final User user2;
  final String tournament1;
  final String tournament2;

  const ShowProfileScreen({Key key, this.user1, this.user2
    , this.tournament1, this.tournament2}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const ShowProfileScreen(),
    );
  }

  @override
  _ShowProfileScreenState createState() => _ShowProfileScreenState();
}

class _ShowProfileScreenState extends State<ShowProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final User user1 = widget.user1;
    final User user2 = widget.user2;

    return BlocProvider(
        create: (BuildContext context) => EditProfileCubit(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            ),
        child: Scaffold(
          appBar: ShowProfileAppBar(
            title: 'screen_view_team_profile__header_title',
          ),
          body: Column(
            children: [
              Text(widget.tournament1,
                style: TextStyle(fontSize: 18)),
              ShowTeamProfileForUser(user1),
              user2.firstName == null ? Container() : ShowTeamProfileForUser(user2),
            ],
          ),
        ));
  }
}
