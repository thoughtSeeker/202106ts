import 'package:flutter/material.dart';

class ShowProfileAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback onPressed;
  final List<Widget> actions;
  final AppBar _appBar = AppBar();

  ShowProfileAppBar({
    Key key,
    this.title,
    this.onPressed,
    this.actions = null,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
        ),
        onPressed: onPressed ?? () => Navigator.of(context).pop(),
      ),
      title: Text(
        title ?? 'Team Profile',
        style: Theme.of(context)
            .appBarTheme
            .textTheme
            .headline6
            .copyWith(fontSize: 18.0),
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
