
import 'package:thoughtseekers/models/user/user.dart';
import 'package:thoughtseekers/ui/screens/show_profile/view/widgets/show_profile_app_bar.dart';
import 'package:thoughtseekers/ui/screens/show_profile/view/widgets/show_profile_email.dart';
import 'package:thoughtseekers/ui/screens/show_profile/view/widgets/show_profile_name.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';



class ShowTeamProfileForUser extends StatefulWidget {
  User user;
  ShowTeamProfileForUser(this.user);
  @override
  _ShowTeamProfileForUserState createState() => _ShowTeamProfileForUserState();
}

class _ShowTeamProfileForUserState extends State<ShowTeamProfileForUser> {
  
  /// Light blue color
  Color lightBlue = Color(0xFF2684FF);
  
  @override
  Widget build(BuildContext context) {
    Widget _createListItem(
        String itemName,
        String itemValue,
        String noValue,
        VoidCallback onTap, {
          bool cvFlag = false,
        }) {
      return Column(
        children: <Widget>[
          ListTile(
            enabled: false,
            contentPadding: EdgeInsets.zero,
            dense: true,
            title: Text(
              itemName,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
              ),
            ),
            // if cvFlag is false
            trailing: (cvFlag == false)
                ? Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 8,
              children: <Widget>[
                // If there is itemValue
                (itemValue != null)
                    ? Text(
                  itemValue,
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .color
                        .withOpacity(0.7),
                  ),
                  overflow: TextOverflow.ellipsis,
                )
                    :
                // if there is no value
                Text(
                  noValue,
                  style: TextStyle(
                    fontSize: 14,
                    color: Theme.of(context)
                        .textTheme
                        .bodyText1
                        .color
                        .withOpacity(0.3),
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
                Icon(Icons.chevron_right),
              ],
            ) // if cvFlag is true
                : Wrap(
              spacing: 8,
              children: <Widget>[
                Text(
                  noValue,
                  style: TextStyle(
                    color: lightBlue,
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                  ),
                ),
                Icon(
                  OMIcons.noteAdd,
                  color: lightBlue,
                  size: 20,
                ),
              ],
            ),
            onTap: onTap,
          ),
          Divider()
        ],
      );
    }

    return Container(
      height: 200,
      color: Colors.white,
        child: ListView(
          padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0),
          children: <Widget>[
          AppBar(
          automaticallyImplyLeading: false,
          title: Text('Angler Profile',
            style: Theme.of(context)
                .appBarTheme
                .textTheme
                .headline6
                .copyWith(fontSize: 18.0),
          ),
        ),
            _createListItem(
              'Name',
              widget.user.firstName + " " + widget.user.lastName,
              'Add a name to your profile',
                  () => Navigator.of(context).push<void>(
                ShowProfileName.route(),
              ),
            ),
            _createListItem(
              'Email',
              widget.user.email,
              'Your email',
                  () => Navigator.of(context).push<void>(
                ShowProfileEmail.route(),
              ),
            ),
          ],
        ),
      )
    ;
  }
}
