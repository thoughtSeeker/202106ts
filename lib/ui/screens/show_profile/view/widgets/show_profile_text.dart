import 'package:flutter/material.dart';

/// Creating title with subtitle
class EditProfileText extends StatelessWidget {
  final String title;
  final String subtitle;

  const EditProfileText({
    Key key,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 16.0,
        ),
      ),
      subtitle: Text(
        subtitle,
        style: TextStyle(
          fontSize: 12,
          color: Color(0xFF172B4D).withOpacity(0.7),
        ),
      ),
    );
  }
}
