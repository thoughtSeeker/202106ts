import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/edit_profile/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Page where user sets his name
class ShowProfileName extends StatefulWidget {
  const ShowProfileName({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<ShowProfileName>(
      builder: (context) {
        return BlocProvider.value(
          value: EditProfileCubit(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: ShowProfileName(),
        );
      },
    );
  }

  @override
  _ShowProfileNameState createState() => _ShowProfileNameState();
}

class _ShowProfileNameState extends State<ShowProfileName> {
  /// control is save button enabled or disabled
  bool isSaveEnabled;

  @override
  void initState() {
    super.initState();
    isSaveEnabled = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ShowProfileAppBar(
        title: 'screen_edit_profile__step_edit_name_header_title',
        onPressed: null,
        actions: isSaveEnabled
            ? [
                CustomFlatButton(
                  text: 'button__save',
                  padding: EdgeInsets.only(
                    right: 24.0,
                  ),
                  textStyle: Theme.of(context)
                      .appBarTheme
                      .textTheme
                      .headline6
                      .copyWith(
                        color: Color(0xFF2684FF),
                        fontWeight: FontWeight.w600,
                      ),
                  onPressed: () {
                    context.read<EditProfileCubit>().editProfileNameSubmitted();
                  },
                ),
              ]
            : null,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: BlocListener<EditProfileCubit, EditProfileState>(
          listenWhen: (previous, current) =>
              previous.name.status != current.name.status,
          listener: (context, state) {
            setState(() {
              isSaveEnabled = state.name.status.isValid;
            });

            if (state.name.status.isSubmissionSuccess) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Color(0xFF2684FF),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_name_message_success',
                          ),
                        ),
                        const Icon(
                          Icons.check,
                          size: 44.0,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  ),
                );
            } else if (state.name.status.isSubmissionInProgress) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_name_message_in_progress',
                          ),
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                );
            } else if (state.name.status.isSubmissionFailure) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_name_message_failed',
                          ),
                        ),
                        const Icon(Icons.warning),
                      ],
                    ),
                  ),
                );
            }
          },
          child: _NameInput(),
        ),
      ),
    );
  }
}

class _NameInput extends StatefulWidget {
  @override
  __NameInputState createState() => __NameInputState();
}

class __NameInputState extends State<_NameInput> {
  /// Text focus node for text field
  FocusNode _focusNode;

  /// Text controller for text field
  TextEditingController _textController;

  String _hintText = 'input_hint__enter_name';
  String _labelText = 'input_label__enter_name';

  EditProfileCubit _editProfileCubit;

  @override
  void initState() {
    super.initState();
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.name.name.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__enter_name';
        _labelText = 'input_label__enter_name';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__enter_name';
          _labelText = 'input_hint__enter_name';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.editProfileNameChanged(_textController.text);
  }

  String _populateErrorText(ProfileName name) {
    if (name.name.invalid) {
      switch (name.name.error) {
        // case NameValidationError.empty:
        //   return 'input_error__empty_enter_name';

        case NameValidationError.invalidFormat:
          return 'input_error__invalid_format_enter_name';

        default:
          print('Exception (_EDIT_NAME_): ${name.name.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) => previous.name.name != current.name.name,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateErrorText(state.name),
          ),
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}
