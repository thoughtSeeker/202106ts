import 'package:thoughtseekers/utils/singleton.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Page where user sets his username
class ShowProfileUsername extends StatefulWidget {
  const ShowProfileUsername({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<ShowProfileUsername>(
      builder: (context) {
        return BlocProvider.value(
          value: EditProfileCubit(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: ShowProfileUsername(),
        );
      },
    );
  }

  @override
  _ShowProfileUsernameState createState() => _ShowProfileUsernameState();
}

class _ShowProfileUsernameState extends State<ShowProfileUsername> {
  /// control is save button enabled or disabled
  bool isSaveEnabled;

  @override
  void initState() {
    super.initState();
    isSaveEnabled = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ShowProfileAppBar(
        title: 'screen_edit_profile__step_edit_username_header_title',
        onPressed: null,
        actions: isSaveEnabled
            ? [
                CustomFlatButton(
                  text: 'button__save',
                  padding: EdgeInsets.only(
                    right: 24.0,
                  ),
                  textStyle: Theme.of(context)
                      .appBarTheme
                      .textTheme
                      .headline6
                      .copyWith(
                        color: Color(0xFF2684FF),
                        fontWeight: FontWeight.w600,
                      ),
                  onPressed: () {
                    context
                        .read<EditProfileCubit>()
                        .editProfileUsernameSubmitted();
                  },
                ),
              ]
            : null,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: BlocListener<EditProfileCubit, EditProfileState>(
          listenWhen: (previous, current) =>
              previous.username.status != current.username.status,
          listener: (context, state) {
            setState(() {
              isSaveEnabled = state.username.status.isValid;
            });

            if (state.username.status.isSubmissionSuccess) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Color(0xFF2684FF),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_username_message_success',
                          ),
                        ),
                        const Icon(
                          Icons.check,
                          size: 44.0,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  ),
                );
            } else if (state.username.status.isSubmissionInProgress) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_username_message_in_progress',
                          ),
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                );
            } else if (state.username.status.isSubmissionFailure) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_username_message_failed',
                          ),
                        ),
                        const Icon(Icons.warning),
                      ],
                    ),
                  ),
                );
            }
          },
          child: Column(
            children: [
              _UsernameInput(),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(
                    top: 12.0,
                    bottom: 12.0,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text(
                        'https://dev.aftournaments.com/',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      Flexible(
                          child: Text(
                        '@username',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ))
                    ],
                  ),
                ),
              ),
              Text(
                'Usernames can contain only letters, numbers, underscores, and periods. Changing your username will also change your profile link',
                style: TextStyle(
                  fontSize: 12.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _UsernameInput extends StatefulWidget {
  @override
  __UsernameInputState createState() => __UsernameInputState();
}

class __UsernameInputState extends State<_UsernameInput> {
  /// Text focus node for text field
  FocusNode _focusNode;

  /// Text controller for text field
  TextEditingController _textController;

  String _hintText = 'input_hint__enter_username';
  String _labelText = 'input_label__enter_username';

  EditProfileCubit _editProfileCubit;

  @override
  void initState() {
    super.initState();
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.username.username.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__enter_username';
        _labelText = 'input_label__enter_username';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__enter_username';
          _labelText = 'input_hint__enter_username';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.editProfileNameChanged(_textController.text);
  }

  String _populateErrorText(ProfileUsername username) {
    if (username.username.invalid) {
      switch (username.username.error) {
        // case UsernameValidationError.empty:
        //   return 'input_error_text_empty_enter_username';

        case UsernameValidationError.invalidFormat:
          return 'input_error__invalid_format_enter_username';

        default:
          print('Exception (_STEP_CHANGE_USERNAME_): '
              '${username.username.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.username.username != current.username.username,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateErrorText(state.username),
          ),
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}
