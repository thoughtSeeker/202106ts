import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/configure_screen_slider.dart';
import 'package:thoughtseekers/ui/screens/discover_insights/view/discover_insights_screen.dart';
import 'package:thoughtseekers/ui/screens/settings/view/settings_screen.dart';
import 'package:thoughtseekers/ui/screens/visualisation/view/widgets/bar_chart_sample5.dart';
import 'package:thoughtseekers/ui/widgets/custom_icon_button_with_text.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'package:thoughtseekers/ui/icons/sean_icons_icons.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: VisualisationScreen(),
    );
  }
}

class VisualisationScreen extends StatefulWidget {
  static Route animatedRoute() {
    return MaterialPageRoute<void>(
      builder: (_) => VisualisationScreen(),
    );
  }

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => VisualisationScreen(),
    );
  }

  @override
  _VisualisationScreenState createState() => _VisualisationScreenState();
}

class _VisualisationScreenState extends State<VisualisationScreen> {
  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String header = "Welcome Toni!";
    String text = '''
You're all set!
You're in the control of your life today.
     
We'll send a reminder tomorrow, to make sure you don't forget to input the values most important to you!
    ''';
    double height = 4;
    double iconWidth = MediaQuery.of(context).size.width / 7;
    double fSize = 15;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: VisualisationAppBar(),
        body: Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            color: Colors.transparent,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(header,
                      style: TextStyle(color: Colors.black, fontSize: fSize)),
                  Container(
                    height: height,
                  ),
                  Text(text,
                      style: TextStyle(color: Colors.black, fontSize: fSize)),
                  Expanded(
                    child: Container(
                        // padding: EdgeInsets.symmetric(horizontal: 20),
                        child: BarChartSample5()),
                  ),
                  Container(
                    height: height,
                  ),

                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomIconWithText(
                          text: "Profile",
                          icon: Icon(
                            Sean_icons.thoughtseekers_menu_icons_profile,
                            size: iconWidth,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () => Navigator.of(context).push<void>(
                            SettingsScreen.route(),
                          ),
                        ),
                        CustomIconWithText(
                          text: "Configure",
                          icon: Icon(
                            Sean_icons.thoughtseekers_menu_icons_configure,
                            size: iconWidth,
                            color: Colors.red,
                          ),
                          onPressed: () => Navigator.of(context).push<void>(
                            ConfigureScreenSliderScreen.route(),
                          ),
                        ),
                        CustomIconWithText(
                          text: "Stats",
                          icon: Icon(
                            Sean_icons.thoughtseekers_menu_icons_stats,
                            size: iconWidth,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () => Navigator.of(context).push<void>(
                            VisualisationScreen.route(),
                          ),
                        ),
                        CustomIconWithText(
                          text: "Insights",
                          icon: Icon(
                            Sean_icons.thoughtseekers_menu_icons_insights,
                            size: iconWidth,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () => Navigator.of(context).push<void>(
                            DiscoverInsightsScreen.route(),
                          ),
                        ),
                        CustomIconWithText(
                          text: "Add",
                          icon: Icon(
                            Sean_icons.thoughtseekers_menu_icons_add,
                            size: iconWidth,
                            color: Colors.blueGrey,
                          ),
                          onPressed: () => Navigator.of(context).push<void>(
                            DiscoverInsightsScreen.route(),
                          ),
                        ),
                      ]),

                ])));
  }
}
