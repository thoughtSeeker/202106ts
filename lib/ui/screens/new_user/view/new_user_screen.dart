import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/widgets/notification_bell.dart';

import '../../../widgets/widgets.dart';
import 'widgets/widgets.dart';

class NewUserScreen extends StatefulWidget {
  const NewUserScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const NewUserScreen(),
    );
  }

  @override
  _NewUserScreenState createState() => _NewUserScreenState();
}

class _NewUserScreenState extends State<NewUserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 40.0),
              child: Row(
                children: [
                  UserInfo(),
                  const SizedBox(width: 8.0),
                  NotificationBell(),
                ],
              ),
            ),
            Expanded(
              child: FollowUsersCarousel(),
            ),
          ],
        ),
      ),
    );
  }
}
