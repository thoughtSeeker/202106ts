import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';


import '../../../../widgets/widgets.dart';

class FollowUsersCarousel extends StatelessWidget {
  /// Blue colors
  final Color lightBlue = Color(0xFF2684FF);
  final Color darkBlue = Color(0xFF172B4D);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 1,
          child: Container(
            height: 500,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                )),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  // Title widgets
                  Padding(
                    padding: EdgeInsets.fromLTRB(40.0, 32.0, 40.0, 2.0),
                    child: Center(
                      child: Column(
                        children: [
                          Text(
                            'screen_new_user__follow_users_carousel_title',
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Text(
                            'screen_new_user__follow_users_carousel_subtitle'
                                ,
                            style: TextStyle(
                              color: Color(0xFF172B4D),
                              fontSize: 14.0,
                              fontWeight: FontWeight.w500,
                              height: 20.0 / 14.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),

                  // Container(
                  //   margin: EdgeInsets.fromLTRB(40, 32, 40, 0),
                  //   child: Center(
                  //     child: Text(
                  //       'new_user_follow_users_carousel_title',
                  //       style: Theme.of(context).textTheme.bodyText1,
                  //     ),
                  //   ),
                  // ),
                  // SizedBox(height: 8),
                  // Container(
                  //   margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
                  //   child: Center(
                  //     child: Text(
                  //       'new_user_follow_users_carousel_subtitle',
                  //       style: TextStyle(
                  //         color: Color(0xFF172B4D),
                  //         fontSize: 14.0,
                  //         fontWeight: FontWeight.w500,
                  //         height: 20.0 / 14.0,
                  //       ),
                  //       textAlign: TextAlign.center,
                  //     ),
                  //   ),
                  // ),

                  // Suggested people
                  CarouselSlider(
                    items: [
                      UserPreview(
                        'gordon_freeman21',
                        'Gordon Freeman',
                        'I am Gordon',
                        '1.5k',
                        'https://bodyartguru.com/wp-content/uploads/2019/02/priyanka-chopra.jpg',
                      ),
                      UserPreview(
                        'helen_tram',
                        "Helen Tram",
                        'I am Helena trojanska ahil je moj bog ja volim da jasem slonove',
                        '2.4k',
                        'https://bodyartguru.com/wp-content/uploads/2019/02/priyanka-chopra.jpg',
                      ),
                      UserPreview(
                        'jonathandavis',
                        'Jonatah Davis',
                        'I am Jonathan',
                        '1.2k',
                        'https://bodyartguru.com/wp-content/uploads/2019/02/priyanka-chopra.jpg',
                      ),
                      UserPreview(
                        'mikle',
                        'Mikle Mikle',
                        'Lorem Ipsum',
                        '2.5k',
                        'https://bodyartguru.com/wp-content/uploads/2019/02/priyanka-chopra.jpg',
                      )
                    ],
                    options: CarouselOptions(
                      enableInfiniteScroll: false,
                      enlargeCenterPage: true,
                      enlargeStrategy: CenterPageEnlargeStrategy.scale,
                      aspectRatio: 1.12,
                      viewportFraction: 0.55,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 40.0,
                      vertical: 24.0,
                    ),
                    child: Column(
                      children: [
                        Container(
                          child: Center(
                            child: Text(
                              'screen_new_user__offer_your_time_and_earn_money'
                                  ,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16.0,
                        ),
                        CustomRaisedButton(
                          text: 'button__find_out_how',
                          color: Color(0xFF2684FF),
                          textStyle: TextStyle(
                            color: Colors.white,
                          ),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class UserPreview extends StatelessWidget {
  /// Blue colors
  Color lightBlue = Color(0xFF2684FF);
  Color darkBlue = Color(0xFF172B4D);

  String username;

  /// First and last name
  String name;

  /// User's short description
  String description;

  /// Number in the corner
  String number;

  /// Profile picture url
  String imgUrl;

  UserPreview(
    this.username,
    this.name,
    this.description,
    this.number,
    this.imgUrl,
  );

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      child: Container(
        height: 300,
        width: 209,
        margin: EdgeInsets.only(top: 20, bottom: 20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                blurRadius: 8.0,
              )
            ]),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  // number
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0, right: 12),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: lightBlue,
                          size: 16,
                        ),
                        Text(
                          number,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: lightBlue,
                              fontSize: 14),
                        )
                      ],
                    ),
                  ),
                  // clear icon
                  IconButton(
                    icon: Icon(Icons.clear, color: darkBlue.withOpacity(0.7)),
                    onPressed: () {
                      // TODO clear function
                    },
                  )
                ],
              ),
              // profile picture
              CircleAvatar(
                backgroundColor: darkBlue,
                backgroundImage: NetworkImage(imgUrl),
                radius: 40,
              ),
              // username
              Container(
                margin: EdgeInsets.fromLTRB(24, 4, 24, 4),
                child: Text(
                  username,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                    color: darkBlue,
                  ),
                ),
              ),
              // name
              Container(
                margin: EdgeInsets.fromLTRB(24, 0, 24, 12),
                child: Text(
                  name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: darkBlue.withOpacity(0.7),
                    fontSize: 14,
                  ),
                ),
              ),
              // description
              Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(24, 0, 24, 0),
                  child: Text(
                    description,
                    style: TextStyle(
                        color: darkBlue.withOpacity(0.7), fontSize: 12),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              // follow button
              InkWell(
                child: Container(
                  margin: EdgeInsets.fromLTRB(37, 12, 37, 12),
                  height: 36,
                  decoration: BoxDecoration(
                      color: lightBlue, borderRadius: BorderRadius.circular(6)),
                  child: Center(
                    child: Text(
                      "Follow",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                  ),
                ),
                onTap: () {
                  // TODO follow function
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
