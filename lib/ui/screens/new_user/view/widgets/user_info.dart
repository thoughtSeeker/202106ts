import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/ui/widgets/user_avatar_with_actions.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../models/models.dart';
import '../../../../widgets/widgets.dart';
import '../../../edit_profile/view/widgets/edit_profile_biography.dart';

class UserInfo extends StatefulWidget {
  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  /// Blue colors
  Color lightBlue = Color(0xFF2684FF);
  Color darkBlue = Color(0xFF172B4D);

  @override
  Widget build(BuildContext context) {
    final User user =
        (context.read<AuthenticationBloc>().state as AuthenticationSuccess)
            .user;

    return Expanded(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          BlocProvider(
            create: (context) => EditProfileCubit(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
            ),
            child: UserAvatarWithActions(
              showPlusButton: true,
            ),
          ),
          SizedBox(
            width: 10.0,
          ),
          // name, username, description widgets
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // name
              Text(
                user.name,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                    color: darkBlue),
              ),
              SizedBox(
                height: 8.0,
              ),
              // username
              Text(
                user.name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: darkBlue.withOpacity(0.7)),
              ),
              SizedBox(
                height: 8.0,
              ),
              // if description is not empty it will be displayed
              Container(
                child: CustomFlatButton(
                  text: 'screen_new_user__add_a_short_description',
                  onPressed: () => Navigator.of(context).push<void>(
                    EditProfileBiography.route(),
                  ),
                  padding: EdgeInsets.zero,
                  textStyle: TextStyle(
                    color: Color(0xFF2684FF),
                    fontSize: 14.0,
                    height: 17.0 / 14.0,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
