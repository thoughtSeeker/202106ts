import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/blocs/forgot_password/forgot_password_cubit.dart';

import '../../../../blocs/blocs.dart';
import 'widgets/widgets.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const ForgotPasswordScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => ForgotPasswordCubit(),
      child: Scaffold(
        body: ForgotPasswordWizard(),
      ),
    );
  }
}
