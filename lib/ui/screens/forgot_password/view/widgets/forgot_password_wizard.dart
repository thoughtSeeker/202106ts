import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/blocs/forgot_password/forgot_password_cubit.dart';

import '../../../../../blocs/blocs.dart';
import 'widgets.dart';

class ForgotPasswordWizard extends StatefulWidget {
  const ForgotPasswordWizard({
    Key key,
  }) : super(key: key);

  @override
  _ForgotPasswordWizardState createState() => _ForgotPasswordWizardState();
}

class _ForgotPasswordWizardState extends State<ForgotPasswordWizard> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordCubit, ForgotPasswordState>(
      listenWhen: (previous, current) => previous.status != current.status,
      listener: (BuildContext context, state) {
        if (state.status.isSubmissionSuccess) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Color(0xFF2684FF),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child:
                          const Text('Forgot password success'),
                    ),
                    const Icon(
                      Icons.check,
                      size: 44.0,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),
            );
          _navigator.push<void>(StepCompleted.route());
        } else if (state.status.isSubmissionInProgress) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: const Text(
                              'Password message in progress'),
                    ),
                    const CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        } else if (state.status.isSubmissionFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                backgroundColor: Colors.red,
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child:
                          const Text('Forgot password message failed'),
                    ),
                    const Icon(Icons.warning),
                  ],
                ),
              ),
            );
        }
      },
      child: Navigator(
        key: _navigatorKey,
        onGenerateRoute: (_) => StepEmail.route(),
      ),
    );
  }
}
