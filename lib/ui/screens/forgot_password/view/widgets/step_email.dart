
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/blocs/forgot_password/forgot_password_cubit.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/forgot_password/models/models.dart';
import '../../../../widgets/widgets.dart';
import '../../../screens.dart';
import 'widgets.dart';

class StepEmail extends StatelessWidget {
  const StepEmail({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepEmail(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ForgotPasswordAppBar(
        onPressed: () => Navigator.of(context).pushAndRemoveUntil<void>(
          LoginScreen.route(),
          (route) => false,
        ),
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 40.0,
            ),
            child: Column(
              children: <Widget>[
                const SizedBox(height: 73.0),
                ForgotPasswordText(
                  bodyText1:
                      'Forgot password title',
                  bodyText2:
                      '',
                ),
                const SizedBox(height: 32.0),
                _EmailInput(),
                const SizedBox(height: 10.0),
                _GoogleRecaptcha(),
                const SizedBox(height: 24.0),
                _ResetPasswordButton(),
                const SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _EmailInput extends StatefulWidget {
  @override
  __EmailInputState createState() => __EmailInputState();
}

class __EmailInputState extends State<_EmailInput> {
  FocusNode _focusNode;
  TextEditingController _emailController;

  String _hintText = 'input_hint__enter_email';
  String _labelText = 'input_label__enter_email';

  ForgotPasswordCubit _forgotPasswordCubit;

  @override
  void initState() {
    super.initState();
    _forgotPasswordCubit = BlocProvider.of<ForgotPasswordCubit>(context);
    _emailController = TextEditingController(
      text: _forgotPasswordCubit.state.email.value,
    )..addListener(_onEmailValueChanged);
    _focusNode = FocusNode()..addListener(_onEmailFocusChanged);
  }

  void _onEmailFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter email';
        _labelText = 'Enter email';
      });
    } else {
      if (_emailController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter email';
          _labelText = 'Enter email';
        });
      }
    }
  }

  void _onEmailValueChanged() {
    _forgotPasswordCubit.forgotPasswordEmailChanged(_emailController.text);
  }

  String _populateEmailErrorText(Email email) {
    if (email.invalid) {
      switch (email.error) {
        case EmailValidationError.invalidFormat:
          return 'Invalid format Enter email';

        default:
          print('Exception (_FORGOT_PASSWORD_): ${email.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ForgotPasswordCubit, ForgotPasswordState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateEmailErrorText(state.email),
            errorMaxLines: 2,
          ),
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _emailController,
        );
      },
    );
  }
}

class _GoogleRecaptcha extends StatefulWidget {
  @override
  __GoogleRecaptchaState createState() => __GoogleRecaptchaState();
}

class __GoogleRecaptchaState extends State<_GoogleRecaptcha> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomFlatButton(
        text: 'reCaptcha',
        textStyle: TextStyle(
          color: Color(0xFF2684FF),
        ),
        onPressed: () {},
      ),
    );
  }
}

class _ResetPasswordButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ForgotPasswordCubit, ForgotPasswordState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'Reset password',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.status.isValidated
              ? () => {
                    FocusScope.of(context).unfocus(),
                    context.read<ForgotPasswordCubit>().forgotPasswordSubmitted(
                          "en_US",
                        ),
                  }
              : null,
        );
      },
    );
  }
}
