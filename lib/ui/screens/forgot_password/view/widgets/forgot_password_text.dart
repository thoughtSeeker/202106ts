import 'package:flutter/material.dart';

class ForgotPasswordText extends StatelessWidget {
  final String bodyText1;
  final String bodyText2;
  final TextAlign textAlign;

  const ForgotPasswordText({
    Key key,
    @required this.bodyText1,
    @required this.bodyText2,
    this.textAlign = TextAlign.start,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: double.infinity,
          child: Text(
            bodyText1,
            style: Theme.of(context).textTheme.bodyText2,
            textAlign: textAlign,
          ),
        ),
        const SizedBox(height: 12.0),
        Container(
          width: double.infinity,
          child: Text(
            bodyText2,
            style: TextStyle(
              color: Color(0xFF172B4D),
              fontSize: 14.0,
              fontWeight: FontWeight.w500,
              height: 1.4,
            ),
            textAlign: textAlign,
          ),
        ),
      ],
    );
  }
}
