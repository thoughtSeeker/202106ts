import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/edit_profile/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Page where user sets his password
class EditProfilePassword extends StatefulWidget {
  const EditProfilePassword({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<EditProfilePassword>(
      builder: (context) {
        return BlocProvider.value(
          value: EditProfileCubit(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: EditProfilePassword(),
        );
      },
    );
  }

  @override
  _EditProfilePasswordState createState() => _EditProfilePasswordState();
}

class _EditProfilePasswordState extends State<EditProfilePassword> {
  /// control is save button enabled or disabled
  bool isSaveEnabled;

  @override
  void initState() {
    super.initState();
    isSaveEnabled = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EditProfileAppBar(
        title: 'Edit password',
        onPressed: null,
        actions: isSaveEnabled
            ? [
                CustomFlatButton(
                  text: 'Save',
                  padding: EdgeInsets.only(
                    right: 24.0,
                  ),
                  textStyle: Theme.of(context)
                      .appBarTheme
                      .textTheme
                      .headline6
                      .copyWith(
                        color: Color(0xFF2684FF),
                        fontWeight: FontWeight.w600,
                      ),
                  onPressed: () {
                    context.read<EditProfileCubit>().editPasswordSubmitted();
                  },
                ),
              ]
            : null,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: BlocListener<EditProfileCubit, EditProfileState>(
          listenWhen: (previous, current) =>
              previous.password.status != current.password.status,
          listener: (context, state) {
            setState(() {
              isSaveEnabled = state.password.status.isValid;
            });

            if (state.password.status.isSubmissionSuccess) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Color(0xFF2684FF),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'Password changed successfully',
                          ),
                        ),
                        const Icon(
                          Icons.check,
                          size: 44.0,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  ),
                );
            } else if (state.password.status.isSubmissionInProgress) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'Editing password in progress',
                          ),
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                );
            } else if (state.password.status.isSubmissionFailure) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'Editing of password failed',
                          ),
                        ),
                        const Icon(Icons.warning),
                      ],
                    ),
                  ),
                );
            }
          },
          child: Column(
            children: [
              _PasswordInput(),
              const SizedBox(height: 10.0),
              _NewPasswordInput(),
              const SizedBox(height: 10.0),
              _NewPasswordConfirmedInput(),
            ],
          ),
        ),
      ),
    );
  }
}

class _PasswordInput extends StatefulWidget {
  @override
  __PasswordInputState createState() => __PasswordInputState();
}

class __PasswordInputState extends State<_PasswordInput> {
  FocusNode _focusNode;
  TextEditingController _textController;

  String _hintText = 'Enter_password';
  String _labelText = 'Enter_password';

  EditProfileCubit _editProfileCubit;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.password.password.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter_password';
        _labelText = 'Enter_password';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter_password';
          _labelText = 'Enter_password';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.passwordChanged(_textController.text);
  }

  String _populateErrorText(ProfilePassword password) {
    if (password.password.invalid) {
      switch (password.password.error) {
        // case PasswordValidationError.empty:
        //   return 'input_error_text_empty_enter_password';

        default:
          print('Exception (_EDIT_PASSWORD_): '
              '${password.password.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.password.password != current.password.password,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          obscureText: !_showPassword,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
            errorText: _populateErrorText(state.password),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}

class _NewPasswordInput extends StatefulWidget {
  @override
  __NewPasswordInputState createState() => __NewPasswordInputState();
}

class __NewPasswordInputState extends State<_NewPasswordInput> {
  FocusNode _focusNode;
  TextEditingController _textController;

  String _hintText = 'Enter new password';
  String _labelText = 'Enter new password';

  EditProfileCubit _editProfileCubit;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.password.newPassword.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter new password';
        _labelText = 'Enter new password';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter new password';
          _labelText = 'Enter new password';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.newPasswordChanged(_textController.text);
  }

  String _populateErrorText(ProfilePassword password) {
    if (password.newPassword.invalid) {
      switch (password.newPassword.error) {
        case NewPasswordValidationError.invalidFormat:
          return 'Invalid format to enter new password';

        default:
          print('Exception (_EDIT_PASSWORD_): '
              '${password.newPassword.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.password.newPassword != current.password.newPassword,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          obscureText: !_showPassword,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
            errorText: _populateErrorText(state.password),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}

class _NewPasswordConfirmedInput extends StatefulWidget {
  @override
  __NewPasswordConfirmedInputState createState() =>
      __NewPasswordConfirmedInputState();
}

class __NewPasswordConfirmedInputState
    extends State<_NewPasswordConfirmedInput> {
  FocusNode _focusNode;
  TextEditingController _textController;

  String _hintText = 'input_hint__enter_confirm_new_password';
  String _labelText = 'input_label__enter_confirm_new_password';

  EditProfileCubit _editProfileCubit;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.password.newPasswordConfirmed.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__enter_confirm_new_password';
        _labelText = 'input_label__enter_confirm_new_password';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__enter_confirm_new_password';
          _labelText = 'input_hint__enter_confirm_new_password';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.newPasswordConfirmedChanged(_textController.text);
  }

  String _populateErrorText(ProfilePassword password) {
    if (password.newPasswordConfirmed.invalid) {
      switch (password.newPasswordConfirmed.error) {
        // case NewPasswordConfirmedValidationError.invalidFormat:
        //   return 'input_error__invalid_format_enter_new_password';

        case NewPasswordConfirmedValidationError.mismatch:
          return 'input_error__mismatch_enter_confirm_new_password';

        default:
          print('Exception (_EDIT_PASSWORD_): '
              '${password.newPasswordConfirmed.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.password.newPasswordConfirmed !=
          current.password.newPasswordConfirmed,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          obscureText: !_showPassword,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
            errorText: _populateErrorText(state.password),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}
