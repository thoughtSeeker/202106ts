import 'package:flutter/material.dart';

class EditProfileAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback onPressed;
  final List<Widget> actions;
  final AppBar _appBar = AppBar();

  EditProfileAppBar({
    Key key,
    this.title,
    this.onPressed,
    this.actions = null,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
        ),
        onPressed: onPressed ?? () => Navigator.of(context).pop(),
      ),
      title: Text(
        title ?? 'HTML page',
        style: Theme.of(context)
            .appBarTheme
            .textTheme
            .headline6
            .copyWith(fontSize: 18.0),
      ),
      actions: actions,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
