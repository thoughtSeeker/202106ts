import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';
import 'package:provider/provider.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/edit_profile/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Page where user sets his biography
class EditProfileBiography extends StatefulWidget {
  const EditProfileBiography({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<EditProfileBiography>(
      builder: (context) {
        return BlocProvider.value(
          value: EditProfileCubit(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: EditProfileBiography(),
        );
      },
    );
  }

  @override
  _EditProfileBiographyState createState() => _EditProfileBiographyState();
}

class _EditProfileBiographyState extends State<EditProfileBiography> {
  /// control is save button enabled or disabled
  bool isSaveEnabled;

  @override
  void initState() {
    super.initState();
    isSaveEnabled = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EditProfileAppBar(
        title: 'screen_edit_profile__step_edit_biography_header_title',
        onPressed: null,
        actions: isSaveEnabled
            ? [
                CustomFlatButton(
                  text: 'Save',
                  padding: EdgeInsets.only(
                    right: 24.0,
                  ),
                  textStyle: Theme.of(context)
                      .appBarTheme
                      .textTheme
                      .headline6
                      .copyWith(
                        color: Color(0xFF2684FF),
                        fontWeight: FontWeight.w600,
                      ),
                  onPressed: () {
                    context
                        .read<EditProfileCubit>()
                        .editProfileBiographySubmitted();
                  },
                ),
              ]
            : null,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: BlocListener<EditProfileCubit, EditProfileState>(
          listenWhen: (previous, current) =>
              previous.biography.status != current.biography.status,
          listener: (context, state) {
            setState(() {
              isSaveEnabled = state.biography.status.isValid;
            });

            if (state.biography.status.isSubmissionSuccess) {
              setState(() {
                isSaveEnabled = false;
              });
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Color(0xFF2684FF),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_biography_message_success',
                          ),
                        ),
                        const Icon(
                          Icons.check,
                          size: 44.0,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  ),
                );
            } else if (state.biography.status.isSubmissionInProgress) {
              setState(() {
                isSaveEnabled = false;
              });
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_biography_message_in_progress',
                          ),
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                );
            } else if (state.biography.status.isSubmissionFailure) {
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_biography_message_failed',
                          ),
                        ),
                        const Icon(Icons.warning),
                      ],
                    ),
                  ),
                );
            }
          },
          child: _BiographyInput(),
        ),
      ),
    );
  }
}

class _BiographyInput extends StatefulWidget {
  @override
  __BiographyInputState createState() => __BiographyInputState();
}

class __BiographyInputState extends State<_BiographyInput> {
  /// Text focus node for text field
  FocusNode _focusNode;

  /// Text controller for text field
  TextEditingController _textController;

  String _hintText = 'input_hint__add_biography';
  String _labelText = 'input_label__biography';

  EditProfileCubit _editProfileCubit;

  @override
  void initState() {
    super.initState();
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.biography.biography.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__add_biography';
        _labelText = 'input_label__biography';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__biography';
          _labelText = 'input_hint__add_biography';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.editProfileBiographyChanged(_textController.text);
  }

  String _populateErrorText(ProfileBiography biography) {
    if (biography.biography.invalid) {
      switch (biography.biography.error) {
        // case BiographyValidationError.empty:
        //   return 'input_error_text_empty_enter_biography';

        case BiographyValidationError.invalidFormat:
          return 'input_error__invalid_format_biography';

        default:
          print('Exception (_STEP_CHANGE_BIOGRAPHY_): '
              '${biography.biography.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.biography.biography != current.biography.biography,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateErrorText(state.biography),
          ),
          textCapitalization: TextCapitalization.sentences,
          minLines: 1,
          maxLines: 6,
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}
