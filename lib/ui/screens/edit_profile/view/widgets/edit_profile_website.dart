import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/edit_profile/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Page where user sets his website
class EditProfileWebsite extends StatefulWidget {
  const EditProfileWebsite({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<EditProfileWebsite>(
      builder: (context) {
        return BlocProvider.value(
          value: EditProfileCubit(
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          ),
          child: EditProfileWebsite(),
        );
      },
    );
  }

  @override
  _EditProfileWebsiteState createState() => _EditProfileWebsiteState();
}

class _EditProfileWebsiteState extends State<EditProfileWebsite> {
  /// control is save button enabled or disabled
  bool isSaveEnabled;

  @override
  void initState() {
    super.initState();
    isSaveEnabled = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EditProfileAppBar(
        title: 'screen_edit_profile__step_edit_website_header_title',
        onPressed: null,
        actions: isSaveEnabled
            ? [
                CustomFlatButton(
                  text: 'button__save',
                  padding: EdgeInsets.only(
                    right: 24.0,
                  ),
                  textStyle: Theme.of(context)
                      .appBarTheme
                      .textTheme
                      .headline6
                      .copyWith(
                        color: Color(0xFF2684FF),
                        fontWeight: FontWeight.w600,
                      ),
                  onPressed: () {
                    context
                        .read<EditProfileCubit>()
                        .editProfileWebsiteSubmitted();
                  },
                ),
              ]
            : null,
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: BlocListener<EditProfileCubit, EditProfileState>(
          listenWhen: (previous, current) =>
              previous.website.status != current.website.status,
          listener: (context, state) {
            setState(() {
              isSaveEnabled = state.website.status.isValid;
            });

            if (state.website.status.isSubmissionSuccess) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Color(0xFF2684FF),
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_website_message_success',
                          ),
                        ),
                        const Icon(
                          Icons.check,
                          size: 44.0,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  ),
                );
            } else if (state.website.status.isSubmissionInProgress) {
              setState(() {
                isSaveEnabled = false;
              });
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_website_message_in_progress',
                          ),
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ),
                  ),
                );
            } else if (state.website.status.isSubmissionFailure) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    backgroundColor: Colors.red,
                    content: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: const Text(
                            'screen_edit_profile__step_edit_website_message_failed',
                          ),
                        ),
                        const Icon(Icons.warning),
                      ],
                    ),
                  ),
                );
            }
          },
          child: _WebsiteInput(),
        ),
      ),
    );
  }
}

class _WebsiteInput extends StatefulWidget {
  @override
  __WebsiteInputState createState() => __WebsiteInputState();
}

class __WebsiteInputState extends State<_WebsiteInput> {
  /// Text focus node for text field
  FocusNode _focusNode;

  /// Text controller for text field
  TextEditingController _textController;

  String _hintText = 'input_hint__add_website';
  String _labelText = 'input_label__website';

  EditProfileCubit _editProfileCubit;

  @override
  void initState() {
    super.initState();
    _editProfileCubit = BlocProvider.of<EditProfileCubit>(context);
    _textController = TextEditingController(
      text: _editProfileCubit.state.website.website.value,
    )..addListener(_onTextValueChanged);
    _focusNode = FocusNode()..addListener(_onTextFocusChanged);
  }

  void _onTextFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'input_hint__add_website';
        _labelText = 'input_label__website';
      });
    } else {
      if (_textController.text.isEmpty) {
        setState(() {
          _hintText = 'input_label__website';
          _labelText = 'input_hint__add_website';
        });
      }
    }
  }

  void _onTextValueChanged() {
    _editProfileCubit.editProfileWebsiteChanged(_textController.text);
  }

  String _populateErrorText(ProfileWebsite website) {
    if (website.website.invalid) {
      switch (website.website.error) {
        // case WebsiteValidationError.empty:
        //   return 'input_error_text_empty_enter_website';

        case WebsiteValidationError.invalidFormat:
          return 'input_error__invalid_format_website';

        default:
          print(
              'Exception (_STEP_CHANGE_WEBSITE_): ${website.website.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _textController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EditProfileCubit, EditProfileState>(
      buildWhen: (previous, current) =>
          previous.website.website != current.website.website,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateErrorText(state.website),
          ),
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _textController,
        );
      },
    );
  }
}
