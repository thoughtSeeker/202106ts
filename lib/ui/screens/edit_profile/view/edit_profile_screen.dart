import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../blocs/blocs.dart';
import '../../../../models/models.dart';
import 'widgets/widgets.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const EditProfileScreen(),
    );
  }

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  /// Light blue color
  Color lightBlue = Color(0xFF2684FF);

  /// Function for creating setting's item
  ///
  /// itemValue is a value of item, noValue text used when there is no value
  /// cvFlag has default false value, true is in case of cv
  Widget _createListItem(
    String itemName,
    String itemValue,
    String noValue,
    VoidCallback onTap, {
    bool cvFlag = false,
  }) {
    return Column(
      children: <Widget>[
        ListTile(
          enabled: false,
          contentPadding: EdgeInsets.zero,
          dense: true,
          title: Text(
            itemName,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ),
          ),
          // if cvFlag is false
          trailing: (cvFlag == false)
              ? Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  spacing: 8,
                  children: <Widget>[
                    // If there is itemValue
                    (itemValue != null)
                        ? Text(
                            itemValue,
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .color
                                  .withOpacity(0.7),
                            ),
                            overflow: TextOverflow.ellipsis,
                          )
                        :
                        // if there is no value
                        Text(
                            noValue,
                            style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .color
                                  .withOpacity(0.3),
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                    Icon(Icons.chevron_right),
                  ],
                ) // if cvFlag is true
              : Wrap(
                  spacing: 8,
                  children: <Widget>[
                    Text(
                      noValue,
                      style: TextStyle(
                        color: lightBlue,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                      ),
                    ),
                    Icon(
                      OMIcons.noteAdd,
                      color: lightBlue,
                      size: 20,
                    ),
                  ],
                ),
          onTap: onTap,
        ),
        Divider()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final User user =
        (context.read<AuthenticationBloc>().state as AuthenticationSuccess)
            .user;

    return BlocProvider(
      create: (BuildContext context) => EditProfileCubit(
        authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
      ),
      child: Scaffold(
        appBar: EditProfileAppBar(
          title: 'Edit',
          // title: 'screen_edit_profile__header_title',
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          child: ListView(
            padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0),
            children: <Widget>[
             /* Center(
                child: UserAvatarWithActions(
                  showPlusButton: true,
                  showTextButton: true,
                ),
              ),*/
              EditProfileText(
                title: 'Info',
                subtitle: '',
              ),
              _createListItem(
                'Name',
                user.name,
                'Add a name to your profile',
                () => Navigator.of(context).push<void>(
                  EditProfileName.route(),
                ),
              ),
              _createListItem(
                'Username',
                user.username,
                'Your username',
                () => Navigator.of(context).push<void>(
                  EditProfileUsername.route(),
                ),
              ),
             /* EditProfileText(
                title: 'screen_settings__list_header_private_title',
                subtitle: 'screen_settings__list_header_private_subtitle',
              ),*/
              _createListItem(
                'Email',
                user.email,
                'Your email',
                () => Navigator.of(context).push<void>(
                  EditProfileEmail.route(),
                ),
              ),
              _createListItem(
                'Password',
                '********',
                'Your password',
                () => Navigator.of(context).push<void>(
                  EditProfilePassword.route(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
