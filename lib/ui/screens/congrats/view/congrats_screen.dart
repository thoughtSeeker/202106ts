import 'package:flutter/material.dart';
import 'package:thoughtseekers/ui/screens/configure_screen_slider/view/configure_screen_slider_screen.dart';
import 'package:thoughtseekers/ui/screens/discover_insights/view/discover_insights_screen.dart';
import 'package:thoughtseekers/ui/screens/visualisation/view/visualisation_screen.dart';
import 'package:thoughtseekers/ui/widgets/custom_flat_button.dart';
import 'package:thoughtseekers/utils/singleton.dart';

import 'widgets/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: CongratsScreen("Toni and M"),
    );
  }
}

class CongratsScreen extends StatefulWidget {
  String userName;
  String email;

  CongratsScreen(this.userName, {this.email});

  static Route route(String toni, String email) {
    return MaterialPageRoute<void>(
      builder: (_) => CongratsScreen(toni, email: email),
    );
  }

  @override
  _CongratsScreenState createState() => _CongratsScreenState();
}

class _CongratsScreenState extends State<CongratsScreen> {
  @override
  Widget build(BuildContext context) {
    Singleton.practiceMod = false;
    String text = "Well done, ${widget.userName}!\n\nYour life is up to date.";
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: CongratsAppBar(),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          margin: EdgeInsets.symmetric(vertical: 150),
          color: Colors.transparent,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(child: Text(text, style: TextStyle(color: Colors.black, fontSize: 22))),
              Expanded(
                  child: Container(
                height: 20,
              )),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomFlatButton(
                    padding: EdgeInsets.all(
                      12.0,
                    ),
                    text: 'View stats',
                    textStyle: TextStyle(color: Colors.black, fontSize: 17),
                    onPressed: () => Navigator.of(context).push<void>(
                      VisualisationScreen.route(),
                    ),
                  ),
                 Text("")

                 /* CustomFlatButton(
                    padding: EdgeInsets.all(
                      12.0,
                    ),
                    text: 'Discover insights',
                    textStyle: TextStyle(color: Colors.black, fontSize: 17),
                    onPressed: () => Navigator.of(context).push<void>(
                      DiscoverInsightsScreen.route(),
                    ),
                  ),*/
                ],
              ),
            ],
          ),
        ));
  }
}
