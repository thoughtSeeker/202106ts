import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class CongratsAppBar extends StatelessWidget implements PreferredSizeWidget {
  final AppBar _appBar = AppBar();

  @override
  Widget build(BuildContext context) {
    TextStyle ts = TextStyle(fontSize: 18, color: Colors.black);
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: 22,
          color: Colors.blueGrey,
        ),
        onPressed: () => Navigator.of(context).pop<void>(),
      ),
      title: AutoSizeText(
        'Congrats',
        style: ts,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
