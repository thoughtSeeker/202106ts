import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../widgets/widgets.dart';
import 'widgets.dart';

/// Filter icon with search sheet on press
class FilterSearch extends StatefulWidget {
  @override
  FilterSearchState createState() => FilterSearchState();
}

class FilterSearchState extends State<FilterSearch> {
  /// Values for range slider
  RangeValues currentRangeValues;

  /// bool for cupertino switch
  bool switchValue;

  /// Bool values for category
  List<bool> categoryButtons;

  /// Bool values for language
  List<bool> languageButtons;

  /// Bool values for type
  List<bool> typeButtons;

  /// Bool values for days
  List<bool> daysButtons;

  /// Set default values for all buttons
  void _defaultState() {
    categoryButtons = [true, false, false];
    languageButtons = [true, false, false];
    typeButtons = [true, false, false];
    daysButtons = [false, false, false, false, false];

    currentRangeValues = RangeValues(0, 1000);

    switchValue = false;
  }

  Map<String, dynamic> _getAllValues() {
    Map<String, dynamic> map = <String, dynamic>{};

    map['categories'] = categoryButtons;
    map['languages'] = languageButtons;
    map['types'] = typeButtons;
    map['days'] = daysButtons;
    map['rangeValues'] = [currentRangeValues.start, currentRangeValues.end];
    map['switchValue'] = switchValue;

    return map;
  }

  /// Set buttons only one to be true
  void _pressButton(List<bool> list, int index) {
    for (int i = 0; i < list.length; i++)
      if (i == index)
        list[i] = true;
      else
        list[i] = false;

    _saveDataToPreferences();
  }

  /// Toogle single day button
  void _toogleDay(int index) {
    daysButtons[index] = !daysButtons[index];
    _saveDataToPreferences();
  }

  /// Set values of list1 from list2
  void _setList(List<bool> list1, dynamic list2) {
    int i = 0;
    for (var value in list2) list1[i++] = value as bool;
  }

  /// Loading data from shared preferences
  void _loadDataFromPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var preferencesData = preferences.getString('SETTINGS_SEARCH');

    if (preferencesData != null) {
      Map data = json.decode(preferencesData) as Map;

      _setList(categoryButtons, data['categories']);
      _setList(languageButtons, data['languages']);
      _setList(typeButtons, data['types']);
      _setList(daysButtons, data['days']);

      switchValue = data['switchValue'] as bool;
      currentRangeValues = RangeValues(
          data['rangeValues'][0] as double, data['rangeValues'][1] as double);
    }
  }

  /// Saving data to shared preferences
  void _saveDataToPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Map<String, dynamic> map = _getAllValues();

    var data = jsonEncode(map);
    await preferences.setString('SETTINGS_SEARCH', data);
  }

  /// Removing data from shared preferences
  void _clearFunction() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('SETTINGS_SEARCH');
  }

  @override
  void initState() {
    super.initState();
    _defaultState();
    _loadDataFromPreferences();
  }

  void _filterSearchSheet(BuildContext context) async {
    await showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(
          builder: (context, StateSetter setState) {
            return Container(
              height: MediaQuery.of(context).size.height * 0.8,
              padding: EdgeInsets.fromLTRB(24, 20, 24, 5),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    NavigationPill(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'screen_search__modal_filter_search_title',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        FlatButton(
                          child: Text(
                            'button__clear',
                            style: TextStyle(
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .fontSize,
                              fontWeight: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .fontWeight,
                              color: Colors.blue,
                            ),
                          ),
                          onPressed: () {
                            setState(_defaultState);
                            _clearFunction();
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Text(
                      'screen_search__modal_filter_search_category_title',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Wrap(
                      children: <Widget>[
                        CustomFilterButton(
                          text:
                              'screen_search__modal_filter_search_category_all'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(categoryButtons, 0);
                            });
                          },
                          value: categoryButtons[0],
                        ),

                        // CustomRaisedButton(
                        //   text: 'test test',
                        //   width: 10.0,
                        //   textStyle: TextStyle(
                        //     color:
                        // ignore: lines_longer_than_80_chars
                        //         categoryButtons[0] ? Colors.white : Colors.blue,
                        //   ),
                        //   color: categoryButtons[0]
                        //       ? Colors.blue
                        //       : Colors.blue.withOpacity(0.1),
                        //   onPressed: () {
                        //     setState(() {
                        //       _pressButton(categoryButtons, 0);
                        //     });
                        //   },
                        // ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_category_language'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(categoryButtons, 1);
                            });
                          },
                          value: categoryButtons[1],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_category_courses'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(categoryButtons, 2);
                            });
                          },
                          value: categoryButtons[2],
                        ),
                      ],
                    ),
                    Text(
                      'screen_search__modal_filter_search_language_title',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Row(
                      children: <Widget>[
                        CustomFilterButton(
                          text:
                              'screen_search__modal_filter_search_language_any'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(languageButtons, 0);
                            });
                          },
                          value: languageButtons[0],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_language_german'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(languageButtons, 1);
                            });
                          },
                          value: languageButtons[1],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_language_english'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(languageButtons, 2);
                            });
                          },
                          value: languageButtons[2],
                        ),
                      ],
                    ),
                    Text(
                      'screen_search__modal_filter_search_type_title',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Row(
                      children: <Widget>[
                        CustomFilterButton(
                          text: 'screen_search__modal_filter_search_type_all'
                              ,
                          onPressed: () {
                            setState(() {
                              _pressButton(typeButtons, 0);
                            });
                          },
                          value: typeButtons[0],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_type_video_call'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(typeButtons, 1);
                            });
                          },
                          value: typeButtons[1],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_type_phone_call'
                                  ,
                          onPressed: () {
                            setState(() {
                              _pressButton(typeButtons, 2);
                            });
                          },
                          value: typeButtons[2],
                        ),
                      ],
                    ),
                    Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'screen_search__modal_filter_search_price_range_title'
                              ,
                          style: Theme.of(context).textTheme.subtitle1,
                        ),
                        Text(
                          // ignore: lines_longer_than_80_chars
                          '\$${currentRangeValues.start.toStringAsFixed(1)}-\$${currentRangeValues.end.toStringAsFixed(1)}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize:
                                Theme.of(context).textTheme.subtitle1.fontSize,
                            color: Theme.of(context).textTheme.subtitle2.color,
                          ),
                        )
                      ],
                    ),
                    RangeSlider(
                      values: currentRangeValues,
                      min: 0,
                      max: 1000,
                      onChanged: (RangeValues values) {
                        setState(() {
                          currentRangeValues = values;
                        });
                      },
                    ),
                    Divider(),
                    Text(
                      'screen_search__modal_filter_search_availability_title'
                          ,
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    Row(
                      children: <Widget>[
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_availability_day_mon'
                                  ,
                          onPressed: () {
                            setState(() {
                              _toogleDay(0);
                            });
                          },
                          value: daysButtons[0],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_availability_day_tue'
                                  ,
                          onPressed: () {
                            setState(() {
                              _toogleDay(1);
                            });
                          },
                          value: daysButtons[1],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_availability_day_wed'
                                  ,
                          onPressed: () {
                            setState(() {
                              _toogleDay(2);
                            });
                          },
                          value: daysButtons[2],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_availability_day_thu'
                                  ,
                          onPressed: () {
                            setState(() {
                              _toogleDay(3);
                            });
                          },
                          value: daysButtons[3],
                        ),
                        CustomFilterButton(
                          text:
                              // ignore: lines_longer_than_80_chars
                              'screen_search__modal_filter_search_availability_day_fri'
                                  ,
                          onPressed: () {
                            setState(() {
                              _toogleDay(4);
                            });
                          },
                          value: daysButtons[4],
                        ),
                      ],
                    ),
                    Divider(),
                    ListTile(
                      contentPadding: EdgeInsets.all(0),
                      title: Text('button__anonymous_callers_allowed'),
                      trailing: CupertinoSwitch(
                        activeColor: Colors.blue,
                        value: switchValue,
                        onChanged: (value) {
                          setState(() {
                            switchValue = value;
                          });
                          _saveDataToPreferences();
                        },
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset('assets/icons/filter.svg'),
      onPressed: () => _filterSearchSheet(context),
    );
  }
}
