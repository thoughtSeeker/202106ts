import 'package:flutter/material.dart';

/// Buttons used in filter sheet
class CustomFilterButton extends StatelessWidget{

  /// Text in the button
  final String text;
  /// OnPressed function
  final VoidCallback onPressed;
  /// Bool if button is value or not
  final bool value;

  CustomFilterButton({
    Key key,
    @required this.text,
    @required this.onPressed,
    @required this.value}
  ) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right:10),
      child: ButtonTheme(
        minWidth: 50,
        child: FlatButton(
          onPressed: onPressed,
          textColor: value ? Colors.white : Colors.blue,
          color: value ? Colors.blue : Colors.blue.withOpacity(0.1),
          child: Text(text),
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
        ),
      ),
    );
  }
  
}