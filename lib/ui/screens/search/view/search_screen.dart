import 'package:flutter/material.dart';

import 'package:thoughtseekers/ui/widgets/user_preview.dart';

import '../../../widgets/widgets.dart';
import 'widgets/widgets.dart';

class SearchPageDelegate extends SearchDelegate<dynamic> {
  /// Loading users for list
  List<User> _loadUseers() {
    return [
      User('Helen Tran', 'Ernahugsexperte', 4.9, 124, true,
          'https://tinyurl.com/y5676xf9'),
      User('Pam Bailey', 'Ernahugsexperte', 4.9, 124, true,
          'https://tinyurl.com/y5676xf9'),
      User('Jenniffer Alson', 'Ernahugsexperte', 4.9, 124, false,
          'https://tinyurl.com/y5676xf9'),
      User('Jonathan Jefferson', 'Ernahugsexperte', 4.9, 124, false,
          'https://tinyurl.com/y5676xf9')
    ];
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [FilterSearch()];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return Center(child: Icon(Icons.search));
  }

  @override
  Widget buildResults(BuildContext context) {
    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<User> users = query.isEmpty
        ? _loadUseers()
        : _loadUseers()
            .where((x) => x.fullName
                .toString()
                .toLowerCase()
                .contains(query.toLowerCase()))
            .toList();

    return Container(
      color: Colors.white,
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            TabBar(
              labelColor: Theme.of(context).textTheme.bodyText1.color,
              indicatorColor: Theme.of(context).textTheme.bodyText1.color,
              tabs: <Widget>[
                Tab(child: Text('tab__users')),
                Tab(child: Text('tab__courses')),
              ],
            ),
            Expanded(
              flex: 1,
              child: TabBarView(
                children: <Widget>[
                  ListView.builder(
                    itemCount: users.length,
                    itemBuilder: (context, index) {
                      User user = users[index];
                      return UserPreview(
                          user.fullName.toString(),
                          user.bio.toString(),
                          user.rating,
                          user.followers,
                          user.following,
                          user.imgUrl.toString());
                    },
                  ),
                  Container(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

/// Temporary class for user
class User {
  /// First and last name
  String fullName;

  /// Biography
  String bio;

  /// User's rating
  double rating;

  /// User's followers
  double followers;

  /// Am i following user
  bool following;

  /// Profile picture url
  String imgUrl;

  User(this.fullName, this.bio, this.rating, this.followers, this.following,
      this.imgUrl);
}
