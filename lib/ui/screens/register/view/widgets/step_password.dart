import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/blocs/register/register_bloc.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/register/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

class StepPassword extends StatelessWidget {
  const StepPassword({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepPassword(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: RegisterAppBar(
        title: 'Enter assword',
        onPressed: () {
          context.read<RegisterBloc>().add(
                RegisterNextStepChanged(
                  nextStep: Steps.name,
                ),
              );
        },
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Positioned(
              left: 0,
              right: 0,
              bottom: 40.0,
              child: Center(
                child: Text(
                  'Do you to our terms of use and privacy policy',
                  style: const TextStyle(
                    color: Color(0xFF172B4D),
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    height: 1.4,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              padding: const EdgeInsets.symmetric(
                horizontal: 40.0,
              ),
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 73.0),
                  RegisterText(
                    bodyText1: 'Password',
                    bodyText2: '',
                  ),
                  const SizedBox(height: 32.0),
                  _PasswordInput(),
                  const SizedBox(height: 24.0),
                  _NextButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _PasswordInput extends StatefulWidget {
  @override
  __PasswordInputState createState() => __PasswordInputState();
}

class __PasswordInputState extends State<_PasswordInput> {
  FocusNode _focusNode;
  TextEditingController _passwordController;

  String _hintText = 'Enter password';
  String _labelText = 'Enter password';

  RegisterBloc _registerBloc;

  bool _showPassword;

  @override
  void initState() {
    super.initState();
    _showPassword = false;
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _passwordController = TextEditingController(
      text: _registerBloc.state.account.password.value,
    )..addListener(_onPasswordValueChanged);
    _focusNode = FocusNode()..addListener(_onPasswordFocusChanged);
  }

  void _onPasswordFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter password';
        _labelText = 'Enter password';
      });
    } else {
      if (_passwordController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter password';
          _labelText = 'Enter password';
        });
      }
    }
  }

  void _onPasswordValueChanged() {
    _registerBloc.add(
      RegisterPasswordChanged(
        password: _passwordController.text,
      ),
    );
  }

  String _populatePasswordErrorText(Password password) {
    if (password.invalid) {
      switch (password.error) {
        // case PasswordValidationError.empty:
        //   return 'input_error_text_empty_enter_password';

        case PasswordValidationError.invalidFormat:
          return 'Invalid format enter password';

        default:
          print('Exception (_STEP_PASSWORD_): ${password.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
      buildWhen: (previous, current) =>
          previous.account.password != current.account.password,
      builder: (BuildContext context, state) {
        return TextFormField(
          obscureText: !_showPassword,
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populatePasswordErrorText(state.account.password),
            suffixIcon: IconButton(
              alignment: Alignment.bottomRight,
              icon: FaIcon(
                _showPassword
                    ? FontAwesomeIcons.eyeSlash
                    : FontAwesomeIcons.eye,
                color: _showPassword ? Colors.grey[700] : Colors.grey[350],
                size: 18.0,
              ),
              onPressed: () {
                setState(() {
                  _showPassword = !_showPassword;
                });
              },
            ),
          ),
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _passwordController,
        );
      },
    );
  }
}

class _NextButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
//      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'button__next',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed:
              state.account.password.valid && state.account.status.isValidated
                  ? () => {
                        FocusScope.of(context).unfocus(),
                        context.read<RegisterBloc>().add(
                              RegisterNextStepChanged(
                                nextStep: Steps.completed,
                              ),
                            ),
                        context.read<RegisterBloc>().add(RegisterSubmitted()),
                      }
                  : null,
        );
      },
    );
  }
}
