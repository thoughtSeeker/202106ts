
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/blocs/register/register_bloc.dart';
import 'package:thoughtseekers/ui/screens/welcome/view/welcome_screen.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/register/models/models.dart';
import '../../../../widgets/widgets.dart';
import '../../../screens.dart';
import 'widgets.dart';

class StepEmail extends StatelessWidget {
  const StepEmail({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepEmail(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: RegisterAppBar(
        title: 'Register email',
        onPressed: () => Navigator.of(context).pushAndRemoveUntil<void>(
          WelcomeScreen.route(),
          (route) => false,
        ),
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40.0),
                        child: Column(
                          children: [
                            const SizedBox(height: 73.0),
                            RegisterText(
                              bodyText1:
                                  'Register email',
                              bodyText2:
                                  '',
                            ),
                            const SizedBox(height: 32.0),
                            _EmailInput(),
                            const SizedBox(height: 24.0),
                            _NextButton(),
                          ],
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      Padding(
                        padding: EdgeInsets.only(
                          bottom:
                              MediaQuery.of(context).viewInsets.bottom + 5.0,
                        ),
                        // child: EmailProviders(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 40.0,
              child: Center(
                child: Text(
                  'Do you_agree to our terms of use and privacy policy',
                  style: const TextStyle(
                    color: Color(0xFF172B4D),
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    height: 1.4,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _EmailInput extends StatefulWidget {
  @override
  __EmailInputState createState() => __EmailInputState();
}

class __EmailInputState extends State<_EmailInput> {
  FocusNode _focusNode;
  TextEditingController _emailController;

  String _hintText = 'Enter email';
  String _labelText = 'Enter email';

  RegisterBloc _registerBloc;

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController = TextEditingController(
      text: _registerBloc.state.account.email.value,
    )..addListener(_onEmailValueChanged);
    _focusNode = FocusNode()..addListener(_onEmailFocusChanged);
  }

  void _onEmailFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter email';
        _labelText = 'Enter email';
      });
    } else {
      if (_emailController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter email';
          _labelText = 'Enter email';
        });
      }
    }
  }

  void _onEmailValueChanged() {
    _registerBloc.add(
      RegisterEmailChanged(
        email: _emailController.text,
      ),
    );
  }

  String _populateEmailErrorText(Email email) {
    if (email.invalid) {
      switch (email.error) {
        case EmailValidationError.invalidFormat:
          return 'Invalid format enteremail';

        default:
          print('Exception (_STEP_EMAIL_): ${email.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
      buildWhen: (previous, current) =>
          previous.account.email != current.account.email,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateEmailErrorText(state.account.email),
            errorMaxLines: 2,
          ),
          keyboardType: TextInputType.emailAddress,
          textInputAction: TextInputAction.done,
          focusNode: _focusNode,
          controller: _emailController,
        );
      },
    );
  }
}

class _NextButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
//      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'button__next',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.account.email.valid
              ? () => {
                    FocusScope.of(context).unfocus(),
                    context.read<RegisterBloc>().add(
                          RegisterNextStepChanged(
                            nextStep: Steps.name,
                          ),
                        ),
                  }
              : null,
        );
      },
    );
  }
}
