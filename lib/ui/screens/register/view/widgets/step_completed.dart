import 'package:flutter/material.dart';

import '../../../../widgets/widgets.dart';
import '../../../screens.dart';
import 'widgets.dart';

class StepCompleted extends StatelessWidget {
  const StepCompleted({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepCompleted(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              const SizedBox(height: 73.0),
              // const LogoImage(),
              Stack(
                alignment: Alignment.center,
                children: [
                  Image.asset(
                    'assets/images/completed_background.png',
                  ),
                  Center(
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                      size: 44.0,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 30.0),
              RegisterText(
                bodyText1: 'Registration completed',
                bodyText2: '',
                textAlign: TextAlign.center,
              ),
              const Spacer(),
              CustomRaisedButton(
                text: 'Login',
                textStyle: const TextStyle(
                  color: Colors.white,
                ),
                color: const Color(0xFF2684FF),
                onPressed: () => Navigator.of(context).pushAndRemoveUntil<void>(
                  LoginScreen.route(),
                  (route) => false,
                ),
              ),
            ],
          ),
        ),
    );
  }
}
