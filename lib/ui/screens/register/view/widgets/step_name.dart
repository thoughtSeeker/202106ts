import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/blocs/register/register_bloc.dart';

import '../../../../../blocs/blocs.dart';
import '../../../../../blocs/register/models/models.dart';
import '../../../../widgets/widgets.dart';
import 'widgets.dart';

class StepName extends StatelessWidget {
  const StepName({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const StepName(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: RegisterAppBar(
        title: 'screen_register__step_name_header_title',
        onPressed: () {
          context.read<RegisterBloc>().add(
                RegisterNextStepChanged(
                  nextStep: Steps.email,
                ),
              );
        },
      ),
      body: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Positioned(
              left: 0,
              right: 0,
              bottom: 40.0,
              child: Center(
                child: Text(
                  'Do you agree to our terms of use and privacy policy' ,
                  style: const TextStyle(
                    color: Color(0xFF172B4D),
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    height: 1.4,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              padding: const EdgeInsets.symmetric(
                horizontal: 40.0,
              ),
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 73.0),
                  RegisterText(
                    bodyText1: 'Register name',
                    bodyText2: '',
                  ),
                  const SizedBox(height: 32.0),
                  _NameInput(),
                  const SizedBox(height: 24.0),
                  _NextButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _NameInput extends StatefulWidget {
  @override
  __NameInputState createState() => __NameInputState();
}

class __NameInputState extends State<_NameInput> {
  FocusNode _focusNode;
  TextEditingController _nameController;

  String _hintText = 'Enter name';
  String _labelText = 'Enter name';

  RegisterBloc _registerBloc;

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _nameController = TextEditingController(
      text: _registerBloc.state.account.name.value,
    )..addListener(_onNameValueChanged);
    _focusNode = FocusNode()..addListener(_onNameFocusChanged);
  }

  void _onNameFocusChanged() {
    if (_focusNode.hasFocus) {
      setState(() {
        _hintText = 'Enter name';
        _labelText = 'Enter name';
      });
    } else {
      if (_nameController.text.isEmpty) {
        setState(() {
          _hintText = 'Enter name';
          _labelText = 'Enter name';
        });
      }
    }
  }

  void _onNameValueChanged() {
    _registerBloc.add(
      RegisterNameChanged(
        name: _nameController.text,
      ),
    );
  }

  String _populateNameErrorText(Name name) {
    if (name.invalid) {
      switch (name.error) {
        // case NameValidationError.empty:
        //   return 'input_error_text_empty_enter_name';

        case NameValidationError.invalidFormat:
          return 'Invalid format enter name';

        default:
          print('Exception (_STEP_NAME_): ${name.error.toString()}');
          break;
      }
    }
    return null;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
      buildWhen: (previous, current) =>
          previous.account.name != current.account.name,
      builder: (BuildContext context, state) {
        return TextFormField(
          autofocus: true,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: _hintText,
            labelText: _labelText,
            errorText: _populateNameErrorText(state.account.name),
          ),
          textInputAction: TextInputAction.next,
          focusNode: _focusNode,
          controller: _nameController,
        );
      },
    );
  }
}

class _NextButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterState>(
//      buildWhen: (previous, current) =>
//          previous.account.status != current.account.status,
      builder: (context, state) {
        return CustomRaisedButton(
          text: 'button__next',
          textStyle: const TextStyle(
            color: Colors.white,
          ),
          color: const Color(0xFF2684FF),
          onPressed: state.account.name.valid
              ? () => {
                    FocusScope.of(context).unfocus(),
                    context.read<RegisterBloc>().add(
                          RegisterNextStepChanged(
                            nextStep: Steps.password,
                          ),
                        ),
                  }
              : null,
        );
      },
    );
  }
}
