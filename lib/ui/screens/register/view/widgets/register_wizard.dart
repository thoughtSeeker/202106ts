import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:thoughtseekers/blocs/register/register_bloc.dart';

import '../../../../../blocs/blocs.dart';
import 'widgets.dart';

class RegisterWizard extends StatefulWidget {
  final ValueSetter<Account> onComplete;

  const RegisterWizard({
    Key key,
    this.onComplete,
  }) : super(key: key);

  @override
  _RegisterWizardState createState() => _RegisterWizardState();
}

class _RegisterWizardState extends State<RegisterWizard> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listenWhen: (previous, current) =>
          (previous.account.nextStep != current.account.nextStep) ||
          (current.account.nextStep == Steps.completed),
      listener: (context, state) {
        if (state.account.nextStep == Steps.completed) {
          if (state.account.status.isSubmissionSuccess) {
            Scaffold.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  backgroundColor: Color(0xFF2684FF),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child:
                            const Text('Register success'),
                      ),
                      const Icon(
                        Icons.check,
                        size: 44.0,
                        color: Colors.green,
                      ),
                    ],
                  ),
                ),
              );
            _navigator.push<void>(StepCompleted.route());
          } else if (state.account.status.isSubmissionInProgress) {
            Scaffold.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child:
                            const Text('Register in progress'),
                      ),
                      const CircularProgressIndicator(),
                    ],
                  ),
                ),
              );
          } else if (state.account.status.isSubmissionFailure) {
            Scaffold.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  backgroundColor: Colors.red,
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child:
                            const Text('Register failed'),
                      ),
                      const Icon(Icons.warning),
                    ],
                  ),
                ),
              );
          }
          widget.onComplete(state.account);
        } else if (state.account.nextStep == Steps.email) {
          _navigator.push<void>(StepEmail.route());
        } else if (state.account.nextStep == Steps.name) {
          _navigator.push<void>(StepName.route());
        } else if (state.account.nextStep == Steps.password) {
          _navigator.push<void>(StepPassword.route());
        }
      },
      child: Navigator(
        key: _navigatorKey,
        onGenerateRoute: (_) => StepEmail.route(),
      ),
    );
  }
}
