import 'package:flutter/material.dart';

class RegisterAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final VoidCallback onPressed;
  final AppBar _appBar = AppBar();

  RegisterAppBar({Key key, this.title, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: true,
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back_ios,
          size: Theme.of(context).appBarTheme.iconTheme.size,
          color: Theme.of(context).appBarTheme.iconTheme.color,
        ),
        onPressed: onPressed ?? () => Navigator.of(context).pop(),
      ),
      title: Text(
        title,
        style: Theme.of(context).appBarTheme.textTheme.headline6,
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_appBar.preferredSize.height);
}
