import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thoughtseekers/blocs/register/register_bloc.dart';

import '../../../../blocs/blocs.dart';
import 'widgets/widgets.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key key}) : super(key: key);

  static Route<Account> route() {
    return MaterialPageRoute(
      builder: (_) => const RegisterScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    _complete(Account account) {
      print('complete: $account');
    }

    return BlocProvider(
      create: (BuildContext context) => RegisterBloc(),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: RegisterWizard(onComplete: _complete),
      ),
    );
  }
}
