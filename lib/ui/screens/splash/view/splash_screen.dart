import 'package:flutter/material.dart';

import 'package:thoughtseekers/ui/widgets/app_background.dart';
import 'package:thoughtseekers/ui/widgets/logo_image.dart';

import '../../../widgets/widgets.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
      builder: (_) => const SplashScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const LogoImage(width: 28.0),
            const SizedBox(width: 10.0),
            Text(
              'Thought Seekers',
              style: Theme.of(context).textTheme.bodyText2.copyWith(
                    color: Colors.white,
                    fontSize: 30.0,
                    height: 40.0 / 30.0,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
