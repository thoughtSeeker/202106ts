

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:thoughtseekers/ui/screens/home/view/home_screen.dart';
import 'package:thoughtseekers/ui/screens/intro/view/intro_screen.dart';
import 'package:thoughtseekers/ui/screens/main_menu/view/main_menu_screen.dart';
import 'package:thoughtseekers/ui/screens/reset_password/view/reset_password_screen.dart';
import 'package:thoughtseekers/ui/screens/splash/view/splash_screen.dart';
import 'package:thoughtseekers/ui/screens/welcome/view/welcome_screen.dart';

import 'blocs/blocs.dart';
import 'blocs/simple_bloc_observer.dart';
import 'repositories/repositories.dart';
import 'ui/screens/screens.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = SimpleBlocObserver();

  _setupLogging();

  final userRepository = UserRepository(
    authenticationApiProvider: AuthenticationApiProvider(),
    userApiProvider: UserApiProvider(),
  );

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) {
            return AuthenticationBloc(
              userRepository: userRepository,
            )..add(AuthenticationStarted(deepLink: null));
          },
        ),
        BlocProvider<ThemeBloc>(
          create: (context) => ThemeBloc(),
        ),
      ],
      child: AppView(),
    ),
  );
}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    print('## LOGGER: ${record.level.name}: ${record.time}: ${record.message}');
  });
}

class AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  final String _appTitle = 'Thought Seekers';
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, state) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          navigatorKey: _navigatorKey,
          title: _appTitle,
          theme: state.theme,
          builder: (context, child) {
            return BlocListener<AuthenticationBloc, AuthenticationState>(
              listener: (context, state) {
                if (state is AuthenticationResetPassword) {
                  _navigator.pushAndRemoveUntil<void>(
                    ResetPasswordScreen.route(),
                    (route) => false,
                  );
                  return;
                } else if (state is AuthenticationFailure) {
                  _navigator.pushAndRemoveUntil<void>(
                    // Booking.route(),
                    // WelcomeScreen.route(),
                    LoginScreen.route(),
                    // IntroScreen.route(),
                    (route) => false,
                  );
                  return;
                } else if (state is AuthenticationSuccess) {
                  _navigator.pushAndRemoveUntil<void>(
                    HomeScreen.route(),
                    (route) => false,
                  );
                  return;
                }
              },
              child: child,
            );
          },
          onGenerateRoute: (_) => WelcomeScreen.route(),
          // onGenerateRoute: (_) => SplashScreen.route(),
        );
      },
    );
  }
}
