class PrintIf {
  String className;
  bool testingIsOn = true;

  PrintIf(this.className, this.testingIsOn);

  printIf(String string) {
    if (testingIsOn) {
      print(string);
    }
  }
}
