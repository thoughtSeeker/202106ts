class FishAndStates{


  List<String> fishList = [
    'Bass',
    'Musky',
    'Walleye',
    'Catfish',
    'Saltwater',
    'Other'
  ];

  List<String> stateList = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Carolina',
    'North Dakota',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming'
  ];

  Map<String, int> fishInd = {
    'Bass': 1,
    'Musky': 2,
    'Walleye': 3,
    'Catfish': 4,
    'Saltwater': 5,
    'Other': 6
  };

  Map<String, int> stateInd = {
    'Alabama': 3,
    'Alaska': 5,
    'Arizona': 8,
    'Arkansas': 9,
    'California': 10,
    'Colorado': 11,
    'Connecticut': 12,
    'Delaware': 13,
    'Florida': 14,
    'Georgia': 15,
    'Hawaii': 16,
    'Idaho': 17,
    'Illinois': 18,
    'Indiana': 19,
    'Iowa': 20,
    'Kansas': 21,
    'Kentucky': 22,
    'Louisiana': 23,
    'Maine': 24,
    'Maryland': 25,
    'Massachusetts': 26,
    'Michigan': 27,
    'Minnesota': 28,
    'Mississippi': 29,
    'Missouri': 30,
    'Montana': 31,
    'Nebraska': 32,
    'Nevada': 33,
    'New Hampshire': 34,
    'New Jersey': 35,
    'New Mexico': 36,
    'New York': 37,
    'North Carolina': 38,
    'North Dakota': 39,
    'Ohio': 40,
    'Oklahoma': 41,
    'Oregon': 42,
    'Pennsylvania': 43,
    'Rhode Island': 44,
    'South Carolina': 45,
    'South Dakota': 46,
    'Tennessee': 47,
    'Texas': 48,
    'Utah': 49,
    'Vermont': 50,
    'Virginia': 51,
    'Washington': 52,
    'West Virginia': 53,
    'Wisconsin': 54,
    'Wyoming': 55
  };



  Map<int, String > intToFish = {
    1:'Bass',
    2:'Musky',
    3:'Walleye',
    4:'Catfish',
    5:'Saltwater',
    6:'Other'
  };

  Map<int, String> intToState = {
    3: 'Alabama',
    5: 'Alaska',
    8: 'Arizona',
    9:'Arkansas',
    10:'California',
    11:'Colorado',
    12:'Connecticut',
    13:'Delaware',
    14:'Florida',
    15:'Georgia',
    16:'Hawaii',
    17:'Idaho',
    18:'Illinois',
    19:'Indiana',
    20:'Iowa',
    21:'Kansas',
    22:'Kentucky',
    23:'Louisiana',
    24:'Maine',
    25:'Maryland',
    26:'Massachusetts',
    27:'Michigan',
    28:'Minnesota',
    29:'Mississippi',
    30:'Missouri',
    31:'Montana',
    32:'Nebraska',
    33:'Nevada',
    34:'New Hampshire',
    35:'New Jersey',
    36:'New Mexico',
    37:'New York',
    38:'North Carolina',
    39:'North Dakota',
    40:'Ohio',
    41:'Oklahoma',
    42:'Oregon',
    43:'Pennsylvania',
    44:'Rhode Island',
    45:'South Carolina',
    46:'South Dakota',
    47:'Tennessee',
    48:'Texas',
    49:'Utah',
    50:'Vermont',
    51:'Virginia',
    52:'Washington',
    53:'West Virginia',
    54:'Wisconsin',
    55:'Wyoming'
  };


   Map<String, int> getStateInd(){
    return stateInd;
  }
   Map<String, int> getFishInd(){
    return fishInd;
  }

  List<String> getFish(){
    return fishList;
  }
  List<String> getStates(){
    return stateList;
  }

  Map<int, String> getIntToFish(){
    return intToFish;
  }

  Map<int, String>  getIntToState(){
    return intToState;
  }
}