class Files {
  String lastPathComponent(String theURLString) {
    // http/asdsd/sfdf/sfd.png return sfd.png
    String ret = "";
    if (theURLString.length > 0) {
      if (theURLString.contains("/")) {
        List<String> queryStringParts = theURLString.split('/');
        if (queryStringParts.length > 0) {
          ret = queryStringParts[queryStringParts.length - 1];
        }
      } else {
        ret = theURLString;
      }
    } else {
      ret = "";
    }
    return ret;
  }

  String getFileNameFromURL(String theURLString) {
    String ret = "";
    bool foundFileName = false;
    if (theURLString.length > 0) {
      if (theURLString.contains("&")) {
        List<String> queryStringParts = theURLString.split('&');
        if (queryStringParts.length > 0) {
          for (String key in queryStringParts) {
            if (key.contains("saveAsFileName")) {
              if (theURLString.contains("&")) {
                List<String> fileNameParts = key.split('=');
                if (fileNameParts.length == 2) {
                  foundFileName = true;
                  ret = fileNameParts[1];
                }
              }
            }
          }
        }
      }
    }
    if (!foundFileName && theURLString.length > 0) {
      ret = lastPathComponent(theURLString);
    }
    return ret;
  }
}
