class ReturnValue {
  String className;

  ReturnValue(this.className, this.localPrint);

  bool localPrint = false;

  int intVal(String s, int defaultValue) {
    if (localPrint ?? false) {
      print(className ??
          '' +
              " " +
              "KIRFTGB returnValueString s = " +
              s +
              " defaultValue = " +
              defaultValue.toString());
    }
    int ret = 50;
    if (defaultValue == null) {
      return ret;
    } else {
      if (s.isEmpty) {
        return defaultValue;
      } else {
        if (s == null) {
          return defaultValue;
        } else {
          ret = int.parse(s);
        }
      }
    }
    return ret;
  }

  String stringVal(String s, String defaultValue) {
    if (localPrint ?? false) {
      print(className ??
          '' +
              " " +
              "KIRFTGB returnValueString s = " +
              s +
              " defaultValue = " +
              defaultValue.toString());
    }
    String ret = "#FFFFFF";
    if (defaultValue == null) {
      return ret;
    } else {
      if (s.isEmpty) {
        return defaultValue;
      } else {
        if (s == null) {
          return defaultValue;
        } else {
          ret = s;
        }
      }
    }
    if (localPrint ?? false) {
      print(className + " " + "KIRFTGB returnValueString ret = " + ret);
    }

    return ret;
  }
}
