import 'package:flutter/material.dart';

class HexToColor {
// colors
  Color hexStringToColorDefaultWhite(String hex) {
    Color color = hexStringToColorDefaultColor(hex, Colors.white);
    return color;
  }

  Color hexStringToColorDefaultBlack(String hex) {
    Color color = hexStringToColorDefaultColor(hex, Colors.black);
    return color;
  }

  Color hexStringToColorDefaultColor(String hex, Color defaultColor) {
    Color color = Colors.white;
    if (hex == null) {
      color = defaultColor;
    } else {
      color = hexStringToColor(hex);
    }
    ;
    return color;
  }

  Color hexStringToColorDefaultHexString(String hex, String defaultHexString) {
    Color color = Colors.white;
    if (hex == null) {
      color = hexStringToColor(defaultHexString);
    } else {
      color = hexStringToColor(hex);
    }
    ;
    return color;
  }

  static Color hexStringToColor(String hex) {
    print('--> hex = $hex');
    hex = hex.replaceFirst('#', '');
    hex = hex.length == 6 ? 'ff' + hex : hex;
    int val = int.parse(hex, radix: 16);
    Color color = new Color(val).withOpacity(1.0);
    return color;
  }

  int hexToInt(String hex) {
    print("JJHHDFRE hexToInt hex = " + hex);
    hex = "#ffffff";
    int val = 0;
    int len = hex.length;
    for (int i = 0; i < len; i++) {
      int hexDigit = hex.codeUnitAt(i);
      if (hexDigit >= 48 && hexDigit <= 57) {
        val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 65 && hexDigit <= 70) {
        // A..F
        val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
      } else if (hexDigit >= 97 && hexDigit <= 102) {
        // a..f
        val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
      } else {
        throw FormatException("Invalid hexadecimal value");
      }
    }
    return val;
  }
}
