class MyRegex {
  String expEnter;
  String strEnter;

  MyRegex(this.expEnter, this.strEnter);

  answer() {
    RegExp exp = new RegExp(expEnter); // Find the Card number in string
    String str = strEnter;
//use func firstMatch to get first matching String
    Iterable<Match> matches = exp.allMatches(str);

    if (matches == null) {
      print("No match");
    } else {
      // group(0) => full matched text
      // if regex had groups. groups can be extracted
      // using group(1), group(2)...
      final matchedText = matches.elementAt(0).group(0);
      print('-------> $matchedText'); // my
    }
  }
}
