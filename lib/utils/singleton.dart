library my_prj.globals;

import 'dart:async';
import 'dart:collection';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';

enum BiographyValidationError { empty, invalidFormat}

enum PhotoValidationError { empty, invalidFormat}

enum CompanyOrCollegeValidationError { empty, invalidFormat}

enum CompanyOrCollegeLocationValidationError { empty, invalidFormat}

enum DateValidationError { empty, invalidFormat}

enum EmailValidationError { empty, invalidFormat }

enum NameValidationError { empty, invalidFormat}

enum NewPasswordValidationError { empty, invalidFormat }

enum NewPasswordConfirmedValidationError { empty, invalidFormat, mismatch}

enum PasswordValidationError { empty, invalidFormat }

enum TitleValidationError { empty, invalidFormat}

enum UsernameValidationError { empty, invalidFormat }

enum WebsiteValidationError { empty, invalidFormat}

enum PasswordConfirmedValidationError { empty, invalidFormat, mismatch}


class Singleton {
  static bool practiceMod = false;
  static bool confirmCancel = false;
  static String readConfig = "";

  static String imageFileName = "";

  static bool downloadFinished = true;
  static bool firstRun = true;
  static bool pageDidLoad = false;
  static int selectedIndex = 0;
  static BuildContext context;
  static String nextScreenToLoad = "";

  // background color for the entire app
  static Color listBackgroundColor = Colors.white;

  static String strX;
  static String strY;
  static bool isIpad;
  static IosDeviceInfo iosDeviceInfo;
  static AndroidDeviceInfo androidDeviceInfo;
  static bool isIOS;
  static bool tabletDetector;
  static bool isMapView3;
  static String waterQualityImage;
  static List<bool> greenForBeaches = [];

  final mediaQueryData = MediaQuery.of(context);
  static double screenWidth = MediaQuery.of(context).size.width;
  static double screenHeight = MediaQuery.of(context).size.height;
  static int numberVisibleIcons = 8;

  static Queue screenIdQueue = new Queue<String>();

  static double horizontalIconsHeight = 90.0;
  static double horizontalIconsWidth = 90.0;

  static Size getSizes(GlobalKey key) {
    final RenderBox renderBoxRed =
        key.currentContext.findRenderObject() as RenderBox;
    final sizeRed = renderBoxRed.size;
    return sizeRed;
  }

// TextStyles start

  static TextStyle registerScreenHeaderText(Color col) {
    return new TextStyle(
      color: col,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle textStyleAlertDialog(
      Color color, String fontFamily, double fontSize) {
    return new TextStyle(
      color: color,
      fontFamily: fontFamily,
      // fontFamilyName,
      fontSize: fontSize,
      fontWeight: FontWeight.bold,
      letterSpacing: 1.1,
    );
  }

  // for NewOrderContent.dart
  static TextStyle textStyleHeader(
      Color color, String fontFamily, double fontSize) {
    return new TextStyle(
      color: color,
      fontFamily: fontFamily,
      fontSize: fontSize,
      fontWeight: FontWeight.bold,
      letterSpacing: 1.1,
    );
  }

  static TextStyle textStyleAStatusButtons(
      Color color, String fontFamily, double fontSize) {
    return new TextStyle(
      color: color,
      fontFamily: fontFamily,
      // fontFamilyName,
      fontSize: fontSize,
      fontWeight: FontWeight.normal,
      letterSpacing: 0.7,
    );
  }
}

/// Mutual class for streams to communicate
class SingletonStream {
  static StreamController<int> streamCounter = StreamController.broadcast();

  static int counter = 0;

}

class MyFmt {
  static double leftMargin = 10.0;
  static double rightMargin = 10.0;

  static double headerTop = 39.0;
  static double headerBottom = 0.0;
  static double normalTop = 10.0;
  static double normalBottom = 0.0;

  static double fontSize = 24.0;
  static double minusTen = 20.0;

  static TextStyle normalTextStyle() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Helvetica Regular',
      fontSize: 20.0,
      fontWeight: FontWeight.normal,
    );
  }

  static TextStyle headerTextStyle() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Helvetica Regular',
      fontSize: 25.0,
      fontWeight: FontWeight.bold,
    );
  }

  static double computeWidth() {
    double ret = 480 - leftMargin - rightMargin + minusTen;
//    double ret = widthLocal - leftMargin - rightMargin + minusTen;
    return ret;
  }

  static Container normalText(String text) {
    return Container(
      width: computeWidth(),
      padding: EdgeInsets.only(
          top: normalTop,
          left: leftMargin,
          right: rightMargin,
          bottom: normalBottom),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: computeWidth() - minusTen,
              child: Text(
                text,
                style: normalTextStyle(),
              ),
            ),
          ]),
    );
  }

  static Container headerText(String text) {
    return Container(
      width: computeWidth(),
      padding: EdgeInsets.only(
          top: headerTop,
          left: leftMargin,
          right: rightMargin,
          bottom: headerBottom),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: computeWidth() - minusTen,
              child: Center(
                child: Text(
                  text,
                  style: headerTextStyle(),
                ),
              ),
            ),
          ]),
    );
  }

  static Container centerImage(
      String imagename, double imageWidth, double imageHeight) {
    return Container(
      margin: const EdgeInsets.all(0.0),
      width: imageWidth,
      height: imageHeight,
      //   padding: const EdgeInsets.all(0.0),
      child: Center(
        child: Text(""),
      ),
      decoration: BoxDecoration(
        color: const Color(0xfffffff),
        image: new DecorationImage(
          image: new AssetImage(imagename),
          fit: BoxFit.cover,
        ),
        /*border: new Border.all(
              color: Colors.black,
              width: 0.0,
            ),*/
      ),
    );
  }
}

BoxDecoration localTesting(
    bool local, Color borderColor, Color boxColor, double width) {
  // localTesting(Colors.black,Colors.white, 4.0)
  // decoration: localTesting(Colors.black, Colors.white, 4.0),
  if (borderColor == null) borderColor = Colors.white;
  if (boxColor == null) boxColor = Colors.white;
  if (width == null) width = 0.0;
  if (local) {
    //print("KKDWMJJIU localTesting setBorderWhite index = " + width.toString());
  }
  if (local) {
    return BoxDecoration(
      color: boxColor,
      border: new Border.all(
        color: borderColor,
        width: width,
      ),
    );
  } else {
    return BoxDecoration(
        /*
          color: Colors.white,
          border: new Border.all(
            color: Colors.white,
            width: 0.0,
          ),*/
        );
  }
}

Future<bool> isIpad() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  IosDeviceInfo info = await deviceInfo.iosInfo;
  if (info.name.toLowerCase().contains("ipad")) {
    return true;
  }
  return false;
}

String assetsFolder = "assets/";
String imagesFolder = "images/";

class CommonThings {
  static String mainTitle = 'Orders \$99 and Over Ship Free!';
}

class IconType {
  final _value;

  const IconType._internal(this._value);

  // toString() => 'Enum.$_value';

  static const SHOWICONURL = const IconType._internal('SHOWICONURL');
  static const SHOWICONNAME = const IconType._internal('SHOWICONNAME');
  static const SHOWNOICON = const IconType._internal('SHOWNOICON');
}

class ImageType {
  final _value;

  const ImageType._internal(this._value);

  toString() => 'Enum.$_value';

  static const SHOWIMAGEURL = const ImageType._internal('SHOWIMAGEURL');
  static const SHOWIMAGENAME = const ImageType._internal('SHOWIMAGENAME');
  static const SHOWNOIMAGE = const ImageType._internal('SHOWNOIMAGE');
}

class AccessoryType {
  final _value;

  const AccessoryType._internal(this._value);

  toString() => 'Enum.$_value';

  static const DISCLOSUREINDICATOR =
      const AccessoryType._internal('DISCLOSUREINDICATOR');
  static const DETAILSDISCLOSUREINDICATOR =
      const AccessoryType._internal('DETAILSDISCLOSUREINDICATOR');
  static const CHECKMARK = const AccessoryType._internal('CHECKMARK');
  static const BLANK = const AccessoryType._internal('BLANK');
}
