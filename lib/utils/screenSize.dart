import 'dart:core';

import 'package:flutter/material.dart';

class ScreenSize extends StatelessWidget {
  static double topImageHeigth = 200.0;
  static double topImageHeight = topImageHeigth;
  static double imageWidthMenu = 280.0;
  static double imageHeigthMenu = 68.0;
  static double fontSize = 22.0;
  static double textFieldFontSize = 22.0;
  static double resultTextFontSize = 22.0;
  double screenWidth;
  double screenHeigth;
  String imgPath = "imgPath from class ScreenSize";

  ScreenSize(this.screenWidth, this.screenHeigth);

  bool equalsToDouble(double d, double what) {
    //debugPrint("DDRFTG equalsToDouble d = " + d.toString() + " what = " + what.toString());
    double epsilon = 0.0001;
    double dif = d - what;
    double difAbs = dif.abs();
    //debugPrint("DDRFGH  difAbs = " + difAbs.toString() + " epsilon = " +
    // epsilon.toString());
    if (difAbs < epsilon) {
      //debugPrint("DDERG equalsToDouble = TRUE");
      return true;
    } else {
      //  debugPrint("DDERG equalsToDouble = FALSE");
      return false;
    }
  }

  bool screenParameters(double d, double what) {
    topImageHeigth = screenHeigth / 7;
    topImageHeight = topImageHeigth;
    imageWidthMenu = 280.0;
    imageHeigthMenu = 68.0;
    fontSize = 22.0;
    resultTextFontSize = 28.0;

    // 5s, SE
    var wi = 320.0;
    var he = 568.0;
    if (equalsToDouble(screenWidth, wi) && equalsToDouble(screenHeigth, he)) {
      topImageHeigth = screenHeigth / 3;
      topImageHeight = topImageHeigth;
      imageWidthMenu = 280.0;
      imageHeigthMenu = 68.0;
      fontSize = 22.0;
      textFieldFontSize = 22.0;
      //  resultTextFontSize = 28.0;
      // debugPrint("JJIIJHDF discovered 5s portrait");

    }

    if (equalsToDouble(screenWidth, he) && equalsToDouble(screenHeigth, wi)) {
      topImageHeigth = screenHeigth / 5;
      topImageHeight = topImageHeigth;
      imageWidthMenu = 500.0;
      imageHeigthMenu = 68.0;
      fontSize = 22.0;
      textFieldFontSize = 22.0;
      //  resultTextFontSize = 22.0;
      //  debugPrint("JJIIJHDF discovered 5s landscape");

    }

    // X pixel 1125 x 2436   dot 375 x 812
    wi = 375.0;
    he = 812.0;
    if (equalsToDouble(screenWidth, wi) && equalsToDouble(screenHeigth, he)) {
      topImageHeigth = screenHeigth / 3;
      topImageHeight = topImageHeigth;
      imageWidthMenu = 280.0;
      imageHeigthMenu = 68.0;
      fontSize = 25.0;
      textFieldFontSize = 20.0;
      resultTextFontSize = 32.0;
      //  debugPrint("JJIIJHDF discovered X portrait");

    }

    if (equalsToDouble(screenWidth, he) && equalsToDouble(screenHeigth, wi)) {
      topImageHeigth = screenHeigth / 5;
      topImageHeight = topImageHeigth;
      imageWidthMenu = 500.0;
      imageHeigthMenu = 68.0;
      fontSize = 25.0;
      textFieldFontSize = 20.0;
      resultTextFontSize = 32.0;
      //  debugPrint("JJIIJHDF discovered X landscape");

    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
