import 'package:shared_preferences/shared_preferences.dart';

class StoreChoice{
  String preference;
  String content;
  StoreChoice(this.preference, this.content);

  Future<void> values(String preference, String content) async {

    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(preference, content);
  }
}
