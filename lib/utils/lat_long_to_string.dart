class LatLongToString {
  double latitude;
  double longitude;
  LatLongToString(this.latitude, this.longitude);

  String _getDirection(double val, [bool isLongitude = false]) {
    if ( !isLongitude )
      return val < 0 ? 'S' : 'N';
    else
      return val < 0 ? 'W' : 'E';
  }

  String latString(double latitude) {
    String direction = _getDirection(latitude);
    latitude = latitude.abs();
    int degrees = latitude.truncate();
    latitude = (latitude - degrees) * 60;
    int minutes = latitude.truncate();
    int seconds = ((latitude - minutes) * 60).truncate();
    return '$direction $degrees°$minutes\'$seconds\"';
  }

  String longString(double longitude) {
    String direction = _getDirection(longitude, true);
    longitude = longitude.abs();
    int degrees = longitude.truncate();
    longitude = (longitude - degrees) * 60;
    int minutes = longitude.truncate();
    int seconds = ((longitude - minutes) * 60).truncate();
    return '$direction $degrees°$minutes\'$seconds\"';
  }

  String converted(){
    String latConv = latString(this.latitude);
    String lonConv = longString(this.longitude);
    String ret = "\n" + latConv + "; " + lonConv;
    return ret;
  }

}