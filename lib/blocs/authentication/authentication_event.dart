part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthenticationStarted extends AuthenticationEvent {
  final DeepLink deepLink;

  AuthenticationStarted({this.deepLink});

  @override
  List<Object> get props => [deepLink];

  @override
  String toString() => 'AuthenticationStarted {deepLink: $deepLink}';
}

class AuthenticationLoggedIn extends AuthenticationEvent {
  final User user;

  AuthenticationLoggedIn({this.user});

  @override
  List<Object> get props => [user];

  @override
  String toString() => 'AuthenticationLoggedIn {user: $user}';
}

class AuthenticationLoggedOut extends AuthenticationEvent {}
