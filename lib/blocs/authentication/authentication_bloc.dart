import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';

import '../../models/models.dart';
import '../../repositories/repositories.dart';

part 'authentication_event.dart';

part 'authentication_state.dart';

enum UniLinksType { string, uri }

class DeepLink {
  final String email;
  final String token;

  DeepLink({
    this.email,
    this.token,
  });
}

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;

  final UniLinksType _type = UniLinksType.string;
  StreamSubscription _uniLinksSubscription;

  AuthenticationBloc({
    this.userRepository,
  })  : assert(userRepository != null),
        super(AuthenticationInitial()) {
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    if (_type == UniLinksType.string) {
      await initPlatformStateForStringUniLinks();
    } else {
      await initPlatformStateForUriUniLinks();
    }
  }

  /// An implementation using a [String] link
  Future<void> initPlatformStateForStringUniLinks() async {
    // Attach a listener to the links stream
/*
    _uniLinksSubscription = getLinksStream().listen((String link) {
      try {
        // TODO __debugging__
        print('latest link: $link');

        if (link != null) {
          String latestUri = Uri.decodeFull(link);

          final List<String> params = latestUri
              .substring(
                latestUri.toString().lastIndexOf('/password/reset/'),
              )
              .split('?email=');

          DeepLink deepLink = DeepLink(
            email: params[1],
            token: params[0],
          );
          add(AuthenticationStarted(deepLink: deepLink));
        }
      } on FormatException {
        // TODO __debugging__
        print('Failed to get latest link.');

        // TODO handle error?!
      }
    }, onError: (dynamic error) {
      // TODO __debugging__
      print('Failed to get latest link: $error.');

      // TODO handle error?!
    });
*/

    // Get the latest link
    String initialUri;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      // Get the latest link
      String initialLink = await getInitialLink();

      // TODO __debugging__
      print('authentication_bloc initial link: $initialLink');

      // convert latest link to uri format
      if (initialLink != null) {
        initialUri = Uri.decodeFull(initialLink);
      }
    } on PlatformException {
      // TODO __debugging__
      print('Failed to get initial link.');

      initialUri = null;
    } on FormatException {
      // TODO __debugging__
      print('Failed to parse the initial link as Uri.');

      // TODO handle error?!

      initialUri = null;
    }

    if (initialUri != null) {
      final List<String> params = initialUri
          .substring(
            initialUri.toString().lastIndexOf('/password/reset/'),
          )
          .split('?email=');

      DeepLink deepLink = DeepLink(
        email: params[1],
        token: params[0],
      );
      add(AuthenticationStarted(deepLink: deepLink));
    }
  }

  /// An implementation using the [Uri] convenience helpers
  Future<void> initPlatformStateForUriUniLinks() async {
    // Attach a listener to the Uri links stream
    _uniLinksSubscription = getUriLinksStream().listen((Uri uri) {
      // TODO __debugging__
      print('latest uri: $uri');

      DeepLink deepLink = DeepLink(
        email: '_email_latest_uri',
        token: '_token',
      );
      add(AuthenticationStarted(deepLink: deepLink));
    }, onError: (dynamic error) {
      // TODO __debugging__
      print('Failed to get latest link: $error.');

      // TODO handle error?!
    });

    // Get the latest Uri
    Uri initialUri;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      Uri initialUri = await getInitialUri();

      // TODO __debugging__
      print('initial uri: ${initialUri?.path}'
          ' ${initialUri?.queryParametersAll}');
    } on PlatformException {
      // TODO __debugging__
      print('Failed to get initial uri.');

      initialUri = null;
    } on FormatException {
      // TODO __debugging__
      print('Bad parse the initial link as Uri.');

      // TODO handle error?!

      initialUri = null;
    }

    if (initialUri != null) {
      DeepLink deepLink = DeepLink(
        email: '_email_initial_uri',
        token: '_token',
      );
      add(AuthenticationStarted(deepLink: deepLink));
    }
  }

  @override
  Future<void> close() {
    _uniLinksSubscription?.cancel();
    return super.close();
  }

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationStarted) {
      yield* _mapAuthenticationStartedToState(event);
    } else if (event is AuthenticationLoggedIn) {
      yield* _mapAuthenticationLoggedInToState(event);
    } else if (event is AuthenticationLoggedOut) {
      yield* _mapAuthenticationLoggedOutToState();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationStartedToState(
      AuthenticationStarted event) async* {
    try {
      final bool isLoggedIn = await userRepository.isLoggedIn();

      if (isLoggedIn) {
        final User _user = await userRepository.getUser();
        if (_user == null) {
          yield AuthenticationFailure();
        }
        yield AuthenticationSuccess(user: _user);
      } else {
        if (event.deepLink != null) {
          final _deepLink = event.deepLink;
          yield AuthenticationResetPassword(deepLink: _deepLink);
        } else {
          yield AuthenticationFailure();
        }
      }
    } catch (e) {
      yield AuthenticationFailure();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedInToState(
      AuthenticationLoggedIn event) async* {
    try {
      final User user = event.user;
      if (user == null) {
        yield AuthenticationFailure();
      }
      yield AuthenticationSuccess(user: user);
    } catch (e) {
      yield AuthenticationFailure();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedOutToState() async* {
    try {
      await userRepository.logout();
      yield AuthenticationFailure();
    } catch (e) {
      yield AuthenticationFailure();
    }
  }

  // to do empty, replace later
  getInitialLink() {}

  getInitialUri() {}

  getUriLinksStream() {}
}
