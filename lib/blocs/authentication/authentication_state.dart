part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationResetPassword extends AuthenticationState {
  final DeepLink deepLink;

  AuthenticationResetPassword({this.deepLink})
      : assert(deepLink != null);

  @override
  List<Object> get props => [deepLink];

  @override
  String toString() => 'AuthenticationResetPassword {deepLink: $deepLink}';
}

class AuthenticationFailure extends AuthenticationState {}

class AuthenticationInProgress extends AuthenticationState {}

class AuthenticationSuccess extends AuthenticationState {
  final User user;

  AuthenticationSuccess({this.user}) : assert(user != null);

  @override
  List<Object> get props => [user];

  @override
  String toString() => 'AuthenticationSuccess {user: $user}';
}
