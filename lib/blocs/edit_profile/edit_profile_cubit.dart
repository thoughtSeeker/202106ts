import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import '../../models/models.dart';
import '../../repositories/repositories.dart';
import '../blocs.dart';
import 'models/models.dart';

part 'edit_profile_state.dart';

class EditProfileCubit extends Cubit<EditProfileState> {
  final AuthenticationBloc authenticationBloc;
  final UserRepository _userRepository;

  EditProfileCubit({
    this.authenticationBloc,
    UserRepository userRepository,
  })  : assert(authenticationBloc != null),
        _userRepository = userRepository ??
            UserRepository(
              authenticationApiProvider: AuthenticationApiProvider(),
              userApiProvider: UserApiProvider(),
            ),
        super(EditProfileState.initial());

  // Edit profile photo
  Future<void> editProfilePhotoSubmitted(File photo) async {
    try {
      // if (!state.photo.status.isValidated) return;
      emit(
        state.copyWith(
          photo: state.photo.copyWith(
            status: FormzStatus.submissionInProgress,
          ),
        ),
      );
      await _userRepository.editProfilePhoto(
        photo: photo,
      );
      emit(
        state.copyWith(
          photo: state.photo.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );

      // await Future<void>.delayed(Duration(seconds: 1));
      final User user = await _userRepository.getUser();
      authenticationBloc.add(AuthenticationLoggedIn(user: user));
    } on Exception {
      emit(
        state.copyWith(
          photo: state.photo.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void editProfileNameChanged(String value) {
    final name = Name.dirty(value);
    emit(
      state.copyWith(
        name: state.name.copyWith(
          name: name,
          status: Formz.validate([name]),
        ),
      ),
    );
  }

  Future<void> editProfileNameSubmitted() async {
    if (!state.name.status.isValidated) return;
    emit(
      state.copyWith(
        name: state.name.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editName(
      //   name: state.name.name.value,
      // );
      emit(
        state.copyWith(
          name: state.name.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          name: state.name.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void editProfileUsernameChanged(String value) {
    final username = Username.dirty(value);
    emit(
      state.copyWith(
        username: state.username.copyWith(
          username: username,
          status: Formz.validate([username]),
        ),
      ),
    );
  }

  Future<void> editProfileUsernameSubmitted() async {
    if (!state.username.status.isValidated) return;
    emit(
      state.copyWith(
        username: state.username.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editUsername(
      //   username: state.username.username.value,
      // );
      emit(
        state.copyWith(
          username: state.username.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          username: state.username.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void editProfileWebsiteChanged(String value) {
    final website = Website.dirty(value);
    emit(
      state.copyWith(
        website: state.website.copyWith(
          website: website,
          status: Formz.validate([website]),
        ),
      ),
    );
  }

  Future<void> editProfileWebsiteSubmitted() async {
    if (!state.website.status.isValidated) return;
    emit(
      state.copyWith(
        website: state.website.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editWebsite(
      //   website: state.website.website.value,
      // );
      emit(
        state.copyWith(
          website: state.website.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          website: state.website.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void editProfileBiographyChanged(String value) {
    final biography = Biography.dirty(value);
    emit(
      state.copyWith(
        biography: state.biography.copyWith(
          biography: biography,
          status: Formz.validate([biography]),
        ),
      ),
    );
  }

  Future<void> editProfileBiographySubmitted() async {
    if (!state.biography.status.isValidated) return;
    emit(
      state.copyWith(
        biography: state.biography.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editBiography(
      //   biography: state.biography.biography.value,
      // );
      emit(
        state.copyWith(
          biography: state.biography.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          biography: state.biography.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void cvEntriesChanged(int value) {
    List<int> cvEntries = List.from(state.cvEntries.cvEntries)..add(value);
    emit(
      state.copyWith(
        cvEntries: state.cvEntries.copyWith(
          cvEntries: cvEntries,
          status:
              cvEntries.isEmpty == true ? FormzStatus.pure : FormzStatus.valid,
        ),
      ),
    );
  }

  Future<void> editCVEntriesSubmitted() async {
    // if (!state.cvEntries.status.isValidated) return;
    emit(
      state.copyWith(
        cvEntries: state.cvEntries.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));
      print('-------------------${state.cvEntries.cvEntries}');
      // await _userRepository.editCVEntries(
      //   cvEntries: state.cvEntries.cvEntries,
      // );
      emit(
        state.copyWith(
          cvEntries: state.cvEntries.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          cvEntries: state.cvEntries.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void editProfileEmailChanged(String value) {
    final email = Email.dirty(value);
    emit(
      state.copyWith(
        email: state.email.copyWith(
          email: email,
          status: Formz.validate([email]),
        ),
      ),
    );
  }

  Future<void> editProfileEmailSubmitted() async {
    if (!state.email.status.isValidated) return;
    emit(
      state.copyWith(
        email: state.email.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editEmail(
      //   email: state.email.email.value,
      // );
      emit(
        state.copyWith(
          email: state.email.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          email: state.email.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }

  void passwordChanged(String value) {
    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: state.password.copyWith(
          password: password,
          status: Formz.validate([
            password,
            state.password.newPassword,
            state.password.newPasswordConfirmed,
          ]),
        ),
      ),
    );
  }

  void newPasswordChanged(String value) {
    final newPassword = NewPassword.dirty(value);
    emit(
      state.copyWith(
        password: state.password.copyWith(
          newPassword: newPassword,
          status: Formz.validate([
            newPassword,
            state.password.password,
            state.password.newPasswordConfirmed,
          ]),
        ),
      ),
    );
  }

  void newPasswordConfirmedChanged(String value) {
    final newPasswordConfirmed = NewPasswordConfirmed.dirty(value);
    emit(
      state.copyWith(
        password: state.password.copyWith(
          newPasswordConfirmed: newPasswordConfirmed,
          status: Formz.validate([
            newPasswordConfirmed,
            state.password.password,
            state.password.newPassword,
          ]),
        ),
      ),
    );
  }

  Future<void> editPasswordSubmitted() async {
    if (!state.password.status.isValidated) return;
    emit(
      state.copyWith(
        password: state.password.copyWith(
          status: FormzStatus.submissionInProgress,
        ),
      ),
    );
    try {
      await Future<void>.delayed(Duration(seconds: 2));

      // await _userRepository.editPassword(
      //   password: state.password.password.value,
      //   newPassword: state.newPassword.newPassword.value,
      //   newPasswordConfirmed: state.newPasswordConfirmed.newPasswordConfirmed.value,
      // );
      emit(
        state.copyWith(
          password: state.password.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        ),
      );
    } on Exception {
      emit(
        state.copyWith(
          password: state.password.copyWith(
            status: FormzStatus.submissionFailure,
          ),
        ),
      );
    }
  }
}
