import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class NewPasswordConfirmed
    extends FormzInput<String, NewPasswordConfirmedValidationError> {
  const NewPasswordConfirmed.pure() : super.pure('');
  const NewPasswordConfirmed.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$',
  );

  @override
  NewPasswordConfirmedValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return NewPasswordConfirmedValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return NewPasswordConfirmedValidationError.invalidFormat;
    } else if (false) {
      // TODO mismatch
      return NewPasswordConfirmedValidationError.mismatch;
    } else {
      return null;
    }
  }
}
