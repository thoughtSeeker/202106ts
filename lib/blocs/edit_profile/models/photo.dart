import 'dart:io';

import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Biography extends FormzInput<File, PhotoValidationError> {
  const Biography.pure() : super.pure(null);
  const Biography.dirty([File value = null]) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,50}$',
  );

  @override
  PhotoValidationError validator(File value) {
    return null;
  }
}
