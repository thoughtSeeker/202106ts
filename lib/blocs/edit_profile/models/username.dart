import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Username extends FormzInput<String, UsernameValidationError> {
  const Username.pure() : super.pure('');
  const Username.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,30}$',
  );

  @override
  UsernameValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return UsernameValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return UsernameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
