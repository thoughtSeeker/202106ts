import 'package:thoughtseekers/utils/singleton.dart';
import 'package:formz/formz.dart';


class Biography extends FormzInput<String, BiographyValidationError> {
  const Biography.pure() : super.pure('');
  const Biography.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{0,160}$gm',
  );

  @override
  BiographyValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return BiographyValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return BiographyValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
