import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Name extends FormzInput<String, NameValidationError> {
  const Name.pure() : super.pure('');
  const Name.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,30}$',
  );

  @override
  NameValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return NameValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return NameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
