import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Title extends FormzInput<String, TitleValidationError> {
  const Title.pure() : super.pure('');
  const Title.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,50}$',
  );

  @override
  TitleValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return TitleValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return TitleValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
