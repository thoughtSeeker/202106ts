import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';

class CompanyOrCollegeLocation
    extends FormzInput<String, CompanyOrCollegeLocationValidationError> {
  const CompanyOrCollegeLocation.pure() : super.pure('');
  const CompanyOrCollegeLocation.dirty([String value = ''])
      : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,50}$',
  );

  @override
  CompanyOrCollegeLocationValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return CompanyOrCollegeLocationValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return CompanyOrCollegeLocationValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
