import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Website extends FormzInput<String, WebsiteValidationError> {
  const Website.pure() : super.pure('');
  const Website.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'(https?:\/\/)?([\w\-])+\.{1}([a-zA-Z]{2,63})([\/\w-]*)*\/?\??([^#\n\r]*)?#?([^\n\r]*)',
  );

  @override
  WebsiteValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return WebsiteValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return WebsiteValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
