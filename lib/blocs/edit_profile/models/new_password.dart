import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class NewPassword extends FormzInput<String, NewPasswordValidationError> {
  const NewPassword.pure() : super.pure('');
  const NewPassword.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$',
  );

  @override
  NewPasswordValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return NewPasswordValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return NewPasswordValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
