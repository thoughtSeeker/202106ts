import 'package:thoughtseekers/utils/singleton.dart';
import 'package:formz/formz.dart';


class CompanyOrCollege
    extends FormzInput<String, CompanyOrCollegeValidationError> {
  const CompanyOrCollege.pure() : super.pure('');
  const CompanyOrCollege.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,50}$',
  );

  @override
  CompanyOrCollegeValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return CompanyOrCollegeValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return CompanyOrCollegeValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
