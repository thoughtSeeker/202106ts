import 'package:formz/formz.dart';

enum EmailValidationError { empty, invalidFormat }

class Email extends FormzInput<String, EmailValidationError> {
  const Email.pure() : super.pure('');
  const Email.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$',
  );

  @override
  EmailValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return EmailValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return EmailValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
