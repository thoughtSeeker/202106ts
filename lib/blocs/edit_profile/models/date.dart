import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Date extends FormzInput<String, DateValidationError> {
  const Date.pure() : super.pure('');
  const Date.dirty([String value = '']) : super.dirty(value);

  // e.g.: dd.mm.yyyy 21.05.2020
  static final _regex =
      RegExp(r'^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$');

  // e.g.: yyyy-mm-dd 2020-05-21
  // static final _regex = RegExp(
  //   r'^\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$',
  // );

  @override
  DateValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return DateValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return DateValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
