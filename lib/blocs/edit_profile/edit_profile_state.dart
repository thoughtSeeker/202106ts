part of 'edit_profile_cubit.dart';

class ProfilePhoto extends Equatable {
  final File photo;
  final FormzStatus status;

  ProfilePhoto({
    this.photo,
    this.status = FormzStatus.pure,
  });

  ProfilePhoto copyWith({
    File photo,
    FormzStatus status,
  }) {
    return ProfilePhoto(
      photo: photo ?? this.photo,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [photo, status];

  @override
  bool get stringify => true;
}

class ProfileName extends Equatable {
  final Name name;
  final FormzStatus status;

  const ProfileName({
    this.name = const Name.pure(),
    this.status = FormzStatus.pure,
  });

  ProfileName copyWith({
    Name name,
    FormzStatus status,
  }) {
    return ProfileName(
      name: name ?? this.name,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [name, status];

  @override
  bool get stringify => true;
}

class ProfileUsername extends Equatable {
  final Username username;
  final FormzStatus status;

  const ProfileUsername({
    this.username = const Username.pure(),
    this.status = FormzStatus.pure,
  });

  ProfileUsername copyWith({
    Username username,
    FormzStatus status,
  }) {
    return ProfileUsername(
      username: username ?? this.username,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [username, status];

  @override
  bool get stringify => true;
}

class ProfileWebsite extends Equatable {
  final Website website;
  final FormzStatus status;

  const ProfileWebsite({
    this.website = const Website.pure(),
    this.status = FormzStatus.pure,
  });

  ProfileWebsite copyWith({
    Website website,
    FormzStatus status,
  }) {
    return ProfileWebsite(
      website: website ?? this.website,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [website, status];

  @override
  bool get stringify => true;
}

class ProfileBiography extends Equatable {
  final Biography biography;
  final FormzStatus status;

  const ProfileBiography({
    this.biography = const Biography.pure(),
    this.status = FormzStatus.pure,
  });

  ProfileBiography copyWith({
    Biography biography,
    FormzStatus status,
  }) {
    return ProfileBiography(
      biography: biography ?? this.biography,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [biography, status];

  @override
  bool get stringify => true;
}

class ProfileCVEntries extends Equatable {
  final List<int> cvEntries;
  final FormzStatus status;

  const ProfileCVEntries({
    this.cvEntries = const [],
    this.status = FormzStatus.pure,
  });

  ProfileCVEntries copyWith({
    List<int> cvEntries,
    FormzStatus status,
  }) {
    return ProfileCVEntries(
      cvEntries: cvEntries ?? this.cvEntries,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [cvEntries, status];

  @override
  bool get stringify => true;
}

class ProfileCVEntry extends Equatable {
  final Title title;
  final CompanyOrCollege companyOrCollege;
  final CompanyOrCollegeLocation companyOrCollegeLocation;
  final Date startDate;
  final Date endDate;
  final bool isCurrentWork;
  final FormzStatus status;

  const ProfileCVEntry({
    this.title = const Title.pure(),
    this.companyOrCollege = const CompanyOrCollege.pure(),
    this.companyOrCollegeLocation = const CompanyOrCollegeLocation.pure(),
    this.startDate = const Date.pure(),
    this.endDate = const Date.pure(),
    this.isCurrentWork = false,
    this.status = FormzStatus.pure,
  });

  ProfileCVEntry copyWith({
    Title title,
    CompanyOrCollege companyOrCollege,
    CompanyOrCollegeLocation companyOrCollegeLocation,
    Date startDate,
    Date endDate,
    bool isCurrentWork,
    FormzStatus status,
  }) {
    return ProfileCVEntry(
      title: title ?? this.title,
      companyOrCollege: companyOrCollege ?? this.companyOrCollege,
      companyOrCollegeLocation:
          companyOrCollegeLocation ?? this.companyOrCollegeLocation,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      isCurrentWork: isCurrentWork ?? this.isCurrentWork,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [
        title,
        companyOrCollege,
        companyOrCollegeLocation,
        startDate,
        endDate,
        isCurrentWork,
        status,
      ];

  @override
  bool get stringify => true;
}

class ProfileEmail extends Equatable {
  final Email email;
  final FormzStatus status;

  const ProfileEmail({
    this.email = const Email.pure(),
    this.status = FormzStatus.pure,
  });

  ProfileEmail copyWith({
    Email email,
    FormzStatus status,
  }) {
    return ProfileEmail(
      email: email ?? this.email,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [email, status];

  @override
  bool get stringify => true;
}

class ProfilePassword extends Equatable {
  final Password password;
  final NewPassword newPassword;
  final NewPasswordConfirmed newPasswordConfirmed;
  final FormzStatus status;

  const ProfilePassword({
    this.password = const Password.pure(),
    this.newPassword = const NewPassword.pure(),
    this.newPasswordConfirmed = const NewPasswordConfirmed.pure(),
    this.status = FormzStatus.pure,
  });

  ProfilePassword copyWith({
    Password password,
    NewPassword newPassword,
    NewPasswordConfirmed newPasswordConfirmed,
    FormzStatus status,
  }) {
    return ProfilePassword(
      password: password ?? this.password,
      newPassword: newPassword ?? this.newPassword,
      newPasswordConfirmed: newPasswordConfirmed ?? this.newPasswordConfirmed,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props =>
      [password, newPassword, newPasswordConfirmed, status];

  @override
  bool get stringify => true;
}

class EditProfileState extends Equatable {
  final ProfilePhoto photo;
  final ProfileName name;
  final ProfileUsername username;
  final ProfileWebsite website;
  final ProfileBiography biography;
  final ProfileCVEntries cvEntries;
  final ProfileCVEntry cvEntry;
  final ProfileEmail email;
  final ProfilePassword password;

  EditProfileState({
    this.photo,
    this.name,
    this.username,
    this.website,
    this.biography,
    this.cvEntries,
    this.cvEntry,
    this.email,
    this.password,
  });

  EditProfileState.initial()
      : this(
          photo: ProfilePhoto(),
          name: ProfileName(),
          username: ProfileUsername(),
          website: ProfileWebsite(),
          biography: ProfileBiography(),
          cvEntries: ProfileCVEntries(),
          cvEntry: ProfileCVEntry(),
          email: ProfileEmail(),
          password: ProfilePassword(),
        );

  EditProfileState copyWith({
    ProfilePhoto photo,
    ProfileName name,
    ProfileUsername username,
    ProfileWebsite website,
    ProfileBiography biography,
    ProfileCVEntries cvEntries,
    ProfileCVEntry cvEntry,
    ProfileEmail email,
    ProfilePassword password,
  }) {
    return EditProfileState(
      photo: photo ?? this.photo,
      name: name ?? this.name,
      username: username ?? this.username,
      website: website ?? this.website,
      biography: biography ?? this.biography,
      cvEntries: cvEntries ?? this.cvEntries,
      cvEntry: cvEntry ?? this.cvEntry,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  @override
  List<Object> get props => [
        photo,
        name,
        username,
        website,
        biography,
        cvEntries,
        cvEntry,
        email,
        password,
      ];
}
