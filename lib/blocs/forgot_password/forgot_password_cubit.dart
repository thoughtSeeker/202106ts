import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../../repositories/repositories.dart';
import 'models/models.dart';

part 'forgot_password_state.dart';

class ForgotPasswordCubit extends Cubit<ForgotPasswordState> {
  final UserRepository _userRepository;

  ForgotPasswordCubit({
    UserRepository userRepository,
  })  : _userRepository = userRepository ??
            UserRepository(
              authenticationApiProvider: AuthenticationApiProvider(),
              userApiProvider: UserApiProvider(),
            ),
        super(const ForgotPasswordState());

  void forgotPasswordEmailChanged(String value) {
    final email = Email.dirty(value);
    emit(state.copyWith(
      email: email,
      status: Formz.validate([email]),
    ));
  }

  Future<void> forgotPasswordSubmitted(String locale) async {
    if (!state.status.isValidated) return;
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      await _userRepository.forgotPassword(
        email: state.email.value,
        language: locale,
      );
      emit(state.copyWith(status: FormzStatus.submissionSuccess));
    } on Exception {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
