part of 'theme_bloc.dart';

class ThemeState extends Equatable {
  final ThemeData theme;

  const ThemeState({this.theme}) : assert(theme != null);

  @override
  List<Object> get props => [theme];

  @override
  String toString() => 'ThemeState { theme: $theme }';
}
