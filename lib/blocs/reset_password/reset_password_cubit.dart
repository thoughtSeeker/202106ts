import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../../repositories/repositories.dart';
import 'models/models.dart';

part 'reset_password_state.dart';

class ResetPasswordCubit extends Cubit<ResetPasswordState> {
  final UserRepository _userRepository;

  ResetPasswordCubit({
    UserRepository userRepository,
  })  : _userRepository = userRepository ??
            UserRepository(
              authenticationApiProvider: AuthenticationApiProvider(),
              userApiProvider: UserApiProvider(),
            ),
        super(const ResetPasswordState());

  void resetPasswordPasswordChanged(String value) {
    final password = Password.dirty(value);
    emit(state.copyWith(
      password: password,
      status: Formz.validate([password, state.passwordConfirmed]),
    ));
  }

  void resetPasswordPasswordConfirmedChanged(String value) {
    final passwordConfirmed = PasswordConfirmed.dirty(value);
    emit(state.copyWith(
      passwordConfirmed: passwordConfirmed,
      status: Formz.validate([passwordConfirmed, state.password]),
    ));
  }

  Future<void> resetPasswordSubmitted() async {
    if (!state.status.isValidated) return;
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      await _userRepository.resetPassword(
        password: state.password.value,
        passwordConfirmed: state.passwordConfirmed.value,
      );
      emit(state.copyWith(status: FormzStatus.submissionSuccess));
    } on Exception {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    }
  }
}
