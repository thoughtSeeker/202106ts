import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Password extends FormzInput<String, PasswordValidationError> {
  const Password.pure() : super.pure('');
  const Password.dirty([String value = '']) : super.dirty(value);

  static final _passwordRegex = RegExp(
    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$',
  );

  @override
  PasswordValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return PasswordValidationError.empty;
    } else if (_passwordRegex.hasMatch(value) != true) {
      return PasswordValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
