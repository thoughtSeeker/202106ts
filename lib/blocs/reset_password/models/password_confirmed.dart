import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class PasswordConfirmed
    extends FormzInput<String, PasswordConfirmedValidationError> {
  const PasswordConfirmed.pure() : super.pure('');
  const PasswordConfirmed.dirty([String value = '']) : super.dirty(value);

  static final _regex = RegExp(
    r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$',
  );

  @override
  PasswordConfirmedValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return PasswordConfirmedValidationError.empty;
    } else if (_regex.hasMatch(value) != true) {
      return PasswordConfirmedValidationError.invalidFormat;
    } else if (false) {
      // TODO mismatch
      return PasswordConfirmedValidationError.mismatch;
    } else {
      return null;
    }
  }
}

