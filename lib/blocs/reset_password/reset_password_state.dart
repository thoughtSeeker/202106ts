part of 'reset_password_cubit.dart';

class ResetPasswordState extends Equatable {
  final Password password;
  final PasswordConfirmed passwordConfirmed;
  final FormzStatus status;

  const ResetPasswordState({
    this.password = const Password.pure(),
    this.passwordConfirmed = const PasswordConfirmed.pure(),
    this.status = FormzStatus.pure,
  });

  ResetPasswordState copyWith({
    Password password,
    PasswordConfirmed passwordConfirmed,
    FormzStatus status,
  }) {
    return ResetPasswordState(
      password: password ?? this.password,
      passwordConfirmed: passwordConfirmed ?? this.passwordConfirmed,
      status: status ?? this.status,
    );
  }

  @override
  List<Object> get props => [password, passwordConfirmed, status];

  @override
  bool get stringify => true;
}
