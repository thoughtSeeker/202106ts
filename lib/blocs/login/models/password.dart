import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Password extends FormzInput<String, PasswordValidationError> {
  const Password.pure() : super.pure('');
  const Password.dirty([String value = '']) : super.dirty(value);

  @override
  PasswordValidationError validator(String value) {
    return value?.isEmpty == true
        ? PasswordValidationError.empty
        : null;
  }
}
