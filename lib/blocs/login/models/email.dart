import 'package:formz/formz.dart';
import 'package:thoughtseekers/utils/singleton.dart';


class Email extends FormzInput<String, EmailValidationError> {
  const Email.pure() : super.pure('');
  const Email.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError validator(String value) {
    return value?.isEmpty == true
        ? EmailValidationError.empty
        : null;
  }
}
