part of 'login_bloc.dart';


abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class LoginEmailChanged extends LoginEvent {
  final String email;

  LoginEmailChanged({this.email});

  @override
  List<Object> get props => [email];
}

class LoginPasswordChanged extends LoginEvent {
  final String password;

  LoginPasswordChanged({this.password});

  @override
  List<Object> get props => [password];
}

class LoginSubmitted extends LoginEvent {
}

class LoginWithGoogleSubmitted extends LoginEvent {}

class LoginWithFacebookSubmitted extends LoginEvent {}

class LoginWithAppleSubmitted extends LoginEvent {}
