import 'dart:async';

import 'package:thoughtseekers/ui/screens/main_menu/view/main_menu_screen.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';

import '../../models/models.dart';
import '../../repositories/repositories.dart';
import '../blocs.dart';
import 'models/models.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthenticationBloc authenticationBloc;
  final UserRepository _userRepository;


  LoginBloc({
    this.authenticationBloc,
    UserRepository userRepository,
  })  : assert(authenticationBloc != null),
        _userRepository = userRepository ??
            UserRepository(
              authenticationApiProvider: AuthenticationApiProvider(),
              userApiProvider: UserApiProvider(),
            ),
        super(const LoginState());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is LoginEmailChanged) {
      yield* _mapLoginEmailChangedToState(event);
    } else if (event is LoginPasswordChanged) {
      yield* _mapLoginPasswordChangedToState(event);
    } else if (event is LoginSubmitted) {
      yield* _mapLoginSubmittedToState(event);
    }
  }

  Stream<LoginState> _mapLoginEmailChangedToState(
      LoginEmailChanged event) async* {
    final email = Email.dirty(event.email);

    yield state.copyWith(
      email: email,
      status: Formz.validate([email, state.password]),
    );
  }

  Stream<LoginState> _mapLoginPasswordChangedToState(
      LoginPasswordChanged event) async* {
    final password = Password.dirty(event.password);

    yield state.copyWith(
      password: password,
      status: Formz.validate([state.email, password]),
    );
  }

  Stream<LoginState> _mapLoginSubmittedToState(LoginSubmitted event) async* {
    try {
      if (state.status.isValidated) {
        yield state.copyWith(
          status: FormzStatus.submissionInProgress,
        );
        await _userRepository.login(
          email: state.email.value,
          password: state.password.value,
        );
        yield state.copyWith(
          status: FormzStatus.submissionSuccess,
        );

        await Future<void>.delayed(Duration(seconds: 1));
        final User user = await _userRepository.getUser();
        authenticationBloc.add(
          AuthenticationLoggedIn(user: user),
        );
      }
    } on Exception catch (error) {
      yield state.copyWith(
        status: FormzStatus.submissionFailure,
      );
    }
  }

}
