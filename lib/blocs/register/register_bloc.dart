import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:formz/formz.dart';

import '../../repositories/repositories.dart';
import 'models/models.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({
    UserRepository userRepository,
  })  : _userRepository = userRepository ??
            UserRepository(
              authenticationApiProvider: AuthenticationApiProvider(),
              userApiProvider: UserApiProvider(),
            ),
        super(RegisterState.initial());

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterEmailChanged) {
      yield* _mapRegisterEmailChangedToState(event);
    } else if (event is RegisterNameChanged) {
      yield* _mapRegisterNameChangedToState(event);
    } else if (event is RegisterPasswordChanged) {
      yield* _mapRegisterPasswordChangedToState(event);
    } else if (event is RegisterNextStepChanged) {
      yield* _mapRegisterNextStepChangedToState(event);
    } else if (event is RegisterSubmitted) {
      yield* _mapRegisterSubmittedToState(event);
    }
  }

  Stream<RegisterState> _mapRegisterEmailChangedToState(
      RegisterEmailChanged event) async* {
    final email = Email.dirty(event.email);

    yield state.copyWith(
      account: state.account.copyWith(
        email: email,
        status: Formz.validate(
          [email, state.account.name, state.account.password],
        ),
      ),
    );
  }

  Stream<RegisterState> _mapRegisterNameChangedToState(
      RegisterNameChanged event) async* {
    final name = Name.dirty(event.name);

    yield state.copyWith(
      account: state.account.copyWith(
        name: name,
        status: Formz.validate(
          [state.account.email, name, state.account.password],
        ),
      ),
    );
  }

  Stream<RegisterState> _mapRegisterPasswordChangedToState(
      RegisterPasswordChanged event) async* {
    final password = Password.dirty(event.password);

    yield state.copyWith(
      account: state.account.copyWith(
        password: password,
        status: Formz.validate(
          [state.account.email, state.account.name, password],
        ),
      ),
    );
  }

  Stream<RegisterState> _mapRegisterNextStepChangedToState(
      RegisterNextStepChanged event) async* {
    final nextStep = event.nextStep;

    yield state.copyWith(
      account: state.account.copyWith(
        nextStep: nextStep,
        status: Formz.validate(
          [state.account.email, state.account.name, state.account.password],
        ),
      ),
    );
  }

  Stream<RegisterState> _mapRegisterSubmittedToState(
      RegisterSubmitted event) async* {
    try {
      if (state.account.status.isValidated) {
        yield state.copyWith(
          account: state.account.copyWith(
            status: FormzStatus.submissionInProgress,
          ),
        );

        await _userRepository.register(
          email: state.account.email.value,
          name: state.account.name.value,
          password: state.account.password.value,
        );
        yield state.copyWith(
          account: state.account.copyWith(
            status: FormzStatus.submissionSuccess,
          ),
        );
      }
    } on Exception catch (error) {
      print('ERROR: $error');
      yield state.copyWith(
        account: state.account.copyWith(
          status: FormzStatus.submissionFailure,
        ),
      );
    }
  }
}
