part of 'register_bloc.dart';

enum Steps { email, name, password, completed }

class Account extends Equatable {
  final Email email;
  final Name name;
  final Password password;
  final FormzStatus status;
  final Steps nextStep;

  const Account({
    this.email = const Email.pure(),
    this.name = const Name.pure(),
    this.password = const Password.pure(),
    this.status = FormzStatus.pure,
    this.nextStep = Steps.email,
  });

  Account copyWith({
    Email email,
    Name name,
    Password password,
    FormzStatus status,
    Steps nextStep,
  }) {
    return Account(
      email: email ?? this.email,
      name: name ?? this.name,
      password: password ?? this.password,
      status: status ?? this.status,
      nextStep: nextStep ?? this.nextStep,
    );
  }

  @override
  List<Object> get props => [email, name, password, status, nextStep];

  @override
  bool get stringify => true;
}

class RegisterState extends Equatable {
  final Account account;

  RegisterState({@required this.account});

  RegisterState.initial() : this(account: Account());

  RegisterState copyWith({Account account}) {
    return RegisterState(
      account: account ?? this.account,
    );
  }

  @override
  List<Object> get props => [account];
}
