import 'package:formz/formz.dart';

enum NameValidationError { empty, invalidFormat }

class Name extends FormzInput<String, NameValidationError> {
  const Name.pure() : super.pure('');
  const Name.dirty([String value = '']) : super.dirty(value);

  static final _nameRegex = RegExp(
    r'^[a-zA-Z0-9 .!#$%&’*+/=?^_`{|}~-]{2,30}$',
  );

  @override
  NameValidationError validator(String value) {
    if (value?.isEmpty == true) {
      return NameValidationError.empty;
    } else if (_nameRegex.hasMatch(value) != true) {
      return NameValidationError.invalidFormat;
    } else {
      return null;
    }
  }
}
