part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class RegisterEmailChanged extends RegisterEvent {
  final String email;

  RegisterEmailChanged({@required this.email});

  @override
  List<Object> get props => [email];
}

class RegisterNameChanged extends RegisterEvent {
  final String name;

  RegisterNameChanged({@required this.name});

  @override
  List<Object> get props => [name];
}

class RegisterPasswordChanged extends RegisterEvent {
  final String password;

  RegisterPasswordChanged({@required this.password});

  @override
  List<Object> get props => [password];
}

class RegisterNextStepChanged extends RegisterEvent {
  final Steps nextStep;

  RegisterNextStepChanged({@required this.nextStep});

  @override
  List<Object> get props => [nextStep];
}

class RegisterSubmitted extends RegisterEvent {}
