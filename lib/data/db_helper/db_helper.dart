import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sql;

/// Helper to access the local database
class DBHelper {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    String s1 = 'CREATE TABLE photos ';
    String s2 = '(id INTEGER PRIMARY KEY, name TEXT, path TEXT,';
    String s3 = 'latitudeTaken TEXT, longitudeTaken TEXT,';
    String s4 = 'latitudeUploaded TEXT, longitudeUploaded TEXT,';
    String s5 = 'dateTimeTaken TEXT, dateTimeUploaded TEXT)';
    String all = s1 + s2 + s3 + s4 + s5;
    print('---> all \n$all\n');
    return sql.openDatabase(join(dbPath, 'myPhotosPro.db'), version: 1,
        onCreate: (db, version) {
          db.execute(all);
        });
  }

  static Future<int> insert(String table, Map<String, Object> data) async {
    final sqlDb = await DBHelper.database();
    return sqlDb.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
  }

  static  Future<int> delete(String table, int id) async {
    final sqlDb = await DBHelper.database();
    return sqlDb.delete(table,where: 'id = ?',whereArgs: [id]);
  }

  static Future<List<Map<String, dynamic>>> getAll(String table) async {
    final sqlDb = await DBHelper.database();
    return sqlDb.query(table);
  }
}
