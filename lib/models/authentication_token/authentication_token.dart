import 'package:json_annotation/json_annotation.dart';

part 'authentication_token.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class AuthenticationToken {
  final String accessToken;
  final String tokenType;
  final String expiresAt;

  AuthenticationToken(
    this.accessToken,
    this.tokenType,
    this.expiresAt,
  );

  factory AuthenticationToken.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationTokenFromJson(json);

  Map<String, dynamic> toJson() => _$AuthenticationTokenToJson(this);
}
