// To parse this JSON data, do
//
//     final resultGuid = resultGuidFromJson(jsonString);

import 'dart:convert';

class ResultGuid {
  ResultGuid({
    this.fishTournamentId,
    this.fishTeamId,
    this.result,
    this.message,
  });

  final String fishTournamentId;
  final String fishTeamId;
  final String result;
  final String message;

  ResultGuid copyWith({
    String fishTournamentId,
    String fishTeamId,
    String result,
    String message,
  }) =>
      ResultGuid(
        fishTournamentId: fishTournamentId ?? this.fishTournamentId,
        fishTeamId: fishTeamId ?? this.fishTeamId,
        result: result ?? this.result,
        message: message ?? this.message,
      );

  factory ResultGuid.fromRawJson(String str) => ResultGuid.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ResultGuid.fromJson(Map<String, dynamic> json) => ResultGuid(
    fishTournamentId: json["fishTournamentId"] == null ? null : json["fishTournamentId"],
    fishTeamId: json["fishTeamId"] == null ? null : json["fishTeamId"],
    result: json["result"] == null ? null : json["result"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "fishTournamentId": fishTournamentId == null ? null : fishTournamentId,
    "fishTeamId": fishTeamId == null ? null : fishTeamId,
    "result": result == null ? null : result,
    "message": message == null ? null : message,
  };
}
