import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class User {
  User({
    this.id,
    this.userGroupId,
    this.username,
    this.email,
    this.membershipCode,
    this.firstName,
    this.lastName,
    this.guid,
    this.isActive,
    this.isEmailVerified,
    this.fishStatusId,
    this.verified,
    this.city,
    this.country,
    this.state,
    this.zipcode,
    this.photo,
    this.streetAddress,
    this.streetAddress2,
    this.streetNumber,
    this.created,
    this.modified,
  });

  final int id;
  final String userGroupId;
  final String username;
  final String email;
  final String membershipCode;
  final String firstName;
  final String lastName;
  final String guid;
  final int isActive;
  final int isEmailVerified;
  final String fishStatusId;
  final String verified;
  final String city;
  final String country;
  final String state;
  final String zipcode;
  final dynamic photo;
  final String streetAddress;
  final dynamic streetAddress2;
  final dynamic streetNumber;
  final DateTime created;
  final DateTime modified;

  User copyWith({
    int id,
    String userGroupId,
    String username,
    String email,
    String membershipCode,
    String firstName,
    String lastName,
    String guid,
    int isActive,
    int isEmailVerified,
    String fishStatusId,
    String verified,
    String city,
    String country,
    String state,
    String zipcode,
    dynamic photo,
    String streetAddress,
    dynamic streetAddress2,
    dynamic streetNumber,
    DateTime created,
    DateTime modified,
  }) =>
      User(
        id: id ?? this.id,
        userGroupId: userGroupId ?? this.userGroupId,
        username: username ?? this.username,
        email: email ?? this.email,
        membershipCode: membershipCode ?? this.membershipCode,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        guid: guid ?? this.guid,
        isActive: isActive ?? this.isActive,
        isEmailVerified: isEmailVerified ?? this.isEmailVerified,
        fishStatusId: fishStatusId ?? this.fishStatusId,
        verified: verified ?? this.verified,
        city: city ?? this.city,
        country: country ?? this.country,
        state: state ?? this.state,
        zipcode: zipcode ?? this.zipcode,
        photo: photo ?? this.photo,
        streetAddress: streetAddress ?? this.streetAddress,
        streetAddress2: streetAddress2 ?? this.streetAddress2,
        streetNumber: streetNumber ?? this.streetNumber,
        created: created ?? this.created,
        modified: modified ?? this.modified,
      );


  String get name => firstName + " " + lastName;
  String get profilePicture => photo;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
