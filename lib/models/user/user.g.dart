// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json["id"] == null ? null : json["id"],
    userGroupId: json["user_group_id"] == null ? null : json["user_group_id"],
    username: json["username"] == null ? null : json["username"],
    email: json["email"] == null ? null : json["email"],
    membershipCode: json["membership_code"] == null ? null : json["membership_code"],
    firstName: json["first_name"] == null ? null : json["first_name"],
    lastName: json["last_name"] == null ? null : json["last_name"],
    guid: json["guid"] == null ? null : json["guid"],
    isActive: json["is_active"] == null ? null : json["is_active"],
    isEmailVerified: json["is_email_verified"] == null ? null : json["is_email_verified"],
    fishStatusId: json["fish_status_id"] == null ? null : json["fish_status_id"],
    verified: json["verified"] == null ? null : json["verified"],
    city: json["city"] == null ? null : json["city"],
    country: json["country"] == null ? null : json["country"],
    state: json["state"] == null ? null : json["state"],
    zipcode: json["zipcode"] == null ? null : json["zipcode"],
    photo: json["photo"],
    streetAddress: json["street_address"] == null ? null : json["street_address"],
    streetAddress2: json["street_address2"],
    streetNumber: json["street_number"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    modified: json["modified"] == null ? null : DateTime.parse(json["modified"]),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
  "id": instance.id == null ? null : instance.id,
  "user_group_id": instance.userGroupId == null ? null : instance.userGroupId,
  "username": instance.username == null ? null : instance.username,
  "email": instance.email == null ? null : instance.email,
  "membership_code": instance.membershipCode == null ? null : instance.membershipCode,
  "first_name": instance.firstName == null ? null : instance.firstName,
  "last_name": instance.lastName == null ? null : instance.lastName,
  "guid": instance.guid == null ? null : instance.guid,
  "is_active": instance.isActive == null ? null : instance.isActive,
  "is_email_verified": instance.isEmailVerified == null ? null : instance.isEmailVerified,
  "fish_status_id": instance.fishStatusId == null ? null : instance.fishStatusId,
  "verified": instance.verified == null ? null : instance.verified,
  "city": instance.city == null ? null : instance.city,
  "country": instance.country == null ? null : instance.country,
  "state": instance.state == null ? null : instance.state,
  "zipcode": instance.zipcode == null ? null : instance.zipcode,
  "photo": instance.photo == null ? null : instance.zipcode,
  "street_address": instance.streetAddress == null ? null : instance.streetAddress,
  "street_address2": instance.streetAddress2,
  "street_number": instance.streetNumber,
  "created": instance.created == null ? null : instance.created.toIso8601String(),
  "modified": instance.modified == null ? null : instance.modified.toIso8601String(),
};
