import 'dart:async';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:logging/logging.dart';

import '../models/models.dart';
import 'repositories.dart';

class AuthenticationApiProvider {
  // define logger
  final _logger = Logger('AuthenticationApiProvider');

  final GoogleSignIn _googleSignIn;
  final FacebookLogin _facebookSignIn;
  final _storageApiProvider = StorageApiProvider();
  final _restApi = BaseApiProvider();

  AuthenticationApiProvider({
    GoogleSignIn googleSignIn,
    FacebookLogin facebookSignIn,
  })  : _googleSignIn = googleSignIn ?? GoogleSignIn.standard(),
        _facebookSignIn = facebookSignIn ?? FacebookLogin();

  /// Starts the Register
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> register({
    @required String email,
    @required String name,
    @required String password,
  }) async {
    try {
      Response response = await _restApi.dio.post<void>(
        _restApi.registerUrl,
        data: {
          'email': email,
          'name': name,
          'password': password,
        },
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __register__: $e');
      rethrow;
    }
  }

  /// Starts the Login with credentials
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> login({
    @required String email,
    @required String password,
  }) async {
    try {
      Response response = await _restApi.dio.post<void>(
        _restApi.loginUrl,
        data: {
          'email': email,
          'password': password,
        },
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __login__: $e');
      rethrow;
    }
  }

  /// Starts the Sign In with Google Flow.
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> logInWithGoogle() async {
    try {
      final googleUser = await _googleSignIn.signIn();
      final googleAuth = await googleUser.authentication;

      Response response = await _restApi.dio.post<void>(
        _restApi.loginWithGoogleUrl,
        data: {
          'accessToken': googleAuth.accessToken,
        },
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } on Exception catch (e) {
      print('Exception __loginWithGoogle__: $e');
      rethrow;
    }
  }

  /// Starts the Sign In with Facebook Flow.
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> logInWithFacebook() async {
    try {
      final FacebookLoginResult result = await _facebookSignIn.logIn(['email']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final FacebookAccessToken accessToken = result.accessToken;

          Response response = await _restApi.dio.post<void>(
            _restApi.loginWithFacebookUrl,
            data: {
              'accessToken': accessToken.token,
            },
          );

          if (response.statusCode != 200) {
            throw Exception(response);
          }
          break;
        case FacebookLoginStatus.cancelledByUser:
          throw Exception('Login cancelled by the user.');
          break;
        case FacebookLoginStatus.error:
          throw Exception(result.errorMessage);
          break;
      }
    } on Exception catch (e) {
      print('Exception __loginWithFacebook__: $e');
      rethrow;
    }
  }

  /// Starts the Sign In with Apple flow.
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> logInWithApple() async {
    try {
      throw Exception('TODO need to be implement!');
    } on Exception catch (e) {
      print('Exception __loginWithApple__: $e');
      rethrow;
    }
  }

  /// Starts the Logout process
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<void> logout() async {
    try {
      Response response = await _restApi.dio.post<void>(_restApi.logoutUrl);

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __logout__: $e');
      rethrow;
    }
  }

  Future<bool> isLoggedIn() async {
    // await Future<dynamic>.delayed(Duration(seconds: 1));

    final AuthenticationToken authenticationToken =
    await _storageApiProvider.hasAuthenticationToken();

    return authenticationToken != null;
  }

  /// Get authenticated user data.
  ///
  /// Throws a [Exception] if an exception occurs.
  Future<User> getUser() async {
    try {
      Response response = await _restApi.dio.post<void>(_restApi.authUserUrl);

      if (response.statusCode != 200) {
        throw Exception(response);
      }

      final user = User.fromJson(
        response.data as Map<String, dynamic>,
      );

      return user;
    } catch (e) {
      print('Exception __getUser__: $e');
      rethrow;
    }
  }
}
