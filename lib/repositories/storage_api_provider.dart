import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/models.dart';

const storageKeyAuthenticationToken = 'AUTHENTICATION_TOKEN';

class StorageApiProvider {
  Future<AuthenticationToken> hasAuthenticationToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final String _authenticationTokenJson =
        prefs.getString(storageKeyAuthenticationToken);

    Map<String, dynamic> map =
        convert.jsonDecode(_authenticationTokenJson) as Map<String, dynamic>;

    final AuthenticationToken authenticationToken =
        AuthenticationToken.fromJson(map);

    return authenticationToken;
  }

  Future<void> persistAuthenticationToken(
      AuthenticationToken authenticationToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final String authenticationTokenJson =
        convert.jsonEncode(authenticationToken);

    await prefs.setString(
        storageKeyAuthenticationToken, authenticationTokenJson);
    return;
  }

  Future<void> deleteAuthenticationToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(storageKeyAuthenticationToken);
  }
}
