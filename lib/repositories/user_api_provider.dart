import 'dart:io';

import 'package:meta/meta.dart';
import 'package:dio/dio.dart';


import 'repositories.dart';

class UserApiProvider {
  // define logger

  // initialize rest api
  final _restApi = BaseApiProvider();

  Future<void> forgotPassword({
    String email,
    String language,
  }) async {
    try {
      Response response = await _restApi.dio.post<void>(
        _restApi.forgotPasswordUrl,
        data: {
          'email': email,
          'language': language,
        },
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __forgotPassword__: $e');
      rethrow;
    }
  }

  Future<void> resetPassword({
    String password,
    String passwordConfirmed,
  }) async {
    try {
      Response response = await _restApi.dio.post<void>(
        _restApi.resetPasswordUrl,
        data: {
          'password': password,
          'passwordConfirmed': passwordConfirmed,
        },
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __resetPassword__: $e');
      rethrow;
    }
  }

  Future<void> editProfilePhoto({
    File photo,
  }) async {
    try {
      String filename = photo.path.split('/').last;


      FormData formData = FormData();
      formData.files.add(MapEntry(
        'file',
        await MultipartFile.fromFile(photo.path, filename: filename),
      ));

      Response response = await _restApi.dio.post<void>(
        _restApi.editProfilePhotoUrl,
        data: formData,
      );

      if (response.statusCode != 200) {
        throw Exception(response);
      }
    } catch (e) {
      print('Exception __editProfilePhoto__: $e');
      rethrow;
    }
  }
}
