import 'package:dio/dio.dart';
import 'package:logging/logging.dart';

import '../models/models.dart';
import 'repositories.dart';

///
enum ApiEnvironments { prod, test, dev }

///
final ApiEnvironments _apiEnvironment = ApiEnvironments.prod;

///
final Map<String, String> _apiEndpoints = <String, String>{
  ///
  'prodBaseUrl': 'https://talkila.com/api/v1',

  ///
  'testBaseUrl': 'http://192.168.10.136/talkila.com/public/api/v1',

  ///
  'devBaseUrl': 'http://192.168.10.136/talkila.com/public/api/v1',
  // 'devBaseUrl': 'http://192.168.99.101/api/v1',

  ///
  'registerUrl': '/register',

  ///
  'loginUrl': '/login',

  ///
  'loginWithGoogleUrl': '/login-with-google',

  ///
  'loginWithFacebookUrl': '/login-with-facebook',

  ///
  'loginWithAppleUrl': '/login-with-apple',

  ///
  'forgotPasswordUrl': '/forgot-password',

  ///
  'resetPasswordUrl': '/reset-password',

  ///
  'authUserUrl': '/user',

  ///
  'logoutUrl': '/logout',

  ///
  'editProfilePhotoUrl': '/edit-profile-photo'
};

///
class BaseApiProvider {
  // define logger
  final _logger = Logger('BaseApiProvider');

  static final BaseApiProvider _singleton = BaseApiProvider._internal();

  factory BaseApiProvider() {
    return _singleton;
  }

  BaseApiProvider._internal();

  ///
  String _getApiBaseUrl() {
    switch (_apiEnvironment) {
      case ApiEnvironments.prod:
        return _apiEndpoints['prodBaseUrl'];

      case ApiEnvironments.test:
        return _apiEndpoints['testBaseUrl'];

      case ApiEnvironments.dev:
        return _apiEndpoints['devBaseUrl'];

      default:
        throw Exception('API Environment is not valid!');
        break;
    }
  }

  String get baseUrl => _getApiBaseUrl();
  String get registerUrl => _apiEndpoints['registerUrl'];
  String get loginUrl => _apiEndpoints['loginUrl'];
  String get loginWithGoogleUrl => _apiEndpoints['loginWithGoogleUrl'];
  String get loginWithFacebookUrl => _apiEndpoints['loginWithFacebookUrl'];
  String get loginWithAppleUrl => _apiEndpoints['loginWithAppleUrl'];
  String get forgotPasswordUrl => _apiEndpoints['forgotPasswordUrl'];
  String get resetPasswordUrl => _apiEndpoints['resetPasswordUrl'];
  String get authUserUrl => _apiEndpoints['authUserUrl'];
  String get logoutUrl => _apiEndpoints['logoutUrl'];
  String get editProfilePhotoUrl => _apiEndpoints['editProfilePhotoUrl'];

  Dio get dio {
    BaseOptions options2 = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    Dio dio = Dio(options2);

    // initialize storage api provider
    final _storageApiProvider = StorageApiProvider();
    dio.interceptors.add(InterceptorsWrapper(
        onRequest:(options, handler) async {
          // Do something before request is sent
          AuthenticationToken authenticationToken =
              await _storageApiProvider.hasAuthenticationToken();

          // set the Authorization header
          if (authenticationToken != null) {
            options.headers['Authorization'] =
            '${authenticationToken.tokenType} '
                '${authenticationToken.accessToken}';
          }

          // log request
          _logger.info(
            'Request: ${options.method}, ${options.baseUrl}${options.path} '
                'Headers: ${options.headers.toString()}',
          );

          return handler.next(options); //continue
          // If you want to resolve the request with some custom data，
          // you can resolve a `Response` object eg: return `dio.resolve(response)`.
          // If you want to reject the request with a error message,
          // you can reject a `DioError` object eg: return `dio.reject(dioError)`
        },
        onResponse:(response,handler) async {
          // Do something with response data
          final int statusCode = response.statusCode;

          // response is ok
          if (statusCode == 200 || statusCode == 201) {
            // save the authentication token from response
            if (response.data.path == loginUrl ||
                response.data.path == loginWithGoogleUrl ||
                response.data.path == loginWithFacebookUrl ||
                response.data.path == loginWithAppleUrl) {
              final authenticationToken = AuthenticationToken.fromJson(
                response.data as Map<String, dynamic>,
              );
              await _storageApiProvider.persistAuthenticationToken(
                authenticationToken,
              );
            }
            // delete the authentication token from storage
            if (response.data.path == logoutUrl) {
              await _storageApiProvider.deleteAuthenticationToken();
            }
          }
          // log response
          _logger.info(
            'Response: ${response.data.method}, '
                '${response.data.baseUrl}${response.data.path} '
                '${response.toString()}',
          );

          // continue
          return handler.next(response); // continue
          // If you want to reject the request with a error message,
          // you can reject a `DioError` object eg: return `dio.reject(dioError)`
        },
        onError: (DioError e, handler) {
          // Do something with response error
          return  handler.next(e);//continue
          // If you want to resolve the request with some custom data，
          // you can resolve a `Response` object eg: return `dio.resolve(response)`.
        }
    ));


    return dio;
  }
}
