import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../models/models.dart';
import 'repositories.dart';

class UserRepository {
  final AuthenticationApiProvider authenticationApiProvider;
  final UserApiProvider userApiProvider;

  UserRepository({
    this.authenticationApiProvider,
    this.userApiProvider,
  }) : assert(
          authenticationApiProvider != null,
          userApiProvider != null,
        );

  Future<void> login({
    String email,
    String password,
  }) async {
    await authenticationApiProvider.login(
      email: email,
      password: password,
    );
  }

  Future<void> logout() async {
    await authenticationApiProvider.logout();
  }

  Future<bool> isLoggedIn() async {
    final bool isLoggedIn = await authenticationApiProvider.isLoggedIn();
    return isLoggedIn;
  }

  Future<User> getUser() async {
    final User user = await authenticationApiProvider.getUser();
    return user;
  }

  Future<void> register({
    String email,
    String name,
    String password,
  }) async {
    await authenticationApiProvider.register(
      email: email,
      name: name,
      password: password,
    );
  }

  Future<void> forgotPassword({
    String email,
    String language,
  }) async {
    await userApiProvider.forgotPassword(
      email: email,
      language: language,
    );
  }

  Future<void> resetPassword({
    String password,
    String passwordConfirmed,
  }) async {
    await userApiProvider.resetPassword(
      password: password,
      passwordConfirmed: passwordConfirmed,
    );
  }

  Future<void> editProfilePhoto({
    File photo,
  }) async {
    await userApiProvider.editProfilePhoto(
      photo: photo,
    );
  }
}
